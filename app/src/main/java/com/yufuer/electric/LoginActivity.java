package com.yufuer.electric;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.joker.api.Permissions4M;
import com.joker.api.wrapper.Wrapper;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.base.BaseActivity;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.entity.UserBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.okhttp.CallBackUtil;
import com.yufuer.electric.okhttp.OkhttpUtil;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;
import com.yufuer.electric.utils.YLog;
import com.yufuer.electric.widget.ClearEditText;

import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";
    private static final int READ_PHONE_STATE = 100;

    @BindView(R.id.et_user_name)
    ClearEditText etUserName;
    @BindView(R.id.et_password)
    ClearEditText etPassword;
    @BindView(R.id.cb_remember)
    AppCompatCheckBox cbRemember;
    @BindView(R.id.btn_login)
    Button btnLogin;

    private HttpRequestManager httpRequestManager;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        if ("1".equals(LoginUtils.getRememberLoginStatus())) {
            cbRemember.setChecked(true);
            etUserName.setText(LoginUtils.getUserName());
            etPassword.setText(LoginUtils.getPassword());
        }

        httpRequestManager = HttpRequestManager.getInstance();


        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etPassword.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /**
     * 静态方法启动 Activity
     *
     * @param context
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }



    @OnClick(R.id.btn_login)
    public void onViewClicked() {

        login();
//        Permissions4M.get(this)
//                .requestPermissions(Manifest.permission.READ_PHONE_STATE)
//                .requestCodes(READ_PHONE_STATE)
//                .requestListener(new Wrapper.PermissionRequestListener() {
//                    @Override
//                    public void permissionGranted(int code) {
//
//                    }
//
//                    @Override
//                    public void permissionDenied(int code) {
//
//                    }
//
//                    @Override
//                    public void permissionRationale(int code) {
//
//                    }
//                })
//                .requestPageType(Permissions4M.PageType.ANDROID_SETTING_PAGE)
//                .requestPage(new Wrapper.PermissionPageListener() {
//                    @Override
//                    public void pageIntent(int code, final Intent intent) {
//
//                        final AlertDialog.Builder dialog =
//                                new AlertDialog.Builder(LoginActivity.this);
////                      normalDialog.setIcon(R.drawable.icon_dialog);
//                        dialog.setTitle("权限申请");
//                        dialog.setMessage("用户您好，我们需要您开启读取手机状态权限：\n请点击前往设置页面");
//                        dialog.setPositiveButton("确定",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //...To-do
//                                        startActivity(intent);
//                                        dialog.dismiss();
//                                    }
//                                });
//                        dialog.setNegativeButton("取消",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //...To-do
//                                        dialog.dismiss();
//                                    }
//                                });
//                        // 显示
//                        dialog.show();
//                    }
//                })
//                .requestCustomRationaleListener(new Wrapper.PermissionCustomRationaleListener() {
//                    @Override
//                    public void permissionCustomRationale(int code) {
//
//                        final AlertDialog.Builder dialog =
//                                new AlertDialog.Builder(LoginActivity.this);
////        normalDialog.setIcon(R.drawable.icon_dialog);
//                        dialog.setTitle("提示");
//                        dialog.setMessage("手机状态权限申请：\n我们需要您开启手机状态权限");
//                        dialog.setPositiveButton("确定",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //...To-do
//                                        dialog.dismiss();
//                                        Permissions4M.get(LoginActivity.this)
//                                                .requestOnRationale()
//                                                .requestPermissions(Manifest.permission.READ_PHONE_STATE)
//                                                .requestCodes(READ_PHONE_STATE)
//                                                .request();
//                                    }
//                                });
//                        dialog.setNegativeButton("取消",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //...To-do
//                                        dialog.dismiss();
//                                    }
//                                });
//                        // 显示
//                        dialog.show();
//                    }
//
//                })
//                .request();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        Permissions4M.onRequestPermissionsResult(this, requestCode, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void login() {


        //1.验证用户名和密码是否为空;
        final String userName = etUserName.getText().toString();
        final String password = etPassword.getText().toString();

        if (TextUtils.isEmpty(userName)) {
            ToastUtils.getInstance(this).showMessage(R.string.user_name_empty_error);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            ToastUtils.getInstance(this).showMessage(R.string.password_empty_error);
            return;
        }



       String rememberStatus = cbRemember.isChecked() ? "1" : "0";

        showProgressDialog();

        String url = UrlUtils.URL_LOGIN;

//        JsonObject param = new JsonObject();
//        param.addProperty(LoginConstant.URL_KEY_NAME, userName);
//        param.addProperty(LoginConstant.URL_KEY_PASSWORD, password);
//        param.addProperty(LoginConstant.URL_KEY_REMEMBER, rememberStatus);
//
//        OkhttpUtil.getInstance().okHttpPostJson(url, param.toString(), new CallBackUtil() {
//            @Override
//            public Object onParseResponse(Call call, Response response) {
//
//                YLog.d(TAG, "onParseResponse in");
//                String responseJson = "";
//                JSONObject jsonObject = null;
//                try {
//                    responseJson =  response.body().string();
//                    jsonObject = new JSONObject(responseJson);
//                }catch (Exception e){
//                    YLog.d(TAG, "get response json error");
//                }
//                return jsonObject;
//            }
//
//            @Override
//            public void onResponse(Object response) {
//                dismissProgressDialog();
//                YLog.d(TAG, "get response:" + response);
//                try {
//                    JSONObject jsonObject = (JSONObject) response;
//                    if (jsonObject.has("token")
//                            &&jsonObject.has("role")
//                            &&jsonObject.has("user")){
//
//                        String token = jsonObject.getString("token");
//                        String role = jsonObject.getString("role");
//                        String user = jsonObject.getString("user");
//
//                        if (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(role)
//                                &&!TextUtils.isEmpty(user)){
//                            Gson gson = new Gson();
//                            UserBean userBean = gson.fromJson(((JSONObject) response).toString(), UserBean.class);
//
//                            LoginUtils.login(userBean, password,rememberStatus);
//                            MainActivity.startActivity(LoginActivity.this);
//                            ToastUtils.getInstance(LoginActivity.this).showMessage("登录成功");
//                        }else {
//                            ToastUtils.getInstance(LoginActivity.this).showMessage(R.string.login_username_password_error);
//                        }
//
//                    }
//
//                }catch (Exception e){
//                    YLog.d(TAG, "");
//                    ToastUtils.getInstance(LoginActivity.this).showMessage(R.string.login_username_password_error);
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Exception e) {
//                dismissProgressDialog();
//                YLog.d(TAG, "get response error:" + e.getMessage());
//                ToastUtils.getInstance(LoginActivity.this).showMessage("服务器连接失败，请检查网络是否正常");
//            }
//        });




        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(LoginConstant.URL_KEY_NAME, userName);
        jsonStr.put(LoginConstant.URL_KEY_PASSWORD, password);
        jsonStr.put(LoginConstant.URL_KEY_REMEMBER, rememberStatus);

        httpRequestManager.httpPostJson(url, UserBean.class, TAG, null, null, jsonStr, new HttpListener<UserBean>() {
            @Override
            public void onResponse(UserBean response) {
                dismissProgressDialog();
                if (null!=response){
                    LoginUtils.login(response, password,rememberStatus);
                    MainActivity.startActivity(LoginActivity.this);
                    ToastUtils.getInstance(LoginActivity.this).showMessage("登录成功");
                }else {
                    ToastUtils.getInstance(LoginActivity.this).showMessage(R.string.login_username_password_error);
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                ToastUtils.getInstance(LoginActivity.this).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (httpRequestManager != null) {
            httpRequestManager.cancelRequest(TAG);
        }
    }
}
