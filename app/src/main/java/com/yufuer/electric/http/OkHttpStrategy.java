/*
 * Copyright (c) 2017.
 * BOCO Inter-Telecom. All rights reserved.
 */

package com.yufuer.electric.http;

import android.os.Handler;


import androidx.annotation.NonNull;

import com.yangkan.commonutillibrary.mobile.utils.JsonUtils;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author yangkan
 * @date 2018/12/18
 * @description
 */

public class OkHttpStrategy implements HttpStrategy {
    private static final String TAG = "OkHttpStrategy";

    private Handler mHandler;
    private OkHttpClient mOkHttpClient;

    public OkHttpStrategy(Handler handler) {
        mHandler = handler;
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(HttpConstant.MAX_TIME_OUT, TimeUnit.MILLISECONDS);
        builder.readTimeout(HttpConstant.MAX_TIME_OUT, TimeUnit.MILLISECONDS);
        builder.writeTimeout(HttpConstant.MAX_TIME_OUT, TimeUnit.MILLISECONDS);
        mOkHttpClient = builder.build();
    }

    @Override
    public <T> void httpGet(String url, final Class<T> clazz, Object tag, Map<String, String> headers, final HttpListener<T> httpListener) {
        Request.Builder builder = new Request.Builder();
        if (headers != null) {
            builder.headers(Headers.of(headers));
        }
        final Request request = builder
                .url(url)
                .tag(tag)
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull final Call call, @NonNull final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        httpListener.onFailure(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(response.message());
                        }
                    });
                    return;
                }

                String str = response.body().string();
                LogUtils.d(TAG, "onResponse: " + str);
                try {
                    final T t = JsonUtils.parseJson(str, clazz);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onResponse(t);
                        }
                    });
                } catch (Exception e) {
                    final String exceptionMessage = e.getMessage() + "/" + e.getCause();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(exceptionMessage);
                        }
                    });
                    LogUtils.i(TAG, e.getMessage() + "/" + e.getCause());
                }
            }
        });
    }

    @Override
    public <T> void httpPost(String url, final Class<T> clazz, Object tag, Map<String, String> headers, Map<String, String> params, final HttpListener<T> httpListener) {
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                formBodyBuilder.add(entry.getKey(), entry.getValue());
            }
        }
        Request.Builder builder = new Request.Builder();
        if (headers != null) {
            builder.headers(Headers.of(headers));
        }
        final Request request = builder
                .url(url)
                .tag(tag)
                .post(formBodyBuilder.build())
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull final Call call, @NonNull final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        httpListener.onFailure(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(response.message());
                        }
                    });
                    return;
                }

                String str = response.body().string();
                LogUtils.d(TAG, "onResponse: " + str);
                try {
                    final T t = JsonUtils.parseJson(str, clazz);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onResponse(t);
                        }
                    });
                } catch (Exception e) {
                    final String exceptionMessage = e.getMessage() + "/" + e.getCause();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(exceptionMessage);
                        }
                    });
                    LogUtils.i(TAG, e.getMessage() + "/" + e.getCause());
                }
            }
        });
    }

    @Override
    public <T> void httpArrayGet(String url, final Class<T> clazz, Object tag, Map<String, String> headers, final HttpListener<List<T>> httpListener) {
        Request.Builder builder = new Request.Builder();
        if (headers != null) {
            builder.headers(Headers.of(headers));
        }
        final Request request = builder
                .url(url)
                .tag(tag)
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull final Call call, @NonNull final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        httpListener.onFailure(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(response.message());
                        }
                    });
                    return;
                }

                String str = response.body().string();
                LogUtils.d(TAG, "onResponse: " + str);
                try {
                    final List<T> list = JsonUtils.parseArrayJson(str, clazz);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onResponse(list);
                        }
                    });
                } catch (Exception e) {
                    final String exceptionMessage = e.getMessage() + "/" + e.getCause();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(exceptionMessage);
                        }
                    });
                    LogUtils.i(TAG, e.getMessage() + "/" + e.getCause());
                }
            }
        });
    }

    @Override
    public <T> void httpArrayPost(String url, final Class<T> clazz, Object tag, Map<String, String> headers, Map<String, String> params, final HttpListener<List<T>> httpListener) {
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                formBodyBuilder.add(entry.getKey(), entry.getValue());
            }
        }
        Request.Builder builder = new Request.Builder();
        if (headers != null) {
            builder.headers(Headers.of(headers));
        }
        final Request request = builder
                .url(url)
                .tag(tag)
                .post(formBodyBuilder.build())
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull final Call call, @NonNull final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        httpListener.onFailure(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(response.message());
                        }
                    });
                    return;
                }

                String str = response.body().string();
                LogUtils.d(TAG, "onResponse: " + str);
                try {
                    final List<T> list = JsonUtils.parseArrayJson(str, clazz);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onResponse(list);
                        }
                    });
                } catch (Exception e) {
                    final String exceptionMessage = e.getMessage() + "/" + e.getCause();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(exceptionMessage);
                        }
                    });
                    LogUtils.i(TAG, e.getMessage() + "/" + e.getCause());
                }
            }
        });
    }

    @Override
    public <T> void httpPostJson(String url, Class<T> clazz, Object tag, Map<String, String> headers, Map<String, String> params, Map<String, Object> jsonMap, HttpListener<T> httpListener) {

        String jsonStr="";

        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                formBodyBuilder.add(entry.getKey(), entry.getValue());
            }
        }

        if (jsonMap != null && jsonMap.size() > 0 ){
            jsonStr = JsonUtils.toJson(jsonMap);
        }


        //MediaType  设置Content-Type 标头中包含的媒体类型值
        RequestBody requestBody = FormBody.create(MediaType.parse("application/json; charset=utf-8")
                , jsonStr);



        Request.Builder builder = new Request.Builder();
        if (headers != null) {
            builder.headers(Headers.of(headers));
        }
        final Request request = builder
                .url(url)
                .tag(tag)
                .post(formBodyBuilder.build())
                .post(requestBody)
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull final Call call, @NonNull final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        httpListener.onFailure(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(response.message());
                        }
                    });
                    return;
                }

                String str = response.body().string();
                LogUtils.d(TAG, "onResponse: " + str);
                try {
                    final T t = JsonUtils.parseJson(str, clazz);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onResponse(t);
                        }
                    });
                } catch (Exception e) {
                    final String exceptionMessage = e.getMessage() + "/" + e.getCause();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            httpListener.onFailure(exceptionMessage);
                        }
                    });
                    LogUtils.i(TAG, e.getMessage() + "/" + e.getCause());
                }
            }
        });
    }

    @Override
    public void cancelRequest(Object tag) {
        for (Call call : mOkHttpClient.dispatcher().queuedCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }
        for (Call call : mOkHttpClient.dispatcher().runningCalls()) {
            if (tag.equals(call.request().tag())) {
                call.cancel();
            }
        }
    }
}
