package com.yufuer.electric.http;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


/**
 * @author yangkan
 * @date 2018/12/18
 * @description 参数化类型实现类，用于Gson解析统一格式的json串
 */
public class ParameterizedTypeImpl implements ParameterizedType {

    private final Class raw;
    private final Type[] args;

    public ParameterizedTypeImpl(Class raw, Type[] args) {
        this.raw = raw;
        this.args = args != null ? args : new Type[0];
    }

    @Override
    public Type[] getActualTypeArguments() {
        return args;
    }

    @Override
    public Type getRawType() {
        return raw;
    }

    @Override
    public Type getOwnerType() {
        return null;
    }
}
