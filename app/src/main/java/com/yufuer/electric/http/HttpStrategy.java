/*
 * Copyright (c) 2017.
 * BOCO Inter-Telecom. All rights reserved.
 */

package com.yufuer.electric.http;

import java.util.List;
import java.util.Map;

/**
 * @author yangkan
 * @date 2018/12/18
 * @description 策略模式，Http策略接口
 */

public interface HttpStrategy {

    /**
     * httpGet请求，带header
     *
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param httpListener
     * @param <T>
     */
    <T> void httpGet(String url, Class<T> clazz, Object tag, Map<String, String> headers, HttpListener<T> httpListener);

    /**
     * httpPost请求，带header
     *
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param params
     * @param httpListener
     * @param <T>
     */
    <T> void httpPost(String url, Class<T> clazz, Object tag, Map<String, String> headers,
                      Map<String, String> params, HttpListener<T> httpListener);

    /**
     *
     * http get 请求，字符串是json数组
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param httpListener
     * @param <T>
     */
    <T> void httpArrayGet(String url, final Class<T> clazz, Object tag, Map<String, String> headers,
                          final HttpListener<List<T>> httpListener);

    /**
     * http post 请求，字符串是 json 数组
     *
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param params
     * @param httpListener
     * @param <T>
     */
    <T> void httpArrayPost(String url, Class<T> clazz, Object tag, Map<String, String> headers,
                           Map<String, String> params, HttpListener<List<T>> httpListener);




    /**
     * http post 请求，字符串是 json 数组
     *
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param params
     * @param httpListener
     * @param <T>
     */
    <T> void httpPostJson(String url, Class<T> clazz, Object tag, Map<String, String> headers,
                           Map<String, String> params, Map<String, Object> jsonMap,HttpListener<T> httpListener);


    /**
     * 根据标签，取消网络请求
     *
     * @param tag
     */
    void cancelRequest(Object tag);
}
