package com.yufuer.electric.http;

/**
 * @author yangkan
 * @date 2018/12/18
 * @description Http请求回调
 */

public interface HttpListener<T> {

    void onResponse(T response);

    void onFailure(String message);
}