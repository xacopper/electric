package com.yufuer.electric;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yufuer.electric.adapter.MainPageAdapter;
import com.yufuer.electric.base.BaseToolBarActivity;
import com.yufuer.electric.fragment.DeviceQueryFragment;
import com.yufuer.electric.fragment.StatusQueryFragment;
import com.yufuer.electric.fragment.SystemReportFragment;
import com.yufuer.electric.fragment.TechnicalAnalysisFragment;
import com.yufuer.electric.widget.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseToolBarActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.viewpager)
    NoScrollViewPager viewpager;
    @BindView(R.id.bnv_tab)
    BottomNavigationView bnvTab;

    /**
     * 静态方法启动 Activity
     *
     * @param context
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        setToolBar(toolbar);

        setToolBarTitle(toolbarTitle, getString(R.string.login_title));

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);//左侧添加一个默认的返回图标
        getSupportActionBar().setHomeButtonEnabled(false); //设置返回键可用

//        BottomNavigationViewHelper.disableShiftMode(bnvTab);

        bnvTab.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int itemId = item.getItemId();
                switch (itemId) {
                    case R.id.tab_status_query:
                        clickTab(0);
                        //返回true,否则tab按钮不变色,未被选中
                        return true;
                    case R.id.tab_technical_analysis:
                        clickTab(1);
                        return true;
                    case R.id.tab_device_query:
                        clickTab(2);
                        return true;
                    case R.id.tab_system_report:
                        clickTab(3);
                        return true;
                    default:
                        break;
                }
                return false;
            }
        });


        MainPageAdapter adapter = new MainPageAdapter(getSupportFragmentManager(), initFragment());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changePage(position);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    protected void setMenuOptions(Menu menu) {

    }

    @Override
    protected void setMenuItemClick(MenuItem item) {

    }

    private List<Fragment> initFragment() {
        List<Fragment> mFragments = new ArrayList<>();
        mFragments.add(StatusQueryFragment.newInstance("", ""));
        mFragments.add(TechnicalAnalysisFragment.newInstance("", ""));
        mFragments.add(DeviceQueryFragment.newInstance("", ""));
        mFragments.add(SystemReportFragment.newInstance("", ""));
        return mFragments;
    }

    private void clickTab(int item) {
        //为防止隔页切换时,滑过中间页面的问题,去除页面切换缓慢滑动的动画效果
        viewpager.setCurrentItem(item, false);
        supportInvalidateOptionsMenu();
    }

    private void changePage(int position) {
        switch (position) {
            case 0:
                bnvTab.setSelectedItemId(R.id.tab_status_query);
                break;
            case 1:
                bnvTab.setSelectedItemId(R.id.tab_technical_analysis);
                break;
            case 2:
                bnvTab.setSelectedItemId(R.id.tab_device_query);
                break;
            case 3:
                bnvTab.setSelectedItemId(R.id.tab_system_report);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
