package com.yufuer.electric.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.DialogActivity;
import com.yufuer.electric.R;
import com.yufuer.electric.activity.FaultWarningActivity;
import com.yufuer.electric.activity.MyDialogActivityActivity;
import com.yufuer.electric.activity.WindGroupActivity;
import com.yufuer.electric.app.MyApplication;
import com.yufuer.electric.base.BaseFragment;
import com.yufuer.electric.base.BaseToolBarFragment;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.constant.MyDialogConstant;
import com.yufuer.electric.constant.SystemReportConstant;
import com.yufuer.electric.constant.WindConstant;
import com.yufuer.electric.entity.CountBean;
import com.yufuer.electric.entity.MyDialogBean;
import com.yufuer.electric.entity.WindBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;
import com.yufuer.electric.utils.WindUtils;
import com.yufuer.electric.utils.YLog;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class StatusQueryFragment extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private boolean isFirst = true;

    @BindView(R.id.tv_wind_field_name)
    TextView tvWindFieldName;
    @BindView(R.id.rl_wind_field_name)
    RelativeLayout rlWindFieldName;
    @BindView(R.id.tv_wind_group_name)
    TextView tvWindGroupName;
    @BindView(R.id.rl_wind_group_name)
    RelativeLayout rlWindGroupName;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.tv_wind_number)
    TextView tvWindNumber;
    @BindView(R.id.rl_wind_number)
    RelativeLayout rlWindNumber;
    @BindView(R.id.tv_wind_electromechanical_number)
    TextView tvWindElectromechanicalNumber;
    @BindView(R.id.rl_wind_electromechanical_number)
    RelativeLayout rlWindElectromechanicalNumber;
    @BindView(R.id.tv_sensor_number)
    TextView tvSensorNumber;
    @BindView(R.id.rl_sensor_number)
    RelativeLayout rlSensorNumber;
    @BindView(R.id.tv_wind_normal_operation_number)
    TextView tvWindNormalOperationNumber;
    @BindView(R.id.rl_wind_normal_operation_number)
    RelativeLayout rlWindNormalOperationNumber;
    @BindView(R.id.tv_warn_number)
    TextView tvWarnNumber;
    @BindView(R.id.rl_warn_number)
    RelativeLayout rlWarnNumber;
    @BindView(R.id.tv_wind_offline_number)
    TextView tvWindOfflineNumber;
    @BindView(R.id.rl_wind_offline_number)
    RelativeLayout rlWindOfflineNumber;

    private ArrayList<String> mWindFileList = new ArrayList<>();
    private ArrayList<String> mWindGroupList = new ArrayList<>();

    private static HttpRequestManager httpRequestManager;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StatusQueryFragment newInstance(String param1, String param2) {
        StatusQueryFragment fragment = new StatusQueryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        httpRequestManager = HttpRequestManager.getInstance();
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_status_query;
    }



    private void getStatusData(String windName, String groupName){

        YLog.d(TAG, "start load data");
        getEquipmentWindfieldCount(windName, groupName);
        getEquipmentWarnCount(windName, groupName);
        getEquipmentIdCount(windName, groupName);
        getEquipmentOfflineCount(windName, groupName);
        getEquipmentNormalCount(windName, groupName);
        getEquipmentWindGroupCount(windName, groupName);

    }

    private void getEquipmentWindfieldCount(String windName, String groupName){

        YLog.d(TAG, "getEquipmentWindfieldCount in");
        HttpListener httpListener = new HttpListener<CountBean>() {
            @Override
            public void onResponse(CountBean response) {

                if (null!=response){
                    String windfiledCount = response.getCount().getStaticCount();
                    tvWindNumber.setText(windfiledCount);
                    YLog.d(TAG, "get getEquipmentWindfieldCount success " + windfiledCount);
                }else {
                    YLog.d(TAG, "get getEquipmentWindfieldCount failed ");
                }

            }

            @Override
            public void onFailure(String message) {
                YLog.d(TAG, "getEquipmentWindfieldCount connect to servier error ");
            }
        };

        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(WindConstant.WIND_FIELD_NAME, windName);
        jsonMap.put(WindConstant.WIND_GROUP_NAME, groupName);

        Map<String, String> mapHeader = new HashMap<String, String>();
        mapHeader.put("Authorization", LoginUtils.getToken());

        httpRequestManager.httpPostJson(UrlUtils.URL_GET_ALL_WIND_FIELD_COUNT,
                CountBean.class, TAG, mapHeader, null, jsonMap, httpListener);

    }

    private void getEquipmentWarnCount(String windName, String groupName){

        YLog.d(TAG, "getEquipmentWarnCount in");
        HttpListener httpListener = new HttpListener<CountBean>() {
            @Override
            public void onResponse(CountBean response) {

                if (null!=response){
                    String windWarnCount = response.getCount().getStaticCount();
                    tvWarnNumber.setText(windWarnCount);
                    YLog.d(TAG, "get getEquipmentWarnCount success " + windWarnCount);
                }else {
                    YLog.d(TAG, "get getEquipmentWarnCount failed ");
                }


            }

            @Override
            public void onFailure(String message) {
                YLog.d(TAG, "getEquipmentWarnCount connect to servier error ");
            }
        };

        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(WindConstant.WIND_FIELD_NAME, windName);
        jsonMap.put(WindConstant.WIND_GROUP_NAME, groupName);


        Map<String, String> mapHeader = new HashMap<String, String>();
        mapHeader.put("Authorization", LoginUtils.getToken());

        httpRequestManager.httpPostJson(UrlUtils.URL_GET_WARN_EQUIPMENT_COUNT,
                CountBean.class, TAG, mapHeader, null, jsonMap, httpListener);

    }

    private void getEquipmentIdCount(String windName, String groupName){

        YLog.d(TAG, "getEquipmentIdCount in");
        HttpListener httpListener = new HttpListener<CountBean>() {
            @Override
            public void onResponse(CountBean response) {

                if (null!=response){
                    String equipmentIdCount = response.getCount().getStaticCount();

                    tvSensorNumber.setText(equipmentIdCount);
                    YLog.d(TAG, "get getEquipmentIdCount success " + equipmentIdCount);
                }else {
                    YLog.d(TAG, "get getEquipmentIdCount failed ");
                }


            }

            @Override
            public void onFailure(String message) {
                YLog.d(TAG, "getEquipmentIdCount connect to servier error ");
            }
        };

        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(WindConstant.WIND_FIELD_NAME, windName);
        jsonMap.put(WindConstant.WIND_GROUP_NAME, groupName);


        Map<String, String> mapHeader = new HashMap<String, String>();
        mapHeader.put("Authorization", LoginUtils.getToken());

        httpRequestManager.httpPostJson(UrlUtils.URL_GET_EQUIPMENT_ID_COUNT,
                CountBean.class, TAG, mapHeader, null, jsonMap, httpListener);

    }

    private void getEquipmentOfflineCount(String windName, String groupName){

        YLog.d(TAG, "getEquipmentOfflineCount in");
        HttpListener httpListener = new HttpListener<CountBean>() {
            @Override
            public void onResponse(CountBean response) {

                if (null!=response){
                    String offlineCount = response.getCount().getStaticCount();

                    tvWindOfflineNumber.setText(offlineCount);
                    YLog.d(TAG, "get getEquipmentOfflineCount success " + offlineCount);
                }else {
                    YLog.d(TAG, "get getEquipmentOfflineCount failed ");
                }

            }

            @Override
            public void onFailure(String message) {
                YLog.d(TAG, "getEquipmentOfflineCount connect to servier error ");
            }
        };

        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(WindConstant.WIND_FIELD_NAME, windName);
        jsonMap.put(WindConstant.WIND_GROUP_NAME, groupName);


        Map<String, String> mapHeader = new HashMap<String, String>();
        mapHeader.put("Authorization", LoginUtils.getToken());

        httpRequestManager.httpPostJson(UrlUtils.URL_GET_OFFLINE_EQUIPMENT_COUNT,
                CountBean.class, TAG, mapHeader, null, jsonMap, httpListener);

    }

    private void getEquipmentNormalCount(String windName, String groupName){

        YLog.d(TAG, "getEquipmentNormalCount in");
        HttpListener httpListener = new HttpListener<CountBean>() {
            @Override
            public void onResponse(CountBean response) {

                if (null!=response){
                    String normalCount = response.getCount().getStaticCount();
                    tvWindNormalOperationNumber.setText(normalCount);
                    YLog.d(TAG, "get getEquipmentNormalCount success " + normalCount);
                }else {
                    YLog.d(TAG, "get getEquipmentNormalCount failed ");
                }
            }

            @Override
            public void onFailure(String message) {
                YLog.d(TAG, "getEquipmentNormalCount connect to servier error ");
            }
        };

        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(WindConstant.WIND_FIELD_NAME, windName);
        jsonMap.put(WindConstant.WIND_GROUP_NAME, groupName);

        Map<String, String> mapHeader = new HashMap<String, String>();
        mapHeader.put("Authorization", LoginUtils.getToken());

        httpRequestManager.httpPostJson(UrlUtils.URL_GET_NORMAL_EQUIPMENT_COUNT,
                CountBean.class, TAG, mapHeader, null, jsonMap, httpListener);

    }

    private void getEquipmentWindGroupCount(String windName, String groupName){

        YLog.d(TAG, "getEquipmentWindGroupCount in");
        HttpListener httpListener = new HttpListener<CountBean>() {
            @Override
            public void onResponse(CountBean response) {

                if (null!=response){
                    String windGroupCount = response.getCount().getStaticCount();

                    tvWindElectromechanicalNumber.setText(windGroupCount);
                    YLog.d(TAG, "get loadAllWindGroupCount success " + windGroupCount);
                }else {
                    YLog.d(TAG, "get getEquipmentWindGroupCount failed ");
                }


            }

            @Override
            public void onFailure(String message) {
                YLog.d(TAG, "getEquipmentWindGroupCount connect to servier error ");
            }
        };

        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(WindConstant.WIND_FIELD_NAME, windName);
        jsonMap.put(WindConstant.WIND_GROUP_NAME, groupName);

        Map<String, String> mapHeader = new HashMap<String, String>();
        mapHeader.put("Authorization", LoginUtils.getToken());

        httpRequestManager.httpPostJson(UrlUtils.URL_GET_ALL_WIND_GROUP_COUNT,
                CountBean.class, TAG, mapHeader, null, jsonMap, httpListener);

    }

    private void getWindFileList() {

        String url = UrlUtils.URL_GET_ALL_WIND_FIELD_NAME;

        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());
        showProgressDialog();

        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, null, new HttpListener<MyDialogBean>() {
            @Override
            public void onResponse(MyDialogBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<String> list = response.getList();
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }
                    String windName = response.getList().get(0);
                    tvWindFieldName.setText(windName);
                    LogUtils.d(TAG, "list size: " + list.size());
                    mWindFileList.addAll(list);

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }


    private void getWindGroupList() {

        String url = UrlUtils.URL_GET_ALL_WIND_GROUP_NAME;

        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());


        String windFieldNameTemp = tvWindFieldName.getText().toString();
        if (TextUtils.isEmpty(windFieldNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请先选择风场");
            return;
        }

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(SystemReportConstant.KEY_WIND_FIELD_NAME, windFieldNameTemp);

        showProgressDialog();

        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, jsonStr, new HttpListener<MyDialogBean>() {
            @Override
            public void onResponse(MyDialogBean response) {
                dismissProgressDialog();
                if (null != response) {
                    List<String> list = response.getList();
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }


                    LogUtils.d(TAG, "list size: " + list.size());

                    String groupName = response.getList().get(0);
                    tvWindGroupName.setText(groupName);

                    mWindGroupList.clear();
                    mWindGroupList.addAll(list);

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }

    @Override
    protected void initView() {

         getWindFileList();

        if (tvWindFieldName != null){
            tvWindFieldName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    getWindGroupList();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


        if (rlWindGroupName != null){
            tvWindGroupName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (isFirst){
                        getStatusData(tvWindFieldName.getText().toString(), s.toString());
                        isFirst = false;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStatusData(tvWindFieldName.getText().toString(), tvWindGroupName.getText().toString());
            }
        });

        YLog.d(TAG, " init view in");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null){
            switch (requestCode) {
                case MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME:
                    if (resultCode == Activity.RESULT_OK) {
                        String windFlieName = data.getStringExtra(MyDialogConstant.KEY_RESULT);
                        tvWindFieldName.setText(windFlieName);

                    }
                    break;
                case MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME:
                    if (resultCode == Activity.RESULT_OK) {
                        String windGroupName = data.getStringExtra(MyDialogConstant.KEY_RESULT);
                        tvWindGroupName.setText(windGroupName);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void setMenuOptions(Menu menu) {

    }

    @Override
    protected void setMenuItemClick(MenuItem item) {

    }

    @OnClick({R.id.rl_wind_field_name, R.id.rl_wind_group_name, R.id.btn_confirm, R.id.rl_wind_number, R.id.rl_wind_electromechanical_number, R.id.rl_sensor_number, R.id.rl_wind_normal_operation_number, R.id.rl_warn_number, R.id.rl_wind_offline_number})
    public void onViewClicked(View view) {

        String windFieldName = tvWindFieldName.getText().toString();
        String windGroupName = tvWindGroupName.getText().toString();

        switch (view.getId()) {
            case R.id.rl_wind_field_name:
                if (mWindFileList.size()<=0){
                    ToastUtils.getInstance(getContext()).showMessage("暂无数据");
                    return;
                }
                MyDialogActivityActivity.startActivity(this,
                        MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME,mWindFileList,"风场");
                break;
            case R.id.rl_wind_group_name:
                String windFieldNameTemp = tvWindFieldName.getText().toString();
                if (TextUtils.isEmpty(windFieldNameTemp)) {
                    ToastUtils.getInstance(getContext()).showMessage("请先选择风场");
                    return;
                }

                MyDialogActivityActivity.startActivity(this,
                        MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME,mWindGroupList,"机组");
                break;
            case R.id.btn_confirm:
                break;
            case R.id.rl_wind_number:
                break;
            case R.id.rl_wind_electromechanical_number:
                WindGroupActivity.startActivity(getActivity(), windFieldName, windGroupName);
                break;
            case R.id.rl_sensor_number:
                break;
            case R.id.rl_wind_normal_operation_number:
                break;
            case R.id.rl_warn_number:
                FaultWarningActivity.startActivity(getActivity(),windFieldName,windGroupName);
                break;
            case R.id.rl_wind_offline_number:
                break;
        }
    }
}
