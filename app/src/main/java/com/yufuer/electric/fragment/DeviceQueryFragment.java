package com.yufuer.electric.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.adapter.DeviceQueryAdapter;
import com.yufuer.electric.app.MyApplication;
import com.yufuer.electric.base.BaseFragment;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.constant.SystemReportConstant;
import com.yufuer.electric.entity.DeviceQueryBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;
import com.yufuer.electric.widget.CustomLinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class DeviceQueryFragment extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.btn_previous_page)
    Button btnPreviousPage;
    @BindView(R.id.btn_next_page)
    Button btnNextPage;
    private HttpRequestManager httpRequestManager;

    private int current_Page = 0;

    private DeviceQueryAdapter mAdapter;


    private List<DeviceQueryBean.ListBean> mDataList = new ArrayList<>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeviceQueryFragment newInstance(String param1, String param2) {
        DeviceQueryFragment fragment = new DeviceQueryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getContentViewId() {
        return R.layout.fragment_device_query;
    }

    @Override
    protected void initView() {
        httpRequestManager = HttpRequestManager.getInstance();
        initRecyclerView();
        loadData(1);
    }

    @Override
    protected void setMenuOptions(Menu menu) {

    }

    @Override
    protected void setMenuItemClick(MenuItem item) {

    }

    private void loadData(int currentPage) {

        String url = UrlUtils.URL_GET_DEVICE_QUERY;
        LogUtils.d(TAG, "url: " + url);


        showProgressDialog();
        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());

        Map<String, Object> jsonStr = new HashMap<>();

        jsonStr.put(SystemReportConstant.KEY_SIZE, 10);
        jsonStr.put(SystemReportConstant.KEY_CURRENT_PAGE, currentPage);


        httpRequestManager.httpPostJson(url, DeviceQueryBean.class, TAG, header, null, jsonStr, new HttpListener<DeviceQueryBean>() {
            @Override
            public void onResponse(DeviceQueryBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<DeviceQueryBean.ListBean> list = response.getList();

                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }
                    LogUtils.d(TAG, "list size: " + list.size());
                    mDataList.addAll(list);
                    LogUtils.d(TAG, "mDataList size: " + list.size());
                    mAdapter.notifyDataSetChanged();

                    // TODO: 2019/9/1  请求数据后，存储参数。这些参数是上一页、下一页点击时候的判断条件

                    current_Page = currentPage;


                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                LogUtils.d(TAG, "failure: " + message);
                dismissProgressDialog();
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (httpRequestManager != null) {
            httpRequestManager.cancelRequest(TAG);
        }
    }


    /**
     * 初始话RecyclerView
     */
    private void initRecyclerView() {

        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getContext());
        layoutManager.setScrollEnabled(false);
        recyclerview.setLayoutManager(layoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        recyclerview.setHasFixedSize(true);
        mAdapter = new DeviceQueryAdapter(R.layout.item_device_query, mDataList);
        recyclerview.setAdapter(mAdapter);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View headerView = inflater.inflate(R.layout.layout_header_device_query, null, false);
        mAdapter.addHeaderView(headerView);
    }

    private void getNextPageData() {
        int currentPageTemp = current_Page;
        currentPageTemp = currentPageTemp + 1;
        mDataList.clear();
        loadData(currentPageTemp);
    }

    private void getPreviousPageData() {

        int currentPageTemp = current_Page;
        currentPageTemp = current_Page - 1;
        if (current_Page <= 1) {
            ToastUtils.getInstance(getContext()).showMessage("当前已是第一页");
            return;
        }
        mDataList.clear();
        loadData(currentPageTemp);
    }

    @OnClick({R.id.btn_previous_page, R.id.btn_next_page})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_previous_page:
                getPreviousPageData();
                break;
            case R.id.btn_next_page:
                getNextPageData();
                break;
        }
    }
}
