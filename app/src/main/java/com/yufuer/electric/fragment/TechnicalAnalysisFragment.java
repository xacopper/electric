package com.yufuer.electric.fragment;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.Gson;
import com.yangkan.commonutillibrary.mobile.utils.DateUtils;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.activity.DrawView;
import com.yufuer.electric.activity.MyDialogActivityActivity;
import com.yufuer.electric.activity.Point;
import com.yufuer.electric.app.MyApplication;
import com.yufuer.electric.base.BaseFragment;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.constant.MyDialogConstant;
import com.yufuer.electric.constant.SystemReportConstant;
import com.yufuer.electric.constant.TechnicalAnalysisConstant;
import com.yufuer.electric.entity.ChartBean;
import com.yufuer.electric.entity.MyDialogBean;
import com.yufuer.electric.entity.RadarBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.utils.DatePickerUtils;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;
import com.yufuer.electric.utils.YLog;
import com.yufuer.electric.widget.SceneMarkerView;

import java.security.cert.PolicyNode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class TechnicalAnalysisFragment extends BaseFragment {
    private static final String TAG = "TechnicalAnalysis";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.old_shake)
    TextView oldShake;
    @BindView(R.id.old_angle)
    TextView oldAngle;
    @BindView(R.id.old_direction)
    TextView oldDirection;
    @BindView(R.id.old_time)
    TextView oldTime;
    @BindView(R.id.new_shake)
    TextView newShake;
    @BindView(R.id.new_angle)
    TextView newAngle;
    @BindView(R.id.new_direction)
    TextView newDirection;
    @BindView(R.id.new_time)
    TextView newTime;

    private boolean isFirst = true;


    //    @BindView(R.id.radar_chart)
//    RadarChart radarChart;
    @BindView(R.id.tv_wind_field_name)
    TextView tvWindFieldName;
    @BindView(R.id.rl_wind_field_name)
    RelativeLayout rlWindFieldName;
    @BindView(R.id.tv_wind_group_name)
    TextView tvWindGroupName;
    @BindView(R.id.rl_wind_group_name)
    RelativeLayout rlWindGroupName;
    @BindView(R.id.tv_start_time)
    TextView tvStartTime;
    @BindView(R.id.rl_start_time)
    RelativeLayout rlStartTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;
    @BindView(R.id.rl_end_time)
    RelativeLayout rlEndTime;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.line_chart)
    LineChart mLineChartShake;
    @BindView(R.id.line_chart_angle)
    LineChart mLineChartAngle;
    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.line_chart_alarm)
    LineChart mLineChartAlarm;
    private static HttpRequestManager httpRequestManager;
    private Calendar calendar = Calendar.getInstance();

    private List<String> xLabelList = new ArrayList<>();
    private ArrayList<String> mWindFileList = new ArrayList<>();
    private ArrayList<String> mWindGroupList = new ArrayList<>();

    private ArrayList<ILineDataSet> shakeDataSets = new ArrayList<>();
    private ArrayList<ILineDataSet> angleDataSets = new ArrayList<>();
    private ArrayList<ILineDataSet> alarmDataSets = new ArrayList<>();

    private ArrayList<IRadarDataSet> shakeRadarDataSets = new ArrayList<>();

    private String[] colors = {"#2582ee", "#1fbef2", "#22c290", "#21e160",
            "#ade978", "#afc642", "#eeb333", "#e9743b", "#de47ad"};


    private int[] shadow = {R.drawable.shape_shadow_low,
            R.drawable.shape_shadow_room, R.drawable.shape_shadow_cover,
            R.drawable.shape_shadow_high, R.drawable.shape_shadow_quality,
            R.drawable.shape_shadow_drop, R.drawable.shape_shadow_cut,
            R.drawable.shape_shadow_connect, R.drawable.shape_shadow_extend};

    private Typeface tfRegular;
    private Typeface tfLight;
    private Typeface mTfBold;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TechnicalAnalysisFragment newInstance(String param1, String param2) {
        TechnicalAnalysisFragment fragment = new TechnicalAnalysisFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        httpRequestManager = HttpRequestManager.getInstance();
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_technical_analysis;
    }

    @Override
    protected void initView() {


        if (tvWindFieldName != null) {
            tvWindFieldName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    getWindGroupList();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        if (tvWindGroupName != null) {
            tvWindGroupName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (isFirst) {
//                        getStatusData(tvWindFieldName.getText().toString(), s.toString());
                        reloadData();
                        isFirst = false;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        initDate();
        getWindFileList();
    }

    private void loadData(String windFieldName, String windGroupName, String startTime, String endTime) {

        String url = UrlUtils.URL_GET_CHART;
        LogUtils.d(TAG, "url: " + url);

        showProgressDialog();
        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(TechnicalAnalysisConstant.KEY_WIND_FIELD_NAME, windFieldName);
        jsonStr.put(TechnicalAnalysisConstant.KEY_WIND_GROUP_NAME, windGroupName);
        jsonStr.put(TechnicalAnalysisConstant.KEY_CURRENT_PAGE, 1);
        jsonStr.put(TechnicalAnalysisConstant.KEY_START_TIME, startTime);
        jsonStr.put(TechnicalAnalysisConstant.KEY_END_TIME, endTime);


        httpRequestManager.httpPostJson(url, ChartBean.class, TAG, header, null, jsonStr, new HttpListener<ChartBean>() {
            @Override
            public void onResponse(ChartBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<List<ChartBean.MapsBean>> list = response.getMaps();

                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }

                    xLabelList.clear();
                    // 取第一组数据,获取x轴坐标.
                    List<ChartBean.MapsBean> indexOfOne = list.get(0);
                    for (int i = 0; i < indexOfOne.size(); i++) {
                        xLabelList.add(indexOfOne.get(i).getTimeOfFilm());
                    }


//                    解析数据 .最多一张图显示9条折线

//                    for (List<ChartBean.MapsBean> chartList:list){

                    shakeDataSets.clear();
                    alarmDataSets.clear();
                    angleDataSets.clear();
                    for (int i = 0; i < (list.size() <= 9 ? list.size() : 9); i++) {
                        List<ChartBean.MapsBean> chartList = list.get(i);

                        List<Float> shakeData = new LinkedList<>();
                        List<Float> angleData = new LinkedList<>();
                        List<Float> alarmData = new LinkedList<>();

//                         取折线说明ID的值
                        String equipmentID = "";
                        if (chartList.size() > 0) {
                            // 取第一组数据,获取x轴坐标.
                            ChartBean.MapsBean bean = chartList.get(0);
                            equipmentID = bean.getEquipment_id();
                        }

                        for (ChartBean.MapsBean mapsBean : chartList) {
//                                获取晃动数值
                            shakeData.add(str2Float(mapsBean.getHuangdongzhi()));
//                                获取倾角数值
                            angleData.add(str2Float(mapsBean.getJiajiao()));
//                                获取倾角数值
                            alarmData.add(str2Float(mapsBean.getGaojing()));

                        }
//                            添加数据
                        shakeDataSets.add(getLineDataSet(getLineAxisYLabel(shakeData), equipmentID, colors[i], shadow[i]));
                        angleDataSets.add(getLineDataSet(getLineAxisYLabel(angleData), equipmentID, colors[i], shadow[i]));
                        alarmDataSets.add(getLineDataSet(getLineAxisYLabel(alarmData), equipmentID, colors[i], shadow[i]));
                    }

                    if (xLabelList.size() > 0 && shakeDataSets.size() > 0) {
                        initLineChart(mLineChartShake, xLabelList, shakeDataSets, 1);
                    }
                    if (xLabelList.size() > 0 && angleDataSets.size() > 0) {
                        initLineChart(mLineChartAngle, xLabelList, angleDataSets, 2);
                    }
                    if (xLabelList.size() > 0 && alarmDataSets.size() > 0) {
                        initLineChart(mLineChartAlarm, xLabelList, alarmDataSets, 3);
                    }


                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                LogUtils.d(TAG, "failure: " + message);
                dismissProgressDialog();
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });
    }

    @Override
    protected void setMenuOptions(Menu menu) {

    }

    @Override
    protected void setMenuItemClick(MenuItem item) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (httpRequestManager != null) {
            httpRequestManager.cancelRequest(TAG);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME:
                    if (resultCode == Activity.RESULT_OK) {
                        String windFlieName = data.getStringExtra(MyDialogConstant.KEY_RESULT);
                        tvWindFieldName.setText(windFlieName);

                    }
                    break;
                case MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME:
                    if (resultCode == Activity.RESULT_OK) {
                        String windGroupName = data.getStringExtra(MyDialogConstant.KEY_RESULT);
                        tvWindGroupName.setText(windGroupName);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void initLineChart(LineChart mLineChart, List<String> xLabelList, ArrayList<ILineDataSet> dataSets, int type) {

        LogUtils.d(TAG, "initLineChart: ");
//        设置x轴
        XAxis xAxis = mLineChart.getXAxis();
//        是否绘制轴线
        xAxis.setDrawAxisLine(true);
        xAxis.setAxisLineColor(R.color.resource_bar_chart_grid_line_color);
        xAxis.setTextSize(9f);
        xAxis.setTextColor(R.color.resource_bar_x_label_text_color);
//        设置x轴上每个点对应的线
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        //设置X轴坐标之间的最小间隔
        xAxis.setGranularity(1f);

        xAxis.setGranularityEnabled(true);    //粒度
//        绘制标签  指x轴上的对应数值
        xAxis.setDrawLabels(true);
        xAxis.setLabelRotationAngle(-45);
//        xAxis.setLabelCount(xLabelList.size());
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xLabelList));


        //设置X轴的刻度数量，第二个参数为true,将会画出明确数量（带有小数点），但是可能值导致不均匀，默认（6，false）
        xAxis.setLabelCount(xLabelList.size() / 6, false);
        //设置X轴的值（最小值、最大值、然后会根据设置的刻度数量自动分配刻度显示）
        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum((float) xLabelList.size());


        /**
         * 设置y左轴
         */
        YAxis axisLeft = mLineChart.getAxisLeft();
        axisLeft.setAxisLineColor(R.color.resource_bar_chart_grid_line_color);
//        axisLeft.setAxisMinimum(0);
        axisLeft.setTextSize(9f);
//        axisLeft.setTextColor(R.color.resource_bar_y_label_text_color);
        axisLeft.setDrawGridLines(true);
        axisLeft.setGridColor(R.color.resource_bar_chart_grid_line_color);
        axisLeft.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                DecimalFormat decimalFormat = new DecimalFormat("##0.00");

                switch (type){
                    case 1:
                        decimalFormat = new DecimalFormat("##0.00(米)");
                        break;
                    case 2:
                        decimalFormat = new DecimalFormat("##0(度)");
                        break;
                    case 3:
                        break;
                        default:{
                            decimalFormat = new DecimalFormat("##0.00");
                        }
                }

                return decimalFormat.format(value);

            }
        });

        /**
         * 设置y右轴
         */
        YAxis axisRight = mLineChart.getAxisRight();
        axisRight.setAxisLineColor(R.color.resource_bar_chart_grid_line_color);
//        axisLeft.setAxisMinimum(0);
        axisRight.setTextSize(9f);
//        axisRight.setTextColor(R.color.resource_bar_y_label_text_color);
        axisRight.setDrawGridLines(false);
        axisRight.setGridColor(R.color.resource_bar_chart_grid_line_color);
        axisRight.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                DecimalFormat decimalFormat = new DecimalFormat("##0.00");
                return decimalFormat.format(value);
            }
        });

        if (dataSets.size() > 0) {
            dataSets.get(0).setAxisDependency(YAxis.AxisDependency.LEFT);
            axisLeft.setTextColor(dataSets.get(0).getColor());
        }

        if (dataSets.size() >= 2) {
            dataSets.get(1).setAxisDependency(YAxis.AxisDependency.RIGHT);
            //设置Y的右轴不显示
            mLineChart.getAxisRight().setEnabled(true);
            axisRight.setTextColor(dataSets.get(1).getColor());
        } else {
            mLineChart.getAxisRight().setEnabled(false);
        }

        //设置描述信息不显示
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setNoDataText(getResources().getText(R.string.chart_view_no_data).toString());
        mLineChart.setClickable(true);

//        //设置手势滑动事件
//        mLineChart.setOnChartGestureListener(this);
//        //设置数值选择监听
//        mLineChart.setOnChartValueSelectedListener(this);

        //设置支持触控
        mLineChart.setTouchEnabled(true);
        //设置是否支持拖拽
        mLineChart.setDragEnabled(true);
        //设置能否缩放
        mLineChart.setScaleEnabled(true);
        //设置true支持两个指头向X、Y轴的缩放，如果为false，只能支持X或者Y轴的当方向缩放
        mLineChart.setPinchZoom(true);


//      设置图例样式，默认可以显示，设置setEnabled（false）；可以不绘制
        mLineChart.getLegend().setEnabled(true);
        Legend l = mLineChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(6f);
        l.setTextSize(8f);
        l.setXEntrySpace(4f);


        LineData lineData = new LineData(dataSets);
        lineData.setDrawValues(false);
        lineData.setValueTextColor(R.color.resource_bar_y_value_text_color);
        lineData.setValueTextSize(9f);
        lineData.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                int intValue = (int) value;
                return String.valueOf(intValue);
            }
        });

        SceneMarkerView mv = new SceneMarkerView(getContext(), R.layout.custom_marker_view, xLabelList);
        mv.setChartView(mLineChart);
        mLineChart.setMarker(mv);
        mLineChart.setData(lineData);


        //这行代码必须放到这里，这里设置的是图表这个视窗能显示，x坐标轴，从最大值到最小值之间
        //多少段，好像这个库没有办法设置x轴中的间隔的大小
//        mLineChart.setVisibleXRangeMaximum(10);

        if (xLabelList.size() > 11) {
            xAxis.setLabelCount(11);
            //设置一页显示的数据条数，超出的数量需要滑动查看：
            mLineChart.setVisibleXRangeMaximum(11);//需要在设置数据源后生效
        } else {
            xAxis.setLabelCount(xLabelList.size());
        }


        //设置动画效果
        mLineChart.animateY(500, Easing.EasingOption.Linear);
        mLineChart.animateX(500, Easing.EasingOption.Linear);
        mLineChart.invalidate();

    }

    private void initDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        tvEndTime.setText(simpleDateFormat.format(date));

        Calendar  calendar =Calendar. getInstance();
        calendar.add( Calendar. DATE, -6); //向前走一天
        Date date2= calendar.getTime();
        tvStartTime.setText(simpleDateFormat.format(date2));
    }

    private void getWindFileList() {

        String url = UrlUtils.URL_GET_ALL_WIND_FIELD_NAME;

        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());
        showProgressDialog();

        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, null, new HttpListener<MyDialogBean>() {
            @Override
            public void onResponse(MyDialogBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<String> list = response.getList();
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }
                    String windName = response.getList().get(0);
                    tvWindFieldName.setText(windName);
                    LogUtils.d(TAG, "list size: " + list.size());
                    mWindFileList.addAll(list);

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }


    private void getWindGroupList() {

        String url = UrlUtils.URL_GET_ALL_WIND_GROUP_NAME;

        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());


        String windFieldNameTemp = tvWindFieldName.getText().toString();
        if (TextUtils.isEmpty(windFieldNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请先选择风场");
            return;
        }

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(SystemReportConstant.KEY_WIND_FIELD_NAME, windFieldNameTemp);

        showProgressDialog();

        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, jsonStr, new HttpListener<MyDialogBean>() {
            @Override
            public void onResponse(MyDialogBean response) {
                dismissProgressDialog();
                if (null != response) {
                    List<String> list = response.getList();
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }


                    LogUtils.d(TAG, "list size: " + list.size());

                    String groupName = response.getList().get(0);
                    tvWindGroupName.setText(groupName);

                    mWindGroupList.clear();
                    mWindGroupList.addAll(list);

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }

    /**
     * 设置折线图数据的显示
     */
    private static ArrayList<Entry> getLineAxisYLabel(List<Float> list) {
        ArrayList<Entry> mPointValues = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            mPointValues.add(new Entry(i, list.get(i)));
        }
        return mPointValues;
    }


    private static LineDataSet getLineDataSet(List<Entry> list, String des, String colorStr, int shadowId) {

        LineDataSet set = new LineDataSet(list, des);

        set.setLineWidth(1.5f);
        set.setColor(Color.parseColor(colorStr));
        set.setCircleColor(Color.parseColor(colorStr));
        set.setHighLightColor(Color.parseColor(colorStr));
        set.setFillColor(Color.parseColor(colorStr));
        set.setValueTextColor(Color.RED);
        set.setValueTextSize(8f);
        //节点不显示具体数值
        set.setDrawValues(false);
        // 显示的数据值
        set.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.valueOf(value);
            }
        });

//      设置曲线为圆滑的线
        set.setMode(LineDataSet.Mode.LINEAR);
//      设置有圆点
        set.setDrawCircles(true);
        set.setCircleRadius(2.5f);

        //选中某个点的时候高亮显示只是线
//        set.enableDashedHighlightLine(10f, 5f, 0f);
        // TODO: 2019/8/31 去掉填充色
        //填充折线图折线和坐标轴之间
//        set.setDrawFilled(true);

//        Drawable drawable = ContextCompat.getDrawable(MyApplication.context, shadowId);
//        set.setFillDrawable(drawable);

        set.setDrawVerticalHighlightIndicator(false);//取消纵向辅助线
        set.setDrawHorizontalHighlightIndicator(false);//取消横向辅助线
        return set;
    }

    private float str2Float(String str) {
        try {
            if (null != str && !TextUtils.isEmpty(str)) {
                String numberStr = str.trim();
                if (numberStr.contains("%")) {
                    numberStr = str.substring(0, str.indexOf("%"));
                }
                return Float.valueOf(numberStr);
            } else {
                return 0;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @OnClick({R.id.rl_wind_field_name,
            R.id.rl_wind_group_name,
            R.id.rl_start_time,
            R.id.rl_end_time,
            R.id.btn_confirm
    })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_wind_field_name:
                if (mWindFileList.size() <= 0) {
                    ToastUtils.getInstance(getContext()).showMessage("暂无数据");
                    return;
                }
                MyDialogActivityActivity.startActivity(this,
                        MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME, mWindFileList, "风场");
                break;
            case R.id.rl_wind_group_name:
                String windFieldNameTemp = tvWindFieldName.getText().toString();
                if (TextUtils.isEmpty(windFieldNameTemp)) {
                    ToastUtils.getInstance(getContext()).showMessage("请先选择风场");
                    return;
                }

                MyDialogActivityActivity.startActivity(this,
                        MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME, mWindGroupList, "机组");
                break;
            case R.id.rl_start_time:
                DatePickerUtils.showDatePickerDialog(getContext(), tvStartTime, calendar);
                break;
            case R.id.rl_end_time:
                DatePickerUtils.showDatePickerDialog(getContext(), tvEndTime, calendar);
                break;
            case R.id.btn_confirm:
                reloadData();
                break;
        }
    }

    private void reloadData() {

        String windFieldName = tvWindFieldName.getText().toString();
        String windGroupName = tvWindGroupName.getText().toString();
        String startDate = tvStartTime.getText().toString();
        String endDate = tvEndTime.getText().toString();


        if (TextUtils.isEmpty(windFieldName)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择风场");
            return;
        }
        if (TextUtils.isEmpty(windGroupName)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择机组");
            return;
        }
        if (TextUtils.isEmpty(startDate)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择开始日期");
            return;
        }
        if (TextUtils.isEmpty(endDate)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择结束日期");
            return;
        }


        // 日期转换为毫秒值
        long startTime = DateUtils.string2Date(startDate, DateUtils.YMD_FORMAT).getTime();
        long endTime = DateUtils.string2Date(endDate, DateUtils.YMD_FORMAT).getTime();

        if (startTime > endTime) {
            ToastUtils.getInstance(getContext()).showMessage("开始日期不能晚于结束时间！");
            return;
        }

        clearAllDataSets();
        clearAllLineChart();

        loadData(windFieldName, windGroupName, startDate, endDate);
        loadRadarData(windFieldName, windGroupName, startDate, endDate);
    }

    public void clearAllDataSets() {

        shakeDataSets.clear();
        angleDataSets.clear();
//        alarmDataSets.clear();

//        shakeRadarDataSets.clear();
    }

    public void clearAllLineChart() {

        mLineChartShake.clear();
        mLineChartShake.removeAllViews();

        mLineChartAngle.clear();
        mLineChartAngle.removeAllViews();

//        mLineChartAlarm.clear();
//        mLineChartAlarm.removeAllViews();

//        radarChart.clear();
//        radarChart.removeAllViews();
    }


    /**
     * 添加雷达图
     */

    private void loadRadarData(String windFieldName, String windGroupName, String startTime, String endTime) {

        String url = UrlUtils.URL_GET_RADAR_DATA;
        LogUtils.d(TAG, "url: " + url);

        showProgressDialog();
        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(TechnicalAnalysisConstant.KEY_WIND_FIELD_NAME, windFieldName);
        jsonStr.put(TechnicalAnalysisConstant.KEY_WIND_GROUP_NAME, windGroupName);
        jsonStr.put(TechnicalAnalysisConstant.KEY_CURRENT_PAGE, 1);
        jsonStr.put(TechnicalAnalysisConstant.KEY_START_TIME, startTime);
        jsonStr.put(TechnicalAnalysisConstant.KEY_END_TIME, endTime);


        httpRequestManager.httpPostJson(url, RadarBean.class, TAG, header, null, jsonStr, new HttpListener<RadarBean>() {
            @Override
            public void onResponse(RadarBean response) {
                dismissProgressDialog();

                String historyData = "";
                String nowData = "";

                if (null != response) {
                    List<List<RadarBean.MapsBean>> list = response.getMaps();
                    Log.d(TAG, "onResponse: list.size()： " + list.size());
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }

                    // 取第一组数据
                    List<RadarBean.MapsBean> indexOfOne = list.get(0);
                    Log.d(TAG, "onResponse: indexOfOne.size()： " + indexOfOne.size());
                    if (indexOfOne.size() >= 0) {
                        RadarBean.MapsBean newData = indexOfOne.get(0);
                        if (newData != null) {
                            newShake.setText(newData.getHuangdongzhi() == null ? "" : newData.getHuangdongzhi());
                            String newAngleStr = newData.getJiajiao();
                            newAngle.setText(newAngleStr == null ? "" : newAngleStr);
                            float newDegree = Float.parseFloat(newAngleStr);
                            newDirection.setText(getDirection(newDegree));
                            newTime.setText(newData.getTimeOfFilm() == null ? "" : newData.getTimeOfFilm());

                            nowData = "最新晃动值:"+newData.getHuangdongzhi()+"\n"
                                    +"晃动夹角:"+newData.getJiajiao() +"\n"
                                    +"晃动方向:"+getDirection(newDegree) +"\n"
                                    +"晃动时间:"+(newData.getTimeOfFilm() == null ? "" : newData.getTimeOfFilm());

                        }

                    }

                    if (list.size() >= 2) {
                        // 取第2组数据
                        List<RadarBean.MapsBean> indexOfTwo = list.get(1);
                        if (indexOfTwo.size() >= 0) {
                            RadarBean.MapsBean oldData = indexOfTwo.get(0);
                            if (oldData != null) {
                                oldShake.setText(oldData.getHuangdongzhi() == null ? "" : oldData.getHuangdongzhi());
                                String oldAngleStr = oldData.getJiajiao();
                                oldAngle.setText(oldAngleStr == null ? "" : oldAngleStr);
                                float oldDegree = Float.parseFloat(oldAngleStr);
                                oldDirection.setText(getDirection(oldDegree));
                                oldTime.setText(oldData.getTimeOfFilm() == null ? "" : oldData.getTimeOfFilm());

                                historyData = "最新晃动值:"+oldData.getHuangdongzhi()+"\n"
                                        +"晃动夹角:"+oldData.getJiajiao() +"\n"
                                        +"晃动方向:"+getDirection(oldDegree) +"\n"
                                        +"晃动时间:"+(oldData.getTimeOfFilm() == null ? "" : oldData.getTimeOfFilm());
                            }
                        }
                    }

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }

                upDateRadarPic(nowData, historyData);
            }

            @Override
            public void onFailure(String message) {
                LogUtils.d(TAG, "failure: " + message);
                dismissProgressDialog();
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });
    }

    private String getDirection(float degree) {
//        if (degree < 0) {
//            degree = 360 + degree;
//        }
//        if (degree > 360) {
//            degree = 360 - degree;
//        }
        if (degree >= 355 && degree <= 360 || degree >= 0 && degree < 5) {
            return "正北方向";
        } else if (degree >= 5 && degree < 85) {
            return "东北方向";
        } else if (degree >= 85 && degree <= 95) {
            return "正东方向";
        } else if (degree >= 95 && degree < 175) {
            return "东南方向";
        } else if (degree >= 175 && degree <= 185) {
            return "正南方向";
        } else if (degree >= 185 && degree < 265) {
            return "西南方向";
        } else if (degree >= 265 && degree < 275) {
            return "正西方向";
        } else if (degree >= 275 && degree < 355) {
            return "西北方向";
        }
        return "";
    }

    private void upDateRadarPic(String nowData, String historyData){

        try {
            Point center = new Point(450, 350, 0);
            Point old = Point.getPointWithCenter(Float.parseFloat(oldAngle.getText().toString()), Float.parseFloat(oldShake.getText().toString()) *100, center);
            Point now = Point.getPointWithCenter(Float.parseFloat(newAngle.getText().toString()), Float.parseFloat(newShake.getText().toString()) *100, center);

//            Point old = Point.getPointWithCenter(330, 100, center);
//            Point now = Point.getPointWithCenter(0, 0, center);
            YLog.d(TAG, "old point: " + old);
            YLog.d(TAG, "now point: " + now);

            DrawView drawView = new DrawView(getContext());

//            drawView.setMinimumHeight(500);
//            drawView.setMinimumWidth(500);
            drawView.setData(center, old, now, nowData, historyData);

            //通知drawview重绘
            drawView.invalidate();
            container.removeAllViews();
            container.addView(drawView);
        }catch (Exception e){

        }
    }

// TODO: 2019/9/8 去除雷达图
//    public void initRadarChart(RadarChart radarChart, List<String> xLabelList, ArrayList<IRadarDataSet> dataSets) {
//
//
//        tfRegular = Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Regular.ttf");
//        tfLight = Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Light.ttf");
//        mTfBold = Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Bold.ttf");
//
//        radarChart.setBackgroundColor(Color.WHITE);
//        radarChart.getDescription().setEnabled(false);
//
//        radarChart.setWebLineWidth(1f);
//        radarChart.setWebColor(Color.LTGRAY);
//        radarChart.setWebLineWidthInner(1f);
//        radarChart.setWebColorInner(Color.LTGRAY);
//        radarChart.setWebAlpha(100);
//
//        radarChart.animateXY(
//                1400, 1400,
//                Easing.EasingOption.EaseInOutQuad,
//                Easing.EasingOption.EaseInOutQuad);
//
//
//        LogUtils.d(TAG, "initRadarChart: ");
//
//
//
//        XAxis xAxis = radarChart.getXAxis();
//        xAxis.setTypeface(tfLight);
//        xAxis.setTextSize(9f);
//        xAxis.setYOffset(0f);
//        xAxis.setXOffset(0f);
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//
//            private final String[] mActivities = new String[]{"0", "30", "60","90", "120", "150","180", "210", "240","270", "300", "330"};
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return mActivities[(int) value % mActivities.length];
//            }
//        });
//        xAxis.setTextColor(R.color.resource_bar_x_label_text_color);
//
//        YAxis yAxis = radarChart.getYAxis();
//        yAxis.setTypeface(tfLight);
//        yAxis.setTextSize(9f);
//        yAxis.setAxisMinimum(0f);
//        yAxis.setAxisMaximum(80f);
//        yAxis.setDrawLabels(false);
//
//
//
//        Legend l = radarChart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setTypeface(tfLight);
//        l.setXEntrySpace(7f);
//        l.setYEntrySpace(5f);
//        l.setTextColor(R.color.resource_bar_x_label_text_color);
//
//
//        RadarData radarData = new RadarData(dataSets);
//        radarData.setValueTypeface(mTfBold);
//        radarData.setValueTextSize(8f);
//        radarData.setDrawValues(true);
//        radarData.setValueTextColor(R.color.resource_bar_x_label_text_color);
//
//        radarChart.setData(radarData);
//        radarChart.invalidate();
//
//        for (IDataSet<?> set :radarChart.getData().getDataSets()){
//            set.setDrawValues(!set.isDrawValuesEnabled());
//        }
//    }
//
//
//    /**
//     * 设置雷达图数据的显示
//     */
//    private static ArrayList<RadarEntry> getRadarLabel(List<Float> list) {
//        ArrayList<RadarEntry> mPointValues = new ArrayList<>();
//        for (int i = 0; i < list.size(); i++) {
//            mPointValues.add(new RadarEntry(i, list.get(i)));
//        }
//        return mPointValues;
//    }
//
//
//    private static RadarDataSet getRadarDataSet(List<RadarEntry> list, String des, String colorStr, int shadowId) {
//
//        RadarDataSet set = new RadarDataSet(list, des);
//
//        set.setColor(Color.parseColor(colorStr));
//        set.setFillColor(Color.parseColor(colorStr));
//
//        set.setDrawFilled(true);
//        set.setFillAlpha(130);
//        set.setLineWidth(2f);
//        set.setDrawHighlightCircleEnabled(true);
//        set.setDrawHighlightIndicators(false);
//        return set;
//    }

}

