package com.yufuer.electric.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yangkan.commonutillibrary.mobile.utils.DateUtils;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.activity.MyDialogActivityActivity;
import com.yufuer.electric.adapter.SystemReportAdapter;
import com.yufuer.electric.app.MyApplication;
import com.yufuer.electric.base.BaseFragment;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.constant.MyDialogConstant;
import com.yufuer.electric.constant.SystemReportConstant;
import com.yufuer.electric.entity.MyDialogBean;
import com.yufuer.electric.entity.SystemReportBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.utils.DatePickerUtils;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;
import com.yufuer.electric.widget.CustomLinearLayoutManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class SystemReportFragment extends BaseFragment {

    private static final String TAG = "SystemReportFragment";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.tv_wind_field_name)
    TextView tvWindFieldName;
    @BindView(R.id.rl_wind_field_name)
    RelativeLayout rlWindFieldName;
    @BindView(R.id.tv_wind_group_name)
    TextView tvWindGroupName;
    @BindView(R.id.rl_wind_group_name)
    RelativeLayout rlWindGroupName;
    @BindView(R.id.tv_start_time)
    TextView tvStartTime;
    @BindView(R.id.rl_start_time)
    RelativeLayout rlStartTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;
    @BindView(R.id.rl_end_time)
    RelativeLayout rlEndTime;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.btn_previous_page)
    Button btnPreviousPage;
    @BindView(R.id.btn_next_page)
    Button btnNextPage;
    private HttpRequestManager httpRequestManager;

    private Calendar calendar = Calendar.getInstance();
    private SystemReportAdapter mAdapter;
    private boolean isFirst = true;


    private List<SystemReportBean.ListBean> mDataList = new ArrayList<>();

    private ArrayList<String> mWindFileList = new ArrayList<>();
    private ArrayList<String> mWindGroupList = new ArrayList<>();

    private int current_Page = 0;

    String wind_Field_Name = "";
    String wind_Group_Name = "";
    String start_Date = "";
    String end_Date = "";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SystemReportFragment newInstance(String param1, String param2) {
        SystemReportFragment fragment = new SystemReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getContentViewId() {
        return R.layout.fragment_system_report;
    }

    @Override
    protected void initView() {


        httpRequestManager = HttpRequestManager.getInstance();
        initRecyclerView();
        setDefaultDate();
        getWindFileList();


        if (tvWindFieldName != null){
            tvWindFieldName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    getWindGroupList();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        if (tvWindGroupName != null){
            tvWindGroupName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (isFirst){
                        reloadData();
                        isFirst = false;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

    }

    private void setDefaultDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        tvEndTime.setText(simpleDateFormat.format(date));
        tvStartTime.setText(simpleDateFormat.format(date));
    }

    @Override
    protected void setMenuOptions(Menu menu) {

    }

    @Override
    protected void setMenuItemClick(MenuItem item) {

    }

    @OnClick({R.id.rl_wind_field_name,
            R.id.rl_wind_group_name,
            R.id.rl_start_time,
            R.id.rl_end_time,
            R.id.btn_confirm,
            R.id.btn_previous_page,
            R.id.btn_next_page
    })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_wind_field_name:
//                MyDialogActivityActivity.startActivity(this,
//                        MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME,
//                        UrlUtils.URL_GET_ALL_WIND_FIELD_NAME,null);
                if (mWindFileList.size()<=0){
                    ToastUtils.getInstance(getContext()).showMessage("暂无数据");
                    return;
                }
                MyDialogActivityActivity.startActivity(this,
                        MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME,mWindFileList,"风场");
                break;
            case R.id.rl_wind_group_name:
                String windFieldNameTemp = tvWindFieldName.getText().toString();
                if (TextUtils.isEmpty(windFieldNameTemp)) {
                    ToastUtils.getInstance(getContext()).showMessage("请先选择风场");
                    return;
                }

//                Map<String, Object> jsonStr = new HashMap<>();
//                jsonStr.put(SystemReportConstant.KEY_WIND_FIELD_NAME, windFieldNameTemp);
//
//                MyDialogActivityActivity.startActivity(this,
//                        MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME,
//                        UrlUtils.URL_GET_ALL_WIND_GROUP_NAME,null);

                MyDialogActivityActivity.startActivity(this,
                        MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME,mWindGroupList,"机组");
                break;
            case R.id.rl_start_time:
                DatePickerUtils.showDatePickerDialog(getContext(), tvStartTime, calendar);
                break;
            case R.id.rl_end_time:
                DatePickerUtils.showDatePickerDialog(getContext(), tvEndTime, calendar);
                break;
            case R.id.btn_confirm:
                reloadData();
                break;
            case R.id.btn_previous_page:
                getPreviousPageData();
                break;
            case R.id.btn_next_page:
                getNextPageData();
                break;
        }
    }

    private void loadData(String windFieldName, String windGroupName, int currentPage, String startTime, String endTime) {

        String url = UrlUtils.URL_GET_SYETEM_REPORT;
        LogUtils.d(TAG, "url: " + url);


        showProgressDialog();
        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(SystemReportConstant.KEY_WIND_FIELD_NAME, windFieldName);
        jsonStr.put(SystemReportConstant.KEY_WIND_GROUP_NAME, windGroupName);
        jsonStr.put(SystemReportConstant.KEY_SIZE, 10);
        jsonStr.put(SystemReportConstant.KEY_CURRENT_PAGE, currentPage);
        jsonStr.put(SystemReportConstant.KEY_START_TIME, startTime);
        jsonStr.put(SystemReportConstant.KEY_END_TIME, endTime);


        httpRequestManager.httpPostJson(url, SystemReportBean.class, TAG, header, null, jsonStr, new HttpListener<SystemReportBean>() {
            @Override
            public void onResponse(SystemReportBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<SystemReportBean.ListBean> list = response.getList();

                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }
                    LogUtils.d(TAG, "list size: " + list.size());
                    mDataList.addAll(list);
                    LogUtils.d(TAG, "mDataList size: " + list.size());
                    mAdapter.notifyDataSetChanged();

                    // TODO: 2019/9/1  请求数据后，存储参数。这些参数是上一页、下一页点击时候的判断条件

                    current_Page = currentPage;
                    wind_Field_Name = windFieldName;
                    wind_Group_Name = windGroupName;
                    start_Date = startTime;
                    end_Date = endTime;

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                LogUtils.d(TAG, "failure: " + message);
                dismissProgressDialog();
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (httpRequestManager != null) {
            httpRequestManager.cancelRequest(TAG);
        }
    }

    private void reloadData() {

        String windFieldNameTemp = tvWindFieldName.getText().toString();
        String windGroupNameTemp = tvWindGroupName.getText().toString();
        String startDateTemp = tvStartTime.getText().toString();
        String endDateTemp = tvEndTime.getText().toString();

        if (TextUtils.isEmpty(windFieldNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择风场");
            return;
        }
        if (TextUtils.isEmpty(windGroupNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择机组");
            return;
        }
        if (TextUtils.isEmpty(startDateTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择开始日期");
            return;
        }
        if (TextUtils.isEmpty(endDateTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择结束日期");
            return;
        }


        // 日期转换为毫秒值
        long startTime = DateUtils.string2Date(startDateTemp, DateUtils.YMD_FORMAT).getTime();
        long endTime = DateUtils.string2Date(endDateTemp, DateUtils.YMD_FORMAT).getTime();

        if (startTime > endTime) {
            ToastUtils.getInstance(getContext()).showMessage("开始日期不能晚于结束时间！");
            return;
        }

        mDataList.clear();

        loadData(windFieldNameTemp, windGroupNameTemp, 1, startDateTemp, endDateTemp);
    }

    /**
     * 初始话RecyclerView
     */
    private void initRecyclerView() {

        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getContext());
        layoutManager.setScrollEnabled(false);
        recyclerview.setLayoutManager(layoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        recyclerview.setHasFixedSize(true);
        mAdapter = new SystemReportAdapter(R.layout.item_system_report, mDataList);
        recyclerview.setAdapter(mAdapter);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View headerView = inflater.inflate(R.layout.layout_header_system_report, null, false);
        mAdapter.addHeaderView(headerView);
    }

    private void getNextPageData() {

        String windFieldNameTemp = tvWindFieldName.getText().toString();
        String windGroupNameTemp = tvWindGroupName.getText().toString();
        String startDateTemp = tvStartTime.getText().toString();
        String endDateTemp = tvEndTime.getText().toString();
        int currentPageTemp = current_Page;

        if (TextUtils.isEmpty(windFieldNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择风场");
            return;
        }
        if (TextUtils.isEmpty(windGroupNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择机组");
            return;
        }
        if (TextUtils.isEmpty(startDateTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择开始日期");
            return;
        }
        if (TextUtils.isEmpty(endDateTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择结束日期");
            return;
        }


        // 日期转换为毫秒值
        long startTime = DateUtils.string2Date(startDateTemp, DateUtils.YMD_FORMAT).getTime();
        long endTime = DateUtils.string2Date(endDateTemp, DateUtils.YMD_FORMAT).getTime();

        if (startTime > endTime) {
            ToastUtils.getInstance(getContext()).showMessage("开始日期不能晚于结束时间！");
            return;
        }
        if (wind_Field_Name.equals(windFieldNameTemp)
                && windGroupNameTemp.equals(windGroupNameTemp)
                && start_Date.equals(startDateTemp)
                && end_Date.equals(endDateTemp)) {

            currentPageTemp = currentPageTemp + 1;

        } else {
            currentPageTemp = 1;
        }
        mDataList.clear();

        loadData(windFieldNameTemp, windGroupNameTemp, currentPageTemp, startDateTemp, endDateTemp);

    }

    private void getPreviousPageData() {


        String windFieldNameTemp = tvWindFieldName.getText().toString();
        String windGroupNameTemp = tvWindGroupName.getText().toString();
        String startDateTemp = tvStartTime.getText().toString();
        String endDateTemp = tvEndTime.getText().toString();
        int currentPageTemp = current_Page;

        if (TextUtils.isEmpty(windFieldNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择风场");
            return;
        }
        if (TextUtils.isEmpty(windGroupNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择机组");
            return;
        }
        if (TextUtils.isEmpty(startDateTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择开始日期");
            return;
        }
        if (TextUtils.isEmpty(endDateTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请选择结束日期");
            return;
        }


        // 日期转换为毫秒值
        long startTime = DateUtils.string2Date(startDateTemp, DateUtils.YMD_FORMAT).getTime();
        long endTime = DateUtils.string2Date(endDateTemp, DateUtils.YMD_FORMAT).getTime();

        if (startTime > endTime) {
            ToastUtils.getInstance(getContext()).showMessage("开始日期不能晚于结束时间！");
            return;
        }

        if (wind_Field_Name.equals(windFieldNameTemp)
                && windGroupNameTemp.equals(windGroupNameTemp)
                && start_Date.equals(startDateTemp)
                && end_Date.equals(endDateTemp)) {

            currentPageTemp = current_Page - 1;

            if (current_Page <= 1) {
                ToastUtils.getInstance(getContext()).showMessage("当前已是第一页");
                return;
            }

        } else {
            currentPageTemp = 1;
        }


        mDataList.clear();

        loadData(windFieldNameTemp, windGroupNameTemp, currentPageTemp, startDateTemp, endDateTemp);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case MyDialogConstant.REQUEST_CODE_WIND_FILE_NAME:
                if (resultCode == Activity.RESULT_OK) {
                    String windFlieName = data.getStringExtra(MyDialogConstant.KEY_RESULT);
                    tvWindFieldName.setText(windFlieName);

                }
                break;
            case MyDialogConstant.REQUEST_CODE_WIND_GROUP_NAME:
                if (resultCode == Activity.RESULT_OK) {
                    String windGroupName = data.getStringExtra(MyDialogConstant.KEY_RESULT);
                    tvWindGroupName.setText(windGroupName);
                }
                break;
            default:
                break;
        }
    }


    private void getWindFileList() {

        String url = UrlUtils.URL_GET_ALL_WIND_FIELD_NAME;

        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());
        showProgressDialog();

        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, null, new HttpListener<MyDialogBean>() {
            @Override
            public void onResponse(MyDialogBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<String> list = response.getList();
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }
                    String windName = response.getList().get(0);
                    tvWindFieldName.setText(windName);
                    LogUtils.d(TAG, "list size: " + list.size());
                    mWindFileList.addAll(list);

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }


    private void getWindGroupList() {

        String url = UrlUtils.URL_GET_ALL_WIND_GROUP_NAME;

        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());


        String windFieldNameTemp = tvWindFieldName.getText().toString();
        if (TextUtils.isEmpty(windFieldNameTemp)) {
            ToastUtils.getInstance(getContext()).showMessage("请先选择风场");
            return;
        }

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(SystemReportConstant.KEY_WIND_FIELD_NAME, windFieldNameTemp);

        showProgressDialog();

        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, jsonStr, new HttpListener<MyDialogBean>() {
            @Override
            public void onResponse(MyDialogBean response) {
                dismissProgressDialog();
                if (null != response) {
                    List<String> list = response.getList();
                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }


                    LogUtils.d(TAG, "list size: " + list.size());

                    String groupName = response.getList().get(0);
                    tvWindGroupName.setText(groupName);

                    mWindGroupList.clear();
                    mWindGroupList.addAll(list);

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                dismissProgressDialog();
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }
}
