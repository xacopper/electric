package com.yufuer.electric.constant;

/**
 * @author yangkan
 * @date 2018/8/28
 * @description
 */

public class LoginConstant {

    /**
     * 登录相关的 Preferences 文件名
     */
    public static final String LOGIN_PREFERENCE_NAME = "login";

    /**
     * 网络访问 key
     */

    /**
     * 用户名
     */
    public static final String URL_KEY_NAME = "username";
    /**
     * 密码
     */
    public static final String URL_KEY_PASSWORD = "password";

    /**
     * 记住用户
     */
    public static final String URL_KEY_REMEMBER = "rememberMe";


    /**
     * 用户ID
     */
    public static final String URL_KEY_USER_ID = "userId";

    /**
     * 本地SharedPreferences 存储的 Key
     */

    /**
     * 是否已登录，true-已登录， false-未登录
     */
    public static final String SP_KEY_LOGINED = "logined";

    /**
     * 用户 id
     */
    public static final String SP_KEY_USER_ID = "userId";

    /**
     * 用户名
     */
    public static final String SP_KEY_USER_NAME = "userName";

    /**
     * 显示的名字
     */
    public static final String SP_KEY_DISPLAY_NAME = "displayName";

    /**
     * 电子邮箱
     */
    public static final String SP_KEY_USER_MAIL = "userMail";

    /**
     * 电话号码
     */
    public static final String SP_KEY_USER_PHONE = "userPhone";


    /**
     * 公司名
     */
    public static final String SP_KEY_COMPANY_NAME = "companyName";



    /**
     * 用户角色
     */
    public static final String SP_KEY_USER_ROLE = "userRole";

    /**
     * 用户头像链接
     */
    public static final String SP_KEY_USER_AVATAR = "userAvatar";

    /**
     * token
     */
    public static final String SP_KEY_TOKEN = "token";

    /**
     * 记住用户登录状态
     */
    public static final String SP_KEY_REMEMBER_LOGIN_STATUS = "remember_login_status";


    /**
     * 用户登录时间
     */
    public static final String URL_KEY_LOGIN_TIME = "login_time";

    /**
     * token
     * */
    public static final String AUTHORIZATION = "Authorization";
}
