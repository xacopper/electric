package com.yufuer.electric.constant;

public class MyDialogConstant {


    public static final String KEY_URL = "url";

    public static final String KEY_JSON = "json";

    public static final int REQUEST_CODE_WIND_FILE_NAME= 10001;

    public static final int REQUEST_CODE_WIND_GROUP_NAME= 10002;

    public static final String KEY_RESULT = "result";

    public static final String KEY_DATA_LIST = "data";

    public static final String KEY_TITLE = "TITLE";
}
