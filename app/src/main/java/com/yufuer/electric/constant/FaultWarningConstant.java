package com.yufuer.electric.constant;

public class FaultWarningConstant {


    public static final String KEY_WIND_FIELD_NAME = "wind_field_name";

    public static final String KEY_WIND_GROUP_NAME = "wind_group_name";

    public static final String KEY_CURRENT_PAGE = "currentpage";

    public static final String KEY_SIZE = "pagesize";

    public static final String KEY_START_TIME = "start_time";

    public static final String KEY_END_TIME = "end_time";
}
