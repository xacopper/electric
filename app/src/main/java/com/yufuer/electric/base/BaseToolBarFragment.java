package com.yufuer.electric.base;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;


import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.widget.PersonalInfoPopup;

import java.lang.reflect.Method;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseToolBarFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();
    private Unbinder unbinder;
    private ProgressDialog mProgressDialog;
    private PersonalInfoPopup popup;


    /**
     * 获取布局文件
     *
     * @return
     */
    protected abstract int getContentViewId();

    /**
     * 初始化View
     */
    protected abstract void initView();

    /**
     * 设置Menu属性
     *
     * @param menu
     */
    protected abstract void setMenuOptions(Menu menu);

    /**
     * 设置MenuItem点击事件
     *
     * @param item
     */
    protected abstract void setMenuItemClick(MenuItem item);


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtils.d(TAG, "onCreateView");
        View rootView = inflater.inflate(getContentViewId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initView();
        popup = new PersonalInfoPopup(getContext());
        popup.setPopupGravity(Gravity.RIGHT);
        return rootView;
    }

    @Override
    public void onResume() {
        LogUtils.d(TAG, "onResume");
        super.onResume();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.toolbar_menu, menu);
//        setMenuOptions(menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.bar_user:
//                popup.showPopupWindow(R.id.bar_user);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    protected void setToolBar(Toolbar toolBar) throws Exception {
        setHasOptionsMenu(true);

        AppCompatActivity context = (AppCompatActivity) getActivity();
        if (null == context) {
            throw new Exception("AppCompatActivity context is null");
        }

//        context.setSupportActionBar(toolBar);
        toolBar.inflateMenu(R.menu.toolbar_menu);
//      设置是否添加显示NavigationIcon.如果要用
//      设置NavigationIcon的icon.可以是Drawable ,也可以是ResId
//        toolBar.setNavigationIcon(R.drawable.home_btn_more);

//      设置显示logo
//        context.getSupportActionBar().setDisplayUseLogoEnabled(false);
//        getSupportActionBar().setLogo(R.mipmap.ic_launcher);

//      显示标题和子标题
//        context.getSupportActionBar().setDisplayShowTitleEnabled(false);
//
//        context.getSupportActionBar().setHomeButtonEnabled(true);
//        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.bar_user) {
                    popup.showPopupWindow(R.id.bar_user);
                } else {
                    setMenuItemClick(item);
                }
                return true;
            }
        });


    }

    protected void setToolBarTitle(TextView view, String title) {
        view.setText(title);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "onMenuOpened...unable to set icons for overflow menu", e);
                }
            }
        }

        super.onPrepareOptionsMenu(menu);
    }



    protected void showProgressDialog() {
        Log.d(TAG, "showProgressDialog, mProgressDialog: " + mProgressDialog);
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), null, getResources().getString(R.string.loading_message));
            mProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        mProgressDialog.dismiss();
                    }
                    return false;
                }
            });
        } else {
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
