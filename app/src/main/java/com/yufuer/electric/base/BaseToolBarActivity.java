package com.yufuer.electric.base;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.yangkan.commonutillibrary.mobile.manage.ActivityStackManager;
import com.yufuer.electric.R;
import com.yufuer.electric.widget.PersonalInfoPopup;

import java.lang.reflect.Method;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.ButterKnife;

public abstract class BaseToolBarActivity extends AppCompatActivity {

    private static final String TAG = "BaseToolBarActivity";

    private ProgressDialog mProgressDialog;
    private PersonalInfoPopup popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());
        ButterKnife.bind(this);
        ActivityStackManager.getInstance().pushActivity(this);
        initViews(savedInstanceState);
        popup = new PersonalInfoPopup(this);
        popup.setPopupGravity(Gravity.RIGHT);

    }

    /**
     * 获取布局文件
     *
     * @return
     */
    protected abstract int getContentViewId();

    /**
     * 初始化View
     *
     * @param savedInstanceState
     */
    protected abstract void initViews(Bundle savedInstanceState);

    protected abstract void setMenuOptions(Menu menu);

    protected abstract void setMenuItemClick(MenuItem item);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        setMenuOptions(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bar_user:
                popup.showPopupWindow(R.id.bar_user);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityStackManager.getInstance().finishActivity(this);
    }

    protected void setToolBar(Toolbar toolBar) {

        setSupportActionBar(toolBar);
//      设置是否添加显示NavigationIcon.如果要用
//      设置NavigationIcon的icon.可以是Drawable ,也可以是ResId
//        toolBar.setNavigationIcon(R.drawable.home_btn_more);

//      设置显示logo
        getSupportActionBar().setDisplayUseLogoEnabled(false);
//        getSupportActionBar().setLogo(R.mipmap.ic_launcher);

//      显示标题和子标题
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.bar_user) {
                    popup.showPopupWindow(R.id.bar_user);
                } else {
                    setMenuItemClick(item);
                }
                return true;
            }
        });
    }

    protected void setToolBarTitle(TextView view, String title) {
        view.setText(title);
    }

    /**
     * 每次菜单被关闭时调用.
     * 菜单被关闭有三种情形:
     * 1.展开menu的按钮被再次点击
     * 2.back按钮被点击
     * 3.用户选择了某一个菜单项
     */
    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    /**
     * 在onCreateOptionsMenu执行后，菜单被显示前调用；如果菜单已经被创建，则在菜单显示前被调用。 同样的，
     * 返回true则显示该menu,false 则不显示; （可以通过此方法动态的改变菜单的状态，比如加载不同的菜单等）
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * 显示menu的icon,通过反射,设置Menu的icon显示.
     *
     * @param view
     * @param menu
     * @return
     */
    @SuppressLint("RestrictedApi")
    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "onMenuOpened...unable to set icons for overflow menu", e);
                }
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }


    protected void showProgressDialog() {
        Log.d(TAG, "showProgressDialog, mProgressDialog: " + mProgressDialog);
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.loading_message));
            mProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        mProgressDialog.dismiss();
                    }
                    return false;
                }
            });
        } else {
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
