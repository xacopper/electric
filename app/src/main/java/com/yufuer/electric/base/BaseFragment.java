package com.yufuer.electric.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.widget.PersonalInfoPopup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();

    private Unbinder unbinder;
    private ProgressDialog mProgressDialog;
    private PersonalInfoPopup popup;


    /**
     * 获取布局文件
     *
     * @return
     */
    protected abstract int getContentViewId();

    /**
     * 初始化View
     */
    protected abstract void initView();

    /**
     * 设置Menu属性
     *
     * @param menu
     */
    protected abstract void setMenuOptions(Menu menu);

    /**
     * 设置MenuItem点击事件
     *
     * @param item
     */
    protected abstract void setMenuItemClick(MenuItem item);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtils.d(TAG, "onCreateView");
        View rootView = inflater.inflate(getContentViewId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initView();
        return rootView;
    }

    @Override
    public void onResume() {
        LogUtils.d(TAG, "onResume");
        super.onResume();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    protected void showProgressDialog() {
        Log.d(TAG, "showProgressDialog, mProgressDialog: " + mProgressDialog);
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), null, getResources().getString(R.string.loading_message));
            mProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        mProgressDialog.dismiss();
                    }
                    return false;
                }
            });
        } else {
            mProgressDialog.show();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
