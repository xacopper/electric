package com.yufuer.electric.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.TextView;

import com.yangkan.commonutillibrary.mobile.utils.DateUtils;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;

import java.util.Calendar;

public class DatePickerUtils {

    private static final String TAG = "DatePickerUtils";

    public static void showDatePickerDialog(Context context, TextView textView, final Calendar calendar) {

        DatePickerDialog dialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        LogUtils.d(TAG, "onDateSet: year: " + year + ", month: " + month + ", dayOfMonth: " + dayOfMonth);
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String time = DateUtils.formatDateTimeMill(calendar.getTimeInMillis(), DateUtils.YMD_FORMAT);

                        if (textView != null) {
                            textView.setText(time);
                        }
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }



}
