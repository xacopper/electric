package com.yufuer.electric.utils;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WindUtils {

    private static ArrayList<String> mWindList = new ArrayList<String>();
    private static HashMap<String, ArrayList<String>> mWindGroupMap = new HashMap<String, ArrayList<String>>();

    public static void setmWindList(ArrayList<String> list){
        mWindList = list;
    }

    public static void setmWindGroupMap(String wind, ArrayList<String> list){

        if (!mWindList.contains(wind)){
            return;
        }

        if (!TextUtils.isEmpty(wind) && list != null){
            mWindGroupMap.put(wind, list);
        }
    }

    public static ArrayList<String>  getmWindGroupList(String wind){

        if (!TextUtils.isEmpty(wind)){
            return  mWindGroupMap.get(wind);
        }

        return null;
    }

    public static ArrayList <String> getmWindList(){
        return mWindList;
    }

}
