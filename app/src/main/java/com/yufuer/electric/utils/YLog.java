package com.yufuer.electric.utils;

import android.util.Log;

public class YLog {

    private final static String TAG = "Eelctric";

    public static void d(String tag, String info){

        Log.d(TAG, "["+tag+"]"+info);
    }

    public static void e(String tag, String info){

        Log.e(TAG, "["+tag+"]"+info);
    }
}
