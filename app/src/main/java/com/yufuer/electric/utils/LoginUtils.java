package com.yufuer.electric.utils;


import com.yangkan.commonutillibrary.mobile.utils.SPUtils;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.entity.UserBean;

/**
 * @author yangkan
 * @date 2019/8/28
 * @description
 */

public class LoginUtils {

    /**
     * 是否已登录
     */
    private static boolean isLogined;

    /**
     * 登录，存储用户信息到本地 SharedPreferences
     *
     * @param userBean
     */
    public static void login(UserBean userBean,String password,String rememberStatus) {
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_LOGINED, true);
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_NAME, userBean.getUser());
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.URL_KEY_PASSWORD, password);
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_ROLE, userBean.getRole());
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.URL_KEY_LOGIN_TIME, userBean.getLogintime());
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_TOKEN, userBean.getToken());
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_REMEMBER_LOGIN_STATUS, rememberStatus);
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_LOGINED, true);
        isLogined = true;
    }

    /**
     * 登出，清空本地用户数据
     */
    public static void logout() {
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_LOGINED, false);
        SPUtils.clear(LoginConstant.LOGIN_PREFERENCE_NAME);
        isLogined = false;
    }

    /**
     * 返回用户是否已经登录
     *
     * @return
     */
    public static boolean isLogined() {
        isLogined = SPUtils.getBoolean(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_LOGINED);
        return isLogined;
    }

    /**
     * 获取用户ID UserId
     *
     * @return
     */
    public static String getUserId() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_ID);
    }

    /**
     * 获取用户名 UserName
     *
     * @return
     */
    public static String getUserName() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_NAME);
    }

    /**
     * 获取用户名密码
     *
     * @return
     */
    public static String getPassword() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.URL_KEY_PASSWORD);
    }


    /**
     * 获取用户角色 UserRole
     *
     * @return
     */
    public static String getUserRole() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_ROLE);
    }
    /**
     * 获取是否记住用户名
     *
     * @return
     */
    public static String getRememberLoginStatus() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_REMEMBER_LOGIN_STATUS);
    }
    /**
     * 获取用户显示名 DisplayName
     *
     * @return
     */
    public static String getDisplayName() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_DISPLAY_NAME);
    }

    /**
     * 获取用户电子邮箱 UserMail
     *
     * @return
     */
    public static String getUserMail() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_MAIL);
    }

    /**
     * 获取用户电话 UserPhone
     *
     * @return
     */
    public static String getUserPhone() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_PHONE);
    }


    /**
     * 获取用户头像链接 UserAvatar
     *
     * @return
     */
    public static String getUserAvatar() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_AVATAR);
    }

    /**
     * 获取用户授权码 Token
     *
     * @return
     */
    public static String getToken() {
        return SPUtils.getString(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_TOKEN);
    }

    /**
     * 设置用户头像
     *
     * @param avatar
     */
    public static void setUserAvatar(String avatar) {
        SPUtils.put(LoginConstant.LOGIN_PREFERENCE_NAME, LoginConstant.SP_KEY_USER_AVATAR, avatar);
    }
}
