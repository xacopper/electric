package com.yufuer.electric.utils;

/**
 * @author yangkan
 * @date 2019/8/28
 * @description URL管理类
 */

public final class UrlUtils {

    private static final boolean IS_DEBUG = true;

    private static final String URL_HOST_TEST = "http://119.3.251.253:8002/";

    private static final String URL_HOST_RELEASE = "";

    private static final String URL_HOST =
            IS_DEBUG ? URL_HOST_TEST : URL_HOST_RELEASE;

    private static final String APP = "app/";
    private static final String API = "api/";


    /**
     * 登录
     */
    public static final String URL_LOGIN = URL_HOST  + "auth/login";

    /*登出*/
    public static final String URL_LOGOUT = URL_HOST + API + "userInfo/Logoff";

    /**
     * 折线图数据
     */
    public static final String URL_GET_CHART = URL_HOST + "equipmentConfig/getEquipmentZhexianInfoList";

    /**
     * 获取所有已授权风场名称
     */
    public static final String URL_GET_ALL_WIND_FIELD_NAME = URL_HOST  + "equipmentstatus/getAllEquipmentWindfieldName";

    /**
     * 获取风场个数
     */
    public static final String URL_GET_ALL_WIND_FIELD_COUNT= URL_HOST  + "equipmentstatus/getEquipmentWindfieldCount";

    /**
     * 获取风场所有风组名称
     */
    public static final String URL_GET_ALL_WIND_GROUP_NAME = URL_HOST  + "equipmentstatus/getAllWindGroupName";

    /**
     * 获取风场机组个数
     */
    public static final String URL_GET_ALL_WIND_GROUP_COUNT= URL_HOST  + "equipmentstatus/getEquipmentWindGroupCount";

    /**
     * 获取所有风场正常运行设备个数
     */
    public static final String URL_GET_NORMAL_EQUIPMENT_COUNT = URL_HOST  + "equipmentstatus/getEquipmentNormalCount";

    /**
     * 获取长期离线设备个数
     */
    public static final String URL_GET_OFFLINE_EQUIPMENT_COUNT = URL_HOST  + "equipmentstatus/getEquipmentOfflineCount";

    /**
     * 获取风场传感器个数
     */
    public static final String URL_GET_EQUIPMENT_ID_COUNT = URL_HOST  + "equipmentstatus/getEquipmentIdCount";

    /**
     * 获取风场故障警告设备个数
     */
    public static final String URL_GET_WARN_EQUIPMENT_COUNT = URL_HOST  + "equipmentstatus/getEquipmentWarnCount";

    /**
     * 获取系统报表
     */
    public static final String URL_GET_SYETEM_REPORT = URL_HOST  + "equipmentConfig/getExportEquipmentInfo";

    /**
     * 获取设备查询
     */
    public static final String URL_GET_DEVICE_QUERY = URL_HOST  + "equipmentConfig/getEquipmentConfigList";

    /**
     * 获取故障警告
     */
    public static final String URL_GET_FAULT_WARN = URL_HOST  + "equipmentstatus/getEquipmentWarnTalbe";

    /**
     * 获取机组设备列表
     */
    public static final String URL_GET_WIND_GROUP = URL_HOST  + "equipmentstatus/getEquipmentWarnAndNormalTalbe";


    /**
     * 获取用户信息
     */
    public static final String URL_GET_USER_INFO = URL_HOST  + "user/selectCurrentUser";

    /**
     * 获取机组状态
     */
    public static final String URL_GET_WIND_STATUS = URL_HOST  + "equipmentstatus/getEquipmentWindfieldStatus";


    /**
     * 雷达图数据
     */
    public static final String URL_GET_RADAR_DATA = URL_HOST + "equipmentConfig/getMaxAndNewHuangDongzhi";

}
