package com.yufuer.electric.adapter;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yufuer.electric.R;
import com.yufuer.electric.utils.YLog;

import java.util.List;

public class DataAdpter extends RecyclerView.Adapter<DataAdpter.ViewHolder> {

    private final static String TAG = "DataAdpter";
    private OnItemClickListener onItemClickListener = null;
    private List<String> mDataList = null;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        if (onItemClickListener != null){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YLog.d(TAG, "onItem click ");
                    onItemClickListener.onItemClick(viewHolder.getText());
                }
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (mDataList != null){
            String data = mDataList.get(position);
            holder.itemName.setText(data);
        }
    }

    private void printList(){

        if (mDataList != null){
            for (int i=0; i<mDataList.size(); i++){
                YLog.d(TAG, "mDataList has: " + mDataList.get(i));
            }
        }

    }

    public DataAdpter(List<String> list){

        printList();
        mDataList = list;
    }

    @Override
    public int getItemCount() {

        if (mDataList == null){
            return 0;
        }
        YLog.d(TAG, "mDataList count: " + mDataList.size());
        return mDataList.size();
    }

    static class ViewHolder extends  RecyclerView.ViewHolder{

        TextView itemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
//            fruitView=itemView;   监听相关
            itemName=itemView.findViewById(R.id.item_title);
        }

        public String getText(){
            String text = "";

            if (itemName != null){
                text = itemName.getText().toString();
            }

            return text;
        }
    }

    //setter方法
    public void setOnItemClickListener(OnItemClickListener onClickListener) {
        this.onItemClickListener = onClickListener;
    }

    //回调接口
    public interface OnItemClickListener {
        void onItemClick(String value);
    }

}
