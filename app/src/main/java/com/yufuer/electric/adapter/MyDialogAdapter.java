package com.yufuer.electric.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yufuer.electric.R;

import java.util.List;

public class MyDialogAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private static final String TAG = "SystemReportAdapter";

    public MyDialogAdapter(int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
        LogUtils.d(TAG, "mDataList size: " + data.size());
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {


        helper.setText(R.id.tv_value, item + "");

    }
}
