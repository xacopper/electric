package com.yufuer.electric.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.entity.DeviceQueryBean;
import com.yufuer.electric.entity.SystemReportBean;

import java.util.List;

import androidx.annotation.Nullable;

public class DeviceQueryAdapter extends BaseQuickAdapter<DeviceQueryBean.ListBean, BaseViewHolder> {

    private static final String TAG = "DeviceQueryAdapter";

    public DeviceQueryAdapter(int layoutResId, @Nullable List<DeviceQueryBean.ListBean> data) {
        super(layoutResId, data);
        LogUtils.d(TAG, "mDataList size: " + data.size());
    }

    @Override
    protected void convert(BaseViewHolder helper, DeviceQueryBean.ListBean item) {


        helper.setText(R.id.tv_1, item.getRowno() + "");//序号


        helper.setText(R.id.tv_2, item.getWind_field_name());//风场名称
        helper.setText(R.id.tv_3, item.getWind_group_name());//风组名称
        helper.setText(R.id.tv_4, item.getEquipment_id());//传感器id
        helper.setText(R.id.tv_5, item.getInstall_location());//安装位置
        helper.setText(R.id.tv_6, item.getInstall_high());//安装高度

        helper.setText(R.id.tv_7, item.getThreshold_value());//晃动值阈值
        helper.setText(R.id.tv_8, item.getWind_admin());//风场管理员
        helper.setText(R.id.tv_9, item.getRemark());//备注

        if ((helper.getAdapterPosition()%2) == 0){
            helper.itemView.setBackgroundColor(Color.rgb(245,245,245));
        }else {
            helper.itemView.setBackgroundColor(Color.rgb(250,250,250));
        }

    }
}
