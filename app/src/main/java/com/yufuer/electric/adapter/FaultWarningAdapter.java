package com.yufuer.electric.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.entity.FaultWarningBean;

import java.util.List;

import androidx.annotation.Nullable;

public class FaultWarningAdapter extends BaseQuickAdapter<FaultWarningBean.ListBean, BaseViewHolder> {

    private static final String TAG = "SystemReportAdapter";

    public FaultWarningAdapter(int layoutResId, @Nullable List<FaultWarningBean.ListBean> data) {
        super(layoutResId, data);
        LogUtils.d(TAG, "mDataList size: " + data.size());
    }

    @Override
    protected void convert(BaseViewHolder helper, FaultWarningBean.ListBean item) {

        helper.setText(R.id.tv_1, item.getRowno() + "");//序号
        helper.setText(R.id.tv_2, item.getWind_field_name());//风场名称
        helper.setText(R.id.tv_3, item.getWind_group_name());//风组名称
        helper.setText(R.id.tv_4, item.getEquipment_id());//传感器id
        helper.setText(R.id.tv_5, item.getTimeOfFilm());//上报时间
        helper.setText(R.id.tv_6, item.getJiajiaox());//x轴夹角
        helper.setText(R.id.tv_7, item.getJiajiaoy());//y轴夹角
        helper.setText(R.id.tv_8, item.getQingjiao());//倾角
        helper.setText(R.id.tv_9, item.getHuangdongzhi());//晃动值（米）

        if ((helper.getAdapterPosition()%2) == 0){
            helper.itemView.setBackgroundColor(Color.rgb(245,245,245));
        }else {
            helper.itemView.setBackgroundColor(Color.rgb(250,250,250));
        }
    }
}
