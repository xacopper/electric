package com.yufuer.electric.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yufuer.electric.R;
import com.yufuer.electric.entity.WindGroupBean;
import com.yufuer.electric.utils.YLog;

import java.util.ArrayList;

public class WindGroupAdpler extends RecyclerView.Adapter<WindGroupAdpler.ViewHolder>{

    private final static String TAG = "WindGroupAdpler";

    private WindGroupAdpler.OnItemClickListener onItemClickListener = null;
    private ArrayList<WindGroupBean.ListBean> mDataList = null;

    public WindGroupAdpler(ArrayList<WindGroupBean.ListBean> list){
        this.mDataList =  list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_button, parent, false);
        final WindGroupAdpler.ViewHolder viewHolder = new WindGroupAdpler.ViewHolder(view);

        if (onItemClickListener != null){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YLog.d(TAG, "onItem click ");
                    onItemClickListener.onItemClick(viewHolder.getText());
                }
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mDataList != null){
            WindGroupBean.ListBean data = mDataList.get(position);
            String text = data.getWind_field_name()+"-"+data.getWind_group_name();
            YLog.d(TAG, "set Text: " +text);
            holder.itemName.setText(text);
            if ("1".equals(data.getNormal())){
                holder.itemName.setBackgroundColor(Color.RED);
            }
        }
    }

    @Override
    public int getItemCount() {

        if (mDataList !=null){
            return mDataList.size();
        }
        return 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView itemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
//            fruitView=itemView;   监听相关
            itemName=itemView.findViewById(R.id.group_item);
        }

        public String getText(){
            String text = "";

            if (itemName != null){
                text = itemName.getText().toString();
            }

            return text;
        }
    }

    //setter方法
    public void setOnItemClickListener(WindGroupAdpler.OnItemClickListener onClickListener) {
        this.onItemClickListener = onClickListener;
    }

    //回调接口
    public interface OnItemClickListener {
        void onItemClick(String value);
    }
}
