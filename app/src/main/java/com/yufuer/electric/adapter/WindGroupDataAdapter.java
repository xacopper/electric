package com.yufuer.electric.adapter;

import android.graphics.Color;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.entity.FaultWarningBean;
import com.yufuer.electric.entity.WindGroupDataBean;
import com.yufuer.electric.utils.YLog;

import java.util.List;

public class WindGroupDataAdapter extends BaseQuickAdapter<WindGroupDataBean.ListBean, BaseViewHolder> {

    private static final String TAG = "WindGroupDataAdapter";

    public WindGroupDataAdapter(int layoutResId, @Nullable List<WindGroupDataBean.ListBean> data) {
        super(layoutResId, data);
        LogUtils.d(TAG, "mDataList size: " + data.size());
    }

    @Override
    protected void convert(BaseViewHolder helper, WindGroupDataBean.ListBean item) {


        YLog.d(TAG, "get normal: " + item.getNormal());
        helper.setText(R.id.tv_1, item.getRowno() + "");//序号
        helper.setText(R.id.tv_2, item.getWind_group_name());//风电机组名称
        helper.setText(R.id.tv_3, item.getEquipment_id());//传感器ID
        helper.setText(R.id.tv_4, item.getTimeOfFilm());//上报时间
        helper.setText(R.id.tv_5, item.getNormal()+"");// 是否警告
        helper.setText(R.id.tv_6, item.getJiajiaox());//x轴夹角
        helper.setText(R.id.tv_7, item.getJiajiaoy());//y轴夹角
        helper.setText(R.id.tv_8, item.getQingjiao());//倾角
        helper.setText(R.id.tv_9, item.getHuangdongzhi());//晃动值（米）
        helper.setText(R.id.tv_10, item.getNetSignal());//信号）

        if ((helper.getAdapterPosition()%2) == 0){
            helper.itemView.setBackgroundColor(Color.rgb(245,245,245));
        }else {
            helper.itemView.setBackgroundColor(Color.rgb(250,250,250));
        }
    }
}
