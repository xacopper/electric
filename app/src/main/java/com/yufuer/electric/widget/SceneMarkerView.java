
package com.yufuer.electric.widget;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.yufuer.electric.R;

import java.text.DecimalFormat;
import java.util.List;


public class SceneMarkerView extends MarkerView {
    private List<String> xLabelList;
    private TextView tvContent;
    private DecimalFormat format = new DecimalFormat("##0");

    public SceneMarkerView(Context context, int layoutResource, List<String> xLabelList) {
        super(context, layoutResource);
        this.xLabelList = xLabelList;
        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        String xLabel = "";
        if (xLabelList.size() > (int) e.getX()) {
            xLabel = xLabelList.get((int) e.getX());
        }

        tvContent.setText("时间：" + xLabel + "\n" + "值：" + e.getY());

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }


}
