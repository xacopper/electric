package com.yufuer.electric.widget;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.yangkan.commonutillibrary.mobile.manage.ActivityStackManager;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.LoginActivity;
import com.yufuer.electric.R;
import com.yufuer.electric.app.MyApplication;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.entity.PersonalInfoBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;

import java.util.HashMap;
import java.util.Map;

import razerdp.basepopup.BasePopupWindow;

public class PersonalInfoPopup extends BasePopupWindow {

    private static final String TAG = "PersonalInfoPopup";

    private HttpRequestManager httpRequestManager;
    private TextView username;
    private TextView fireName;
    private TextView mailbox;
    private TextView phone;
    private TextView permission;
    private Button button;
    public PersonalInfoPopup(Context context) {
        super(context);

    }

    @Override
    public View onCreateContentView() {

        View view = createPopupById(R.layout.layout_personal_info);
        username = view.findViewById(R.id.tv_user_name);
        fireName = view.findViewById(R.id.tv_firm_name);
        mailbox = view.findViewById(R.id.tv_mailbox);
        phone = view.findViewById(R.id.tv_phone);
        permission = view.findViewById(R.id.tv_permission);
        button = view.findViewById(R.id.btn_logout);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                LoginUtils.logout();
                ActivityStackManager.getInstance().finishAllActivity();
                LoginActivity.startActivity(MyApplication.context);
            }
        });

        return view;
    }

    // 以下为可选代码（非必须实现）
    // 返回作用于PopupWindow的show和dismiss动画，本库提供了默认的几款动画，这里可以自由实现
    @Override
    protected Animation onCreateShowAnimation() {
        return getDefaultScaleAnimation(true);
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return getDefaultScaleAnimation(false);
    }

    private void loadData() {

        String url = UrlUtils.URL_GET_USER_INFO;
        LogUtils.d(TAG, "url: " + url);

        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());

        httpRequestManager = HttpRequestManager.getInstance();

        httpRequestManager.httpPostJson(url, PersonalInfoBean.class, TAG, header, null, null, new HttpListener<PersonalInfoBean>() {
            @Override
            public void onResponse(PersonalInfoBean response) {

                if (null != response) {
                    username.setText(response.getUsername()==null?"":response.getUsername());
                    fireName.setText(response.getCompany()==null?"":response.getCompany());
                    mailbox.setText(response.getEmail()==null?"":response.getEmail());
                    phone.setText(response.getPhone()==null?"":response.getPhone());
                    permission.setText(response.getRole()==null?"":response.getRole());

                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                LogUtils.d(TAG, "failure: " + message);
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }

    @Override
    public void showPopupWindow(View anchorView) {
        super.showPopupWindow(anchorView);
        loadData();
    }


    @Override
    public void dismiss() {
        super.dismiss();
        if (httpRequestManager != null) {
            httpRequestManager.cancelRequest(TAG);
        }
    }
}
