package com.yufuer.electric.app;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.lzy.okgo.OkGo;

import com.yangkan.commonutillibrary.mobile.utils.SPUtils;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.http.OkHttpStrategy;


/**
 * @author yangkan
 * @date 2019/8/28
 * @description
 */

public class MyApplication extends Application {

    private static final String TAG = "MyApplication";
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        initConfig(context);
        initOkGo();
    }


    /**
     * 初始化OkGo,上传下载库
     */
    private void initOkGo() {
        OkGo.getInstance().init(this)
                .setRetryCount(1);
    }

    /**
     * 初始化相关配置
     */
    private void initConfig(Context context) {
        SPUtils.init(context);

        HttpRequestManager.getInstance().init(context);
        HttpRequestManager.getInstance().setHttpStrategy(
                new OkHttpStrategy(new Handler(Looper.getMainLooper())));
        initLogConfig();

    }

    /**
     * 日志配置
     * Log.VERBOSE 表示打印所有日志, 即VERBOSE, DEBUG, INFO, WARN, ERROR 日志
     * Log.DEBUG 表示打印DEBUG, INFO, WARN, ERROR 日志
     * Log.INFO 表示打印INFO, WARN, ERROR 日志
     * Log.WARN 表示打印WARN, ERROR 日志
     * Log.ERROR 表示打印ERROR 日志
     * Log.ERROR + 1 表示不打印所有日志
     */
    private void initLogConfig() {
        com.yangkan.commonutillibrary.mobile.utils.LogUtils.LOG_LEVEL = Log.VERBOSE;
    }



}
