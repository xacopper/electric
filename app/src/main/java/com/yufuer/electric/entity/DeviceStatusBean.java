package com.yufuer.electric.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeviceStatusBean {


    /**
     * list : [{"normal":"0","rowno":0,"timeOfFilm":"","wind_field_name":"风场1","wind_group_name":"风组1","huangdongzhi":"","qingjiao":"","totalCount":0,"netSignal":"","equipment_id":""}]
     */
    @SerializedName("list")
    private List<ListEntity> list;

    public void setList(List<ListEntity> list) {
        this.list = list;
    }

    public List<ListEntity> getList() {
        return list;
    }

    public class ListEntity {
        /**
         * normal : 0
         * rowno : 0
         * timeOfFilm :
         * wind_field_name : 风场1
         * wind_group_name : 风组1
         * huangdongzhi :
         * qingjiao :
         * totalCount : 0
         * netSignal :
         * equipment_id :
         */
        @SerializedName("normal")
        private String normal;
        @SerializedName("rowno")
        private int rowno;
        @SerializedName("timeOfFilm")
        private String timeOfFilm;
        @SerializedName("wind_field_name")
        private String wind_field_name;
        @SerializedName("wind_group_name")
        private String wind_group_name;
        @SerializedName("huangdongzhi")
        private String huangdongzhi;
        @SerializedName("qingjiao")
        private String qingjiao;
        @SerializedName("totalCount")
        private int totalCount;
        @SerializedName("netSignal")
        private String netSignal;
        @SerializedName("equipment_id")
        private String equipment_id;

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public void setRowno(int rowno) {
            this.rowno = rowno;
        }

        public void setTimeOfFilm(String timeOfFilm) {
            this.timeOfFilm = timeOfFilm;
        }

        public void setWind_field_name(String wind_field_name) {
            this.wind_field_name = wind_field_name;
        }

        public void setWind_group_name(String wind_group_name) {
            this.wind_group_name = wind_group_name;
        }

        public void setHuangdongzhi(String huangdongzhi) {
            this.huangdongzhi = huangdongzhi;
        }

        public void setQingjiao(String qingjiao) {
            this.qingjiao = qingjiao;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public void setNetSignal(String netSignal) {
            this.netSignal = netSignal;
        }

        public void setEquipment_id(String equipment_id) {
            this.equipment_id = equipment_id;
        }

        public String getNormal() {
            return normal;
        }

        public int getRowno() {
            return rowno;
        }

        public String getTimeOfFilm() {
            return timeOfFilm;
        }

        public String getWind_field_name() {
            return wind_field_name;
        }

        public String getWind_group_name() {
            return wind_group_name;
        }

        public String getHuangdongzhi() {
            return huangdongzhi;
        }

        public String getQingjiao() {
            return qingjiao;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public String getNetSignal() {
            return netSignal;
        }

        public String getEquipment_id() {
            return equipment_id;
        }
    }
}
