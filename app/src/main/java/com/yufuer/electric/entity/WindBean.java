package com.yufuer.electric.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WindBean {
    /**
     * list : ["风场1"]
     */
    @SerializedName("list")
    private ArrayList<String> list;

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public ArrayList<String> getList() {
        return list;
    }
}
