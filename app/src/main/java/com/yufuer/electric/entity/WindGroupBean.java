package com.yufuer.electric.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class WindGroupBean implements Parcelable {


    private ArrayList<ListBean> list;

    protected WindGroupBean(Parcel in) {
    }

    public static final Creator<WindGroupBean> CREATOR = new Creator<WindGroupBean>() {
        @Override
        public WindGroupBean createFromParcel(Parcel in) {
            return new WindGroupBean(in);
        }

        @Override
        public WindGroupBean[] newArray(int size) {
            return new WindGroupBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public ArrayList<ListBean> getList() {
        return list;
    }

    public void setList(ArrayList<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * equipment_id :
         * huangdongzhi :
         * jiajiaox :
         * jiajiaoy :
         * netSignal :
         * normal : 0
         * qingjiao :
         * rowno : 0
         * timeOfFilm :
         * totalCount : 0
         * wind_field_name : 风场1
         * wind_group_name : 风组1
         */

        private String equipment_id;
        private String huangdongzhi;
        private String jiajiaox;
        private String jiajiaoy;
        private String netSignal;
        private String normal;
        private String qingjiao;
        private int rowno;
        private String timeOfFilm;
        private int totalCount;
        private String wind_field_name;
        private String wind_group_name;

        public String getEquipment_id() {
            return equipment_id;
        }

        public void setEquipment_id(String equipment_id) {
            this.equipment_id = equipment_id;
        }

        public String getHuangdongzhi() {
            return huangdongzhi;
        }

        public void setHuangdongzhi(String huangdongzhi) {
            this.huangdongzhi = huangdongzhi;
        }

        public String getJiajiaox() {
            return jiajiaox;
        }

        public void setJiajiaox(String jiajiaox) {
            this.jiajiaox = jiajiaox;
        }

        public String getJiajiaoy() {
            return jiajiaoy;
        }

        public void setJiajiaoy(String jiajiaoy) {
            this.jiajiaoy = jiajiaoy;
        }

        public String getNetSignal() {
            return netSignal;
        }

        public void setNetSignal(String netSignal) {
            this.netSignal = netSignal;
        }

        public String getNormal() {
            return normal;
        }

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getQingjiao() {
            return qingjiao;
        }

        public void setQingjiao(String qingjiao) {
            this.qingjiao = qingjiao;
        }

        public int getRowno() {
            return rowno;
        }

        public void setRowno(int rowno) {
            this.rowno = rowno;
        }

        public String getTimeOfFilm() {
            return timeOfFilm;
        }

        public void setTimeOfFilm(String timeOfFilm) {
            this.timeOfFilm = timeOfFilm;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public String getWind_field_name() {
            return wind_field_name;
        }

        public void setWind_field_name(String wind_field_name) {
            this.wind_field_name = wind_field_name;
        }

        public String getWind_group_name() {
            return wind_group_name;
        }

        public void setWind_group_name(String wind_group_name) {
            this.wind_group_name = wind_group_name;
        }
    }
}
