package com.yufuer.electric.entity;

import java.util.List;

public class SystemReportBean {

    private List<ListBean> list;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * equipment_id : HFZA4A00000007
         * firex : 0
         * firey : 0
         * gaojing : 1
         * huangdongzhi : 2.508
         * jiajiaox : -84.848
         * jiajiaoy : 5.152
         * netSignal : 22
         * qingjiao : -1.437
         * rowno : 2
         * threshold_value : 2
         * timeOfFilm : 2019-08-27 22:55:34
         * totalcount : 152
         * wind_field_name : 风场1
         * wind_group_name : 风组1
         */

        private String equipment_id;
        private String firex;
        private String firey;
        private String gaojing;
        private String huangdongzhi;
        private String jiajiaox;
        private String jiajiaoy;
        private String netSignal;
        private String qingjiao;
        private int rowno;
        private String threshold_value;
        private String timeOfFilm;
        private int totalcount;
        private String wind_field_name;
        private String wind_group_name;

        public String getEquipment_id() {
            return equipment_id;
        }

        public void setEquipment_id(String equipment_id) {
            this.equipment_id = equipment_id;
        }

        public String getFirex() {
            return firex;
        }

        public void setFirex(String firex) {
            this.firex = firex;
        }

        public String getFirey() {
            return firey;
        }

        public void setFirey(String firey) {
            this.firey = firey;
        }

        public String getGaojing() {
            return gaojing;
        }

        public void setGaojing(String gaojing) {
            this.gaojing = gaojing;
        }

        public String getHuangdongzhi() {
            return huangdongzhi;
        }

        public void setHuangdongzhi(String huangdongzhi) {
            this.huangdongzhi = huangdongzhi;
        }

        public String getJiajiaox() {
            return jiajiaox;
        }

        public void setJiajiaox(String jiajiaox) {
            this.jiajiaox = jiajiaox;
        }

        public String getJiajiaoy() {
            return jiajiaoy;
        }

        public void setJiajiaoy(String jiajiaoy) {
            this.jiajiaoy = jiajiaoy;
        }

        public String getNetSignal() {
            return netSignal;
        }

        public void setNetSignal(String netSignal) {
            this.netSignal = netSignal;
        }

        public String getQingjiao() {
            return qingjiao;
        }

        public void setQingjiao(String qingjiao) {
            this.qingjiao = qingjiao;
        }

        public int getRowno() {
            return rowno;
        }

        public void setRowno(int rowno) {
            this.rowno = rowno;
        }

        public String getThreshold_value() {
            return threshold_value;
        }

        public void setThreshold_value(String threshold_value) {
            this.threshold_value = threshold_value;
        }

        public String getTimeOfFilm() {
            return timeOfFilm;
        }

        public void setTimeOfFilm(String timeOfFilm) {
            this.timeOfFilm = timeOfFilm;
        }

        public int getTotalcount() {
            return totalcount;
        }

        public void setTotalcount(int totalcount) {
            this.totalcount = totalcount;
        }

        public String getWind_field_name() {
            return wind_field_name;
        }

        public void setWind_field_name(String wind_field_name) {
            this.wind_field_name = wind_field_name;
        }

        public String getWind_group_name() {
            return wind_group_name;
        }

        public void setWind_group_name(String wind_group_name) {
            this.wind_group_name = wind_group_name;
        }
    }
}
