package com.yufuer.electric.entity;

import com.google.gson.annotations.SerializedName;

public class CountBean {

    /**
     * count : {"staticCount":"1"}
     */
    @SerializedName("count")
    private CountEntity count;

    public void setCount(CountEntity count) {
        this.count = count;
    }

    public CountEntity getCount() {
        return count;
    }

    public class CountEntity {
        /**
         * staticCount : 1
         */
        @SerializedName("staticCount")
        private String staticCount;

        public void setStaticCount(String staticCount) {
            this.staticCount = staticCount;
        }

        public String getStaticCount() {
            return staticCount;
        }
    }
}
