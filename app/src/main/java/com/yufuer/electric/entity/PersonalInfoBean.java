package com.yufuer.electric.entity;

public class PersonalInfoBean {


    /**
     * id : 20
     * username : admin
     * password : $2a$10$WZaFff6Bx0zVt6xiY9TwQO51Dq5v3w41poon9jyZaODR9aggkDEfe
     * role : admin
     * company : 单位15
     * email : 1010733746@qq.com
     * phone : 18392603906
     */

    private int id;
    private String username;
    private String password;
    private String role;
    private String company;
    private String email;
    private String phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
