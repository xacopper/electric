package com.yufuer.electric.entity;

import java.util.List;

public class ChartBean {

    private List<List<MapsBean>> maps;

    public List<List<MapsBean>> getMaps() {
        return maps;
    }

    public void setMaps(List<List<MapsBean>> maps) {
        this.maps = maps;
    }

    public static class MapsBean {
        /**
         * equipment_id : HFZA4A00000007
         * firex : 0
         * firey : 0
         * gaojing : 1
         * huangdongzhi : 2.5
         * install_location : 低
         * jiajiao : 1.567
         * qingjiao : 1.415
         * threshold_value : 2
         * timeOfFilm : 2019-08-27 01:56:50
         * touyingchangdu : 0.987
         * wind_field_name :
         * wind_group_name :
         */

        private String equipment_id;
        private String firex;
        private String firey;
        private String gaojing;
        private String huangdongzhi;
        private String install_location;
        private String jiajiao;
        private String qingjiao;
        private String threshold_value;
        private String timeOfFilm;
        private String touyingchangdu;
        private String wind_field_name;
        private String wind_group_name;

        public String getEquipment_id() {
            return equipment_id;
        }

        public void setEquipment_id(String equipment_id) {
            this.equipment_id = equipment_id;
        }

        public String getFirex() {
            return firex;
        }

        public void setFirex(String firex) {
            this.firex = firex;
        }

        public String getFirey() {
            return firey;
        }

        public void setFirey(String firey) {
            this.firey = firey;
        }

        public String getGaojing() {
            return gaojing;
        }

        public void setGaojing(String gaojing) {
            this.gaojing = gaojing;
        }

        public String getHuangdongzhi() {
            return huangdongzhi;
        }

        public void setHuangdongzhi(String huangdongzhi) {
            this.huangdongzhi = huangdongzhi;
        }

        public String getInstall_location() {
            return install_location;
        }

        public void setInstall_location(String install_location) {
            this.install_location = install_location;
        }

        public String getJiajiao() {
            return jiajiao;
        }

        public void setJiajiao(String jiajiao) {
            this.jiajiao = jiajiao;
        }

        public String getQingjiao() {
            return qingjiao;
        }

        public void setQingjiao(String qingjiao) {
            this.qingjiao = qingjiao;
        }

        public String getThreshold_value() {
            return threshold_value;
        }

        public void setThreshold_value(String threshold_value) {
            this.threshold_value = threshold_value;
        }

        public String getTimeOfFilm() {
            return timeOfFilm;
        }

        public void setTimeOfFilm(String timeOfFilm) {
            this.timeOfFilm = timeOfFilm;
        }

        public String getTouyingchangdu() {
            return touyingchangdu;
        }

        public void setTouyingchangdu(String touyingchangdu) {
            this.touyingchangdu = touyingchangdu;
        }

        public String getWind_field_name() {
            return wind_field_name;
        }

        public void setWind_field_name(String wind_field_name) {
            this.wind_field_name = wind_field_name;
        }

        public String getWind_group_name() {
            return wind_group_name;
        }

        public void setWind_group_name(String wind_group_name) {
            this.wind_group_name = wind_group_name;
        }
    }
}
