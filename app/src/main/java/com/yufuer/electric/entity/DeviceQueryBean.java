package com.yufuer.electric.entity;

import java.util.List;

public class DeviceQueryBean {

    private List<ListBean> list;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * equipment_id : HFZA4A00000005
         * install_high : 90
         * install_location : 高
         * remark : 李延康新增id
         * rowno : 1
         * threshold_value : 1
         * totalcount : 2
         * wind_admin : 风场管理员1
         * wind_field_name : 风场1
         * wind_group_name : 风组1
         */

        private String equipment_id;
        private String install_high;
        private String install_location;
        private String remark;
        private int rowno;
        private String threshold_value;
        private int totalcount;
        private String wind_admin;
        private String wind_field_name;
        private String wind_group_name;

        public String getEquipment_id() {
            return equipment_id;
        }

        public void setEquipment_id(String equipment_id) {
            this.equipment_id = equipment_id;
        }

        public String getInstall_high() {
            return install_high;
        }

        public void setInstall_high(String install_high) {
            this.install_high = install_high;
        }

        public String getInstall_location() {
            return install_location;
        }

        public void setInstall_location(String install_location) {
            this.install_location = install_location;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public int getRowno() {
            return rowno;
        }

        public void setRowno(int rowno) {
            this.rowno = rowno;
        }

        public String getThreshold_value() {
            return threshold_value;
        }

        public void setThreshold_value(String threshold_value) {
            this.threshold_value = threshold_value;
        }

        public int getTotalcount() {
            return totalcount;
        }

        public void setTotalcount(int totalcount) {
            this.totalcount = totalcount;
        }

        public String getWind_admin() {
            return wind_admin;
        }

        public void setWind_admin(String wind_admin) {
            this.wind_admin = wind_admin;
        }

        public String getWind_field_name() {
            return wind_field_name;
        }

        public void setWind_field_name(String wind_field_name) {
            this.wind_field_name = wind_field_name;
        }

        public String getWind_group_name() {
            return wind_group_name;
        }

        public void setWind_group_name(String wind_group_name) {
            this.wind_group_name = wind_group_name;
        }
    }
}
