package com.yufuer.electric.entity;

import java.util.List;

public class MyDialogBean {

    private List<String> list;

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
