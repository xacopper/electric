package com.yufuer.electric.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author yangkan
 * @date 2018/8/28
 * @description 用户实体类
 */

public class UserBean implements Parcelable {

    /**
     * token : Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlzcyI6ImVjaGlzYW4iLCJleHAiOjE1NjY4MTA0MTYsImlhdCI6MTU2NjgwNjgxNiwicm9sIjoiYWRtaW4ifQ.tWjfEDTBqEBlAoxnzwStvUC0gFVYj_oRm_9q19xprdA-H-BLyPw48EQaPVOF3wvwuUfUhBOEMJEX-ZFffQVrXA
     * logintime : 2019-08-26 16:06:56
     * role : admin
     * user : admin
     */

    private String token;
    private String logintime;
    private String role;
    private String user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.token);
        dest.writeString(this.logintime);
        dest.writeString(this.role);
        dest.writeString(this.user);
    }

    public UserBean() {
    }

    protected UserBean(Parcel in) {
        this.token = in.readString();
        this.logintime = in.readString();
        this.role = in.readString();
        this.user = in.readString();
    }

    public static final Parcelable.Creator<UserBean> CREATOR = new Parcelable.Creator<UserBean>() {
        @Override
        public UserBean createFromParcel(Parcel source) {
            return new UserBean(source);
        }

        @Override
        public UserBean[] newArray(int size) {
            return new UserBean[size];
        }
    };
}
