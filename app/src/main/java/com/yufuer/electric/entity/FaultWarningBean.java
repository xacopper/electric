package com.yufuer.electric.entity;

import java.util.List;

public class FaultWarningBean {


    private List<ListBean> list;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * equipment_id : HFZA4A00000007
         * huangdongzhi : 2.6
         * jiajiaox : -84.975
         * jiajiaoy : 5.025
         * netSignal : 40
         * normal :
         * qingjiao : -1.517
         * rowno : 6
         * timeOfFilm : 2019-09-01 10:55:22
         * totalCount : 210
         * wind_field_name : 风场1
         * wind_group_name : 风组1
         */

        private String equipment_id;
        private String huangdongzhi;
        private String jiajiaox;
        private String jiajiaoy;
        private String netSignal;
        private String normal;
        private String qingjiao;
        private int rowno;
        private String timeOfFilm;
        private int totalCount;
        private String wind_field_name;
        private String wind_group_name;

        public String getEquipment_id() {
            return equipment_id;
        }

        public void setEquipment_id(String equipment_id) {
            this.equipment_id = equipment_id;
        }

        public String getHuangdongzhi() {
            return huangdongzhi;
        }

        public void setHuangdongzhi(String huangdongzhi) {
            this.huangdongzhi = huangdongzhi;
        }

        public String getJiajiaox() {
            return jiajiaox;
        }

        public void setJiajiaox(String jiajiaox) {
            this.jiajiaox = jiajiaox;
        }

        public String getJiajiaoy() {
            return jiajiaoy;
        }

        public void setJiajiaoy(String jiajiaoy) {
            this.jiajiaoy = jiajiaoy;
        }

        public String getNetSignal() {
            return netSignal;
        }

        public void setNetSignal(String netSignal) {
            this.netSignal = netSignal;
        }

        public String getNormal() {
            return normal;
        }

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getQingjiao() {
            return qingjiao;
        }

        public void setQingjiao(String qingjiao) {
            this.qingjiao = qingjiao;
        }

        public int getRowno() {
            return rowno;
        }

        public void setRowno(int rowno) {
            this.rowno = rowno;
        }

        public String getTimeOfFilm() {
            return timeOfFilm;
        }

        public void setTimeOfFilm(String timeOfFilm) {
            this.timeOfFilm = timeOfFilm;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public String getWind_field_name() {
            return wind_field_name;
        }

        public void setWind_field_name(String wind_field_name) {
            this.wind_field_name = wind_field_name;
        }

        public String getWind_group_name() {
            return wind_group_name;
        }

        public void setWind_group_name(String wind_group_name) {
            this.wind_group_name = wind_group_name;
        }
    }
}
