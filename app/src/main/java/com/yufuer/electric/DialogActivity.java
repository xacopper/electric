package com.yufuer.electric;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.yufuer.electric.adapter.DataAdpter;
import com.yufuer.electric.base.BaseActivity;
import com.yufuer.electric.utils.WindUtils;
import com.yufuer.electric.utils.YLog;

import java.util.ArrayList;
import java.util.List;

public class DialogActivity extends Activity {

    private final static String TAG = "DialogActivity";
    public final static String TITLE_KEY = "title";
    public final static String LIST_KEY = "list";
    public final static String CODE_KEY = "code";

    private RecyclerView mDataListView = null;
    private TextView mTitleView = null;
    private int code = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_layout);
        ArrayList list = new ArrayList();
        String title = "";


        Intent intent = getIntent();

        if (intent != null){
            title = intent.getStringExtra(TITLE_KEY);
            list =  intent.getStringArrayListExtra(LIST_KEY);
            code = intent.getIntExtra(CODE_KEY, 0);

            YLog.d(TAG, "recived title: " +title);
        }

        mTitleView = (TextView) findViewById(R.id.dialog_title);
        mTitleView.setText(title);

        mDataListView = (RecyclerView) findViewById(R.id.data_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        mDataListView.setLayoutManager(layoutManager);

        layoutManager.setOrientation(RecyclerView.VERTICAL);

        DataAdpter dataAdpter = new DataAdpter(list);

        dataAdpter.setOnItemClickListener(new DataAdpter.OnItemClickListener() {
            @Override
            public void onItemClick(String v) {
                YLog.d(TAG, "click str: " +v);
                Intent i = new Intent();
                i.putExtra("data", v);
                setResult(code, i);
                finish();
            }
        });

        mDataListView.setItemAnimator( new DefaultItemAnimator());
        mDataListView.setAdapter(dataAdpter);

        WindowManager.LayoutParams p = getWindow().getAttributes();
        p.gravity = Gravity.BOTTOM;
        p.width = WindowManager.LayoutParams.MATCH_PARENT;

        getWindow().setAttributes(p);

        setFinishOnTouchOutside(true);


        YLog.d(TAG, "DialogActivity oncreate end");
    }
}

