package com.yufuer.electric.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yufuer.electric.R;
import com.yufuer.electric.adapter.MyDialogAdapter;
import com.yufuer.electric.base.BaseActivity;
import com.yufuer.electric.constant.MyDialogConstant;
import com.yufuer.electric.http.HttpRequestManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyDialogActivityActivity extends BaseActivity {

    private static final String TAG = "MyDialogActivityActivit";

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
//    private HttpRequestManager httpRequestManager;

    private List<String> mDataList = new ArrayList<>();
    private MyDialogAdapter mAdapter;
//    第一版设计
//    /**
//     * 静态方法启动 Activity
//     *
//     * @param context
//     */
//    public static void startActivity(Fragment context, int requestCode, String url, Map<String, Object> jsonStr) {
//        Intent intent = new Intent(context.getContext(), MyDialogActivityActivity.class);
//        //传递数据
//        Bundle bundle=new Bundle();
//        SerializableMap myMap=new SerializableMap();
//        myMap.setMap(jsonStr);//将map数据添加到封装的myMap中
//        bundle.putSerializable(MyDialogConstant.KEY_JSON, myMap);
//        bundle.putString(MyDialogConstant.KEY_URL, url);
//        intent.putExtras(bundle);
//        context.startActivityForResult(intent, requestCode);
//
//
//    }

    /**
     * 静态方法启动 Activity
     *
     * @param context
     */
    public static void startActivity(Fragment context, int requestCode, ArrayList<String> list,String title)  {

        Intent intent = new Intent(context.getContext(), MyDialogActivityActivity.class);
        //传递数据
        Bundle bundle=new Bundle();
        bundle.putStringArrayList(MyDialogConstant.KEY_DATA_LIST,list);
        bundle.putString(MyDialogConstant.KEY_TITLE,title);
        intent.putExtras(bundle);
        context.startActivityForResult(intent, requestCode);


    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_my_dialog_activity;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
//    第一版设计
//        Bundle bundle = getIntent().getExtras();
//        SerializableMap serializableMap = (SerializableMap) bundle.get(MyDialogConstant.KEY_JSON);
//        Map<String, Object> jsonStr =  serializableMap.getMap();
//        String url = bundle.getString(MyDialogConstant.KEY_URL);

        Bundle bundle = getIntent().getExtras();
        ArrayList list = bundle.getStringArrayList(MyDialogConstant.KEY_DATA_LIST);
        String title = bundle.getString(MyDialogConstant.KEY_TITLE);
        tvTitle.setText(title);
        mDataList.addAll(list);

        Window win = this.getWindow();
        win.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM; // 修改当前activity or dialogfragment不紧挨底部问题
        lp.dimAmount = 0.8f; // 这是没有布局的地方可以显示为黑色背影
        win.setAttributes(lp);
        overridePendingTransition(R.anim.anim_bottom_dialog_in, R.anim.anim_bottom_dialog_out);

        setFinishOnTouchOutside(true);

        initRecyclerView();
//        loadData(url,jsonStr);

        mAdapter.notifyDataSetChanged();
    }

    /**
     * 第一版设计是把网络请求放在该acitviy中,外部传出url 和jsonstr
     */
//    private void loadData(String url, Map<String, Object> jsonStr) {
//
//
//        LogUtils.d(TAG, "url: " + url);
//
//        Map<String, String> header = new HashMap<>();
//        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());
//
//        httpRequestManager = HttpRequestManager.getInstance();
//        httpRequestManager.httpPostJson(url, MyDialogBean.class, TAG, header, null, jsonStr, new HttpListener<MyDialogBean>() {
//            @Override
//            public void onResponse(MyDialogBean response) {
//
//                if (null != response) {
//                    List<String> list = response.getList();
//
//                    if (list.size() <= 0) {
//                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
//                        return;
//                    }
//                    LogUtils.d(TAG, "list size: " + list.size());
//                    mDataList.addAll(list);
//                    LogUtils.d(TAG, "mDataList size: " + list.size());
//                    mAdapter.notifyDataSetChanged();
//
//
//                } else {
//                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
//                }
//            }
//
//            @Override
//            public void onFailure(String message) {
//                LogUtils.d(TAG, "failure: " + message);
//                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
//            }
//        });
//
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (httpRequestManager != null) {
//            httpRequestManager.cancelRequest(TAG);
//        }
    }


    /**
     * 初始话RecyclerView
     */
    private void initRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyDialogAdapter(R.layout.item_my_dialog, mDataList);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    view.findViewById(R.id.iv_flag).setVisibility(View.VISIBLE);
                    String value = mDataList.get(position);
                    Intent intent = new Intent();
                    intent.putExtra(MyDialogConstant.KEY_RESULT, value);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
            }
        });
//        mAdapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
//                view.findViewById(R.id.iv_flag).setVisibility(View.VISIBLE);
//                String value = mDataList.get(position);
//                Intent intent = new Intent();
//                intent.putExtra(MyDialogConstant.KEY_RESULT, value);
//                setResult(Activity.RESULT_OK, intent);
//                finish();
//                return false;
//            }
//        });

    }

    @OnClick(R.id.btn_cancel)
    public void onViewClicked() {
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override public void finish() {
        super.finish();
        overridePendingTransition(R.anim.anim_bottom_dialog_in, R.anim.anim_bottom_dialog_out);
    }



}
