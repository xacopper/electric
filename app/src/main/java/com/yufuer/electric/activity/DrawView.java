package com.yufuer.electric.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.yufuer.electric.R;
import com.yufuer.electric.utils.YLog;

/**
 * http://blog.csdn.net/rhljiayou/article/details/7212620
 * Created by flannery on 17-6-2.
 */

public class DrawView extends View {

    private final static String TAG = "DrawView";
    private final static int MaxRadio = 250;

    private Point mCenter = null;
    private Point mStart = null;
    private Point mEnd = null;
    private String mNowData ="";
    private String mHistorData = "";

    public DrawView(Context context) {
        super(context);
    }

    public void setData(Point center, Point start, Point end, String nowData, String history){

        mCenter = center;
        mStart = start;
        mEnd = end;
        mNowData = nowData;
        mHistorData = history;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*
         * 方法 说明 drawRect 绘制矩形 drawCircle 绘制圆形 drawOval 绘制椭圆 drawPath 绘制任意多边形
         * drawLine 绘制直线 drawPoin 绘制点
         */
        // 创建画笔
        Paint p = new Paint();
        p.setColor(Color.BLACK);// 设置红色
        p.setTypeface(Typeface.DEFAULT_BOLD);

        drawCircke(canvas, p);

        YLog.d(TAG, mStart.toString());
        YLog.d(TAG, mEnd.toString());

        drawLine(canvas, p, mStart, mEnd);


        p.setColor(Color.RED);// 设置绿色
        drawPoint(canvas, p, mStart);

        p.setColor(Color.rgb(69,122,29));// 设置绿色
        drawPoint(canvas, p, mEnd);

    }

    private void drawPic(Canvas canvas, Paint p, int drawableId){
        //        //画图片，就是贴图
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId);
        canvas.drawBitmap(bitmap, 250, 360, p);
    }


    private void drawLine(Canvas canvas, Paint p, Point start, Point end){

        p.setTextSize(26);
        p.setStyle(Paint.Style.FILL);

        drawText(canvas, p, "北", mCenter.getmX() -10, 50, Color.BLACK);
        // Y 轴
        drawText(canvas, p, "0", mCenter.getmX() -10, 85,Color.BLACK);
        canvas.drawLine(mCenter.getmX(), 100, mCenter.getmX(), 600, p);// Y轴
        drawText(canvas, p, "180", mCenter.getmX()-20, 630,Color.BLACK);

        // 120度斜线
        Point tmpStart = Point.getPointWithCenter(30, 250, mCenter);
        Point tmpend = Point.getPointWithCenter(210, 250, mCenter);
        drawText(canvas, p, "30", tmpStart.getmX()+10, tmpStart.getmY()-10,Color.BLACK);
        canvas.drawLine(tmpStart.getmX(), tmpStart.getmY(), tmpend.getmX(), tmpend.getmY(), p);// Y轴
        drawText(canvas, p, "210", tmpend.getmX()-20, tmpend.getmY()+30,Color.BLACK);

        // 150度斜线
        tmpStart = Point.getPointWithCenter(60, 250, mCenter);
        tmpend = Point.getPointWithCenter(240, 250, mCenter);
        drawText(canvas, p, "60", tmpStart.getmX()+10, tmpStart.getmY()-10,Color.BLACK);
        canvas.drawLine(tmpStart.getmX(), tmpStart.getmY(), tmpend.getmX(), tmpend.getmY(), p);// Y轴
        drawText(canvas, p, "240", tmpend.getmX()-30, tmpend.getmY()+30,Color.BLACK);

        // X 轴
        drawText(canvas, p, "90", mCenter.getmX()+ MaxRadio+20, mCenter.getmY(),Color.BLACK);
        canvas.drawLine(mCenter.getmX()- MaxRadio, mCenter.getmY(), mCenter.getmX() + MaxRadio, mCenter.getmY(), p);
        drawText(canvas, p, "270", mCenter.getmX()- MaxRadio - 40, mCenter.getmY(),Color.BLACK);


        // 60度斜线
        tmpStart = Point.getPointWithCenter(120, 250, mCenter);
        tmpend = Point.getPointWithCenter(300, 250, mCenter);
        drawText(canvas, p, "120", tmpStart.getmX(), tmpStart.getmY()+10,Color.BLACK);
        canvas.drawLine(tmpStart.getmX(), tmpStart.getmY(), tmpend.getmX(), tmpend.getmY(), p);// Y轴
        drawText(canvas, p, "300", tmpend.getmX()-50, tmpend.getmY()-10,Color.BLACK);

        // 30度斜线
        tmpStart = Point.getPointWithCenter(150, 250, mCenter);
        tmpend = Point.getPointWithCenter(330, 250, mCenter);
        drawText(canvas, p, "150", tmpStart.getmX() -10, tmpStart.getmY()+40,Color.BLACK);
        canvas.drawLine(tmpStart.getmX(), tmpStart.getmY(), tmpend.getmX(), tmpend.getmY(), p);// Y轴
        drawText(canvas, p, "330", tmpend.getmX()-40, tmpend.getmY()-10,Color.BLACK);

        p.setTextSize(36);
        if (!TextUtils.isEmpty(mNowData)){

            drawText(canvas, p, "绿色是最新晃动值", 100, 730, Color.rgb(69,122,29));
        }

//        YLog.d(TAG, "nowData:" + mNowData);
        if (!TextUtils.isEmpty(mHistorData)){
            drawText(canvas, p, "红色是历史晃动值", 520, 730, Color.RED);
        }
//        YLog.d(TAG, "mHistorData:" + mHistorData);
    }

    private void drawCircke(Canvas canvas, Paint p){

        p.setStyle(Paint.Style.STROKE);
        p.setAntiAlias(true);// 设置画笔的锯齿效果。 true是去除，大家一看效果就明白了
        canvas.drawCircle(mCenter.getmX(), mCenter.getmY(), 250, p);// 大圆
        canvas.drawCircle(mCenter.getmX(), mCenter.getmY(), 200, p);// 大圆
        canvas.drawCircle(mCenter.getmX(), mCenter.getmY(), 150, p);// 大圆
        canvas.drawCircle(mCenter.getmX(), mCenter.getmY(), 100, p);// 大圆
        canvas.drawCircle(mCenter.getmX(), mCenter.getmY(), 50, p);// 大圆

    }

    /***
     * 画点
     * @param canvas
     */
    private void drawPoint(Canvas canvas, Paint p, Point point){

//        p.setColor(Color.RED);// 设置红色
        p.setStyle(Paint.Style.FILL);
//        canvas.drawText("画点：", 10, 390, p);
//        canvas.drawPoint(point.getmX(), point.getmY(), p);//画一个点
        canvas.drawCircle(point.getmX(), point.getmY(), 8,  p);
//        canvas.drawPoints(new float[]{60, 400, 65, 400, 70, 400}, p);//画多个点
    }

    private void drawText(Canvas canvas, Paint p, String text, float x, float y, int color){

        p.setColor(color);
        canvas.drawText(text, x, y, p);
    }

}
