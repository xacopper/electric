package com.yufuer.electric.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yangkan.commonutillibrary.mobile.utils.LogUtils;
import com.yangkan.commonutillibrary.mobile.utils.ToastUtils;
import com.yufuer.electric.R;
import com.yufuer.electric.adapter.FaultWarningAdapter;
import com.yufuer.electric.app.MyApplication;
import com.yufuer.electric.base.BaseToolBarActivity;
import com.yufuer.electric.constant.FaultWarningConstant;
import com.yufuer.electric.constant.LoginConstant;
import com.yufuer.electric.constant.SystemReportConstant;
import com.yufuer.electric.entity.FaultWarningBean;
import com.yufuer.electric.http.HttpListener;
import com.yufuer.electric.http.HttpRequestManager;
import com.yufuer.electric.utils.LoginUtils;
import com.yufuer.electric.utils.UrlUtils;
import com.yufuer.electric.widget.CustomLinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class FaultWarningActivity extends BaseToolBarActivity {

    private static final String TAG = "FaultWarningActivity";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.btn_previous_page)
    Button btnPreviousPage;
    @BindView(R.id.btn_next_page)
    Button btnNextPage;

    private HttpRequestManager httpRequestManager;

    private int current_Page = 0;

    private FaultWarningAdapter mAdapter;


    private List<FaultWarningBean.ListBean> mDataList = new ArrayList<>();

    private String windFieldName = "";
    private String windGroupName = "";

    /**
     * 静态方法启动 Activity
     *
     * @param context
     */
    public static void startActivity(Context context, String windFieldName, String windGroupName) {
        Intent intent = new Intent(context, FaultWarningActivity.class);
        intent.putExtra(FaultWarningConstant.KEY_WIND_FIELD_NAME, windFieldName);
        intent.putExtra(FaultWarningConstant.KEY_WIND_GROUP_NAME, windGroupName);
        context.startActivity(intent);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_fault_warning;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        Intent intent = getIntent();

        if (intent != null) {
            if (intent.hasExtra(FaultWarningConstant.KEY_WIND_FIELD_NAME)) {
                windFieldName = intent.getStringExtra(FaultWarningConstant.KEY_WIND_FIELD_NAME);
            }

            if (intent.hasExtra(FaultWarningConstant.KEY_WIND_GROUP_NAME)) {
                windGroupName = intent.getStringExtra(FaultWarningConstant.KEY_WIND_GROUP_NAME);
            }
        }

        setToolBar(toolbar);

        setToolBarTitle(toolbarTitle, getString(R.string.login_title));

        httpRequestManager = HttpRequestManager.getInstance();
        initRecyclerView();
        loadData(windFieldName,windGroupName,1);
    }

    @Override
    protected void setMenuOptions(Menu menu) {

    }

    @Override
    protected void setMenuItemClick(MenuItem item) {

    }

    private void loadData(String windFieldName, String windGroupName, int currentPage) {

        String url = UrlUtils.URL_GET_FAULT_WARN;
        LogUtils.d(TAG, "url: " + url);


        showProgressDialog();
        Map<String, String> header = new HashMap<>();
        header.put(LoginConstant.AUTHORIZATION, LoginUtils.getToken());

        Map<String, Object> jsonStr = new HashMap<>();
        jsonStr.put(FaultWarningConstant.KEY_WIND_FIELD_NAME, windFieldName);
        jsonStr.put(FaultWarningConstant.KEY_WIND_GROUP_NAME, windGroupName);
        jsonStr.put(FaultWarningConstant.KEY_SIZE, 10);
        jsonStr.put(FaultWarningConstant.KEY_CURRENT_PAGE, currentPage);


        httpRequestManager.httpPostJson(url, FaultWarningBean.class, TAG, header, null, jsonStr, new HttpListener<FaultWarningBean>() {
            @Override
            public void onResponse(FaultWarningBean response) {
                dismissProgressDialog();

                if (null != response) {
                    List<FaultWarningBean.ListBean> list = response.getList();

                    if (list.size() <= 0) {
                        ToastUtils.getInstance(MyApplication.context).showMessage("暂无数据");
                        return;
                    }
                    LogUtils.d(TAG, "list size: " + list.size());
                    mDataList.addAll(list);
                    LogUtils.d(TAG, "mDataList size: " + list.size());
                    mAdapter.notifyDataSetChanged();

                    // TODO: 2019/9/1  请求数据后，存储参数。这些参数是上一页、下一页点击时候的判断条件

                    current_Page = currentPage;


                } else {
                    ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
                }
            }

            @Override
            public void onFailure(String message) {
                LogUtils.d(TAG, "failure: " + message);
                dismissProgressDialog();
                ToastUtils.getInstance(MyApplication.context).showMessage("服务器连接失败，请检查网络是否正常");
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (httpRequestManager != null) {
            httpRequestManager.cancelRequest(TAG);
        }
    }


    /**
     * 初始话RecyclerView
     */
    private void initRecyclerView() {

        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(this);
        layoutManager.setScrollEnabled(false);
        recyclerview.setLayoutManager(layoutManager);
        //如果可以确定每个item的高度是固定的，设置这个选项可以提高性能
        recyclerview.setHasFixedSize(true);
        mAdapter = new FaultWarningAdapter(R.layout.item_fault_warning, mDataList);
        recyclerview.setAdapter(mAdapter);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        LayoutInflater inflater = LayoutInflater.from(this);
        View headerView = inflater.inflate(R.layout.layout_header_fault_warning, null, false);
        mAdapter.addHeaderView(headerView);
    }

    private void getNextPageData() {
        int currentPageTemp = current_Page;
        currentPageTemp = currentPageTemp + 1;
        mDataList.clear();
        loadData(windFieldName,windGroupName,currentPageTemp);
    }

    private void getPreviousPageData() {

        int currentPageTemp = current_Page;
        currentPageTemp = current_Page - 1;
        if (current_Page <= 1) {
            ToastUtils.getInstance(this).showMessage("当前已是第一页");
            return;
        }
        mDataList.clear();
        loadData(windFieldName,windGroupName,currentPageTemp);
    }

    @OnClick({R.id.btn_previous_page, R.id.btn_next_page})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_previous_page:
                getPreviousPageData();
                break;
            case R.id.btn_next_page:
                getNextPageData();
                break;
        }
    }
}
