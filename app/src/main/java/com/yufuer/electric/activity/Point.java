package com.yufuer.electric.activity;

public class Point {

    public static final int AREA_FIRST = 1;
    public static final int AREA_SECOND = 2;
    public static final int AREA_THIRD = 3;
    public static final int AREA_FOURTH = 4;
    private float mX;
    private float mY;
    private float mEdge;

    public Point(float x, float y, float edge){

        this.mY = y;
        this.mX = x;
        this.mEdge = edge;

    }

    public static Point getPoint(float angle, float edge){

        float tmp = angle + 90;
        double sin = Math.sin(Math.PI * tmp / 180);
        double cos = Math.cos(Math.PI * tmp / 180);

        float y = (float) sin * edge *-1;
        float x = (float) cos * edge *-1;

        return  new Point(x, y, edge);
    }

    public static Point getPointWithCenter(float angle, float edge, Point center){

        Point p = new Point(0, 0, edge);
        Point tmp = getPoint(angle,edge);

        p.setmX(tmp.getmX()+center.getmX());
        p.setmY(tmp.getmY()+center.getmY());
        return p;
    }

    public int getLocation(float angle){

        int ret = 0;
        if (angle > 0 && angle <= 90){
            ret = AREA_FIRST;
        }else if (angle > 90 && angle <= 180){
            ret = AREA_SECOND;
        }else if (angle > 180 && angle <= 270){

            ret = AREA_THIRD;
        }else if (angle > 270 && angle <= 360){
            ret = AREA_FOURTH;
        }

        return ret;
    }

    public float getmX() {
        return mX;
    }

    public void setmX(float mX) {
        this.mX = mX;
    }

    public float getmY() {
        return mY;
    }

    public void setmY(float mY) {
        this.mY = mY;
    }

    @Override
    public String toString() {
        String tmp = "("+mX +","+mY
                +")"
                + " edge: " + mEdge;
        return tmp;
    }
}
