package com.yangkan.commonutillibrary.mobile.permission;

import android.content.Context;
import android.content.pm.PackageManager;


import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;


/**
 * @author yangkan
 * @date 2018/01/22
 * @description 权限检查器
 */
public class PermissionsChecker {

    /**
     * 判断是否缺少权限
     * @param context
     * @param permission
     * @return
     */
    private static boolean lacksPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) ==
                PackageManager.PERMISSION_DENIED;
    }

    /**
     * 判断是否满足一组权限
     * @param context
     * @param permissions
     * @return
     */
    public static boolean lacksPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (lacksPermission(context, permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取尚未获得的权限
     * @param context
     * @param rawPermissions
     * @return
     */
    public static String[] getLackedPermissions(Context context, String[] rawPermissions) {
        List<String> results = new ArrayList<>();
        for (String item : rawPermissions) {
            if (PermissionsChecker.lacksPermission(context, item)) {
                results.add(item);
            }
        }
        String[] resultPermissions = new String[results.size()];
        return results.toArray(resultPermissions);
    }
}
