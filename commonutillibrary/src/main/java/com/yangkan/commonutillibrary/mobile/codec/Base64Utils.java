package com.yangkan.commonutillibrary.mobile.codec;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by yangkan on 2017/7/10.
 */

public final class Base64Utils {

    public static String getBase64String(String source) {
        return Base64.encodeToString(source.getBytes(), Base64.DEFAULT);
    }

    public static String getSHA1AndBase64(String source) {
        try {
            MessageDigest digest = MessageDigest
                    .getInstance("SHA-1");
            digest.update(source.getBytes("utf-8"));
            byte messageDigest[] = digest.digest();

            return Base64.encodeToString(messageDigest, Base64.DEFAULT);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }
}
