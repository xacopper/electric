
package com.yangkan.commonutillibrary.mobile.watermark;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;

import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Pair;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import static com.yangkan.commonutillibrary.mobile.watermark.WatermarkPosition.CUSTOM;
import static com.yangkan.commonutillibrary.mobile.watermark.WatermarkPosition.TILE;

public class Watermark {

    private Context mContext;
    private static final int BACKGROUND_MARGIN = 10;

    public Watermark(@NonNull Context context) {
        mContext = context;
    }

    public Bitmap addStamp(@NonNull WatermarkConfig config) {

        if (config == null) {
            throw new IllegalArgumentException("The config passed to this method should never" +
                    "be null");
        }

        Bitmap baseBitmap = getBaseBitmap(config);
        if (baseBitmap == null) {
            return baseBitmap;
        }

        int baseBitmapWidth = baseBitmap.getWidth();
        int baseBitmapHeight = baseBitmap.getHeight();

        Bitmap result = Bitmap.createBitmap(baseBitmapWidth, baseBitmapHeight, baseBitmap.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(baseBitmap, 0, 0, null);

        // Either one of the methods(text/bitmap) can be used to add
        if (!TextUtils.isEmpty(config.getWatermarkString())) {
            addTextToBitmap(config, canvas, baseBitmapWidth, baseBitmapHeight);
        } else if (config.getWatermarkBitmap() != null) {

            if (config.getWatermarkWidth() != 0 && config.getWatermarkHeight() != 0) {

                addBitmapToBitmap(scaleBitmap(config.getWatermarkBitmap(), config.getWatermarkWidth(),
                        config.getWatermarkHeight()),
                        config, canvas,
                        baseBitmapWidth, baseBitmapHeight);

            } else {
                addBitmapToBitmap(config.getWatermarkBitmap(), config, canvas,
                        baseBitmapWidth, baseBitmapHeight);
            }
        }
        return result;
    }

    @Nullable
    private Bitmap getBaseBitmap(@NonNull WatermarkConfig config) {
        Bitmap baseBitmap = config.getBaseBitmap();
        @DrawableRes int drawable = config.getBaseDrawable();

        if (baseBitmap == null) {
            baseBitmap = BitmapFactory.decodeResource(mContext.getResources(), drawable);
            if (baseBitmap == null) return null;
        }
        return baseBitmap;
    }

    /**
     * Method to add text to a canvas based on the provided configuration
     *
     * @param config
     * @param canvas
     * @param baseBitmapWidth
     * @param baseBitmapHeight
     */
    private void addTextToBitmap(@NonNull WatermarkConfig config,
                                 @NonNull Canvas canvas,
                                 int baseBitmapWidth,
                                 int baseBitmapHeight) {
        Rect bounds = new Rect();
        TextPaint paint = new TextPaint();

        paint.setAntiAlias(true);
        paint.setUnderlineText(false);
//        TODO 添加解决清晰度问题
        paint.setDither(true); // 获取跟清晰的图像采样
        paint.setFilterBitmap(true);// 过滤一些

        paint.setTextSize(config.getTextSize());

        String typeFacePath = config.getTypeFacePath();
        // Add font typeface if its present in the config.
        if (!TextUtils.isEmpty(typeFacePath)) {
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), typeFacePath);
            paint.setTypeface(typeface);
        }

        Shader shader = config.getTextShader();
        // Add shader if its present in the config.
        if (shader != null) {
            paint.setShader(shader);
        }

        if (config.getTextShadowXOffset() != 0 || config.getTextShadowYOffset() != 0
                || config.getTextShadowBlurRadius() != 0) {
            // If any shadow property is present, set a shadow layer.
            paint.setShadowLayer(config.getTextShadowBlurRadius(),
                    config.getTextShadowXOffset(),
                    config.getTextShadowYOffset(),
                    config.getTextShadowColor());
        }

        String watermarkInfoString = config.getWatermarkString();
        paint.getTextBounds(watermarkInfoString, 0, watermarkInfoString.length(), bounds);

        int watermarkWidth = bounds.width();
        float watermarkMeasuredWidth = paint.measureText(watermarkInfoString);
        int watermarkHeight = bounds.height();

        int positionX = config.getPositionX();
        int positionY = config.getPositionY();

        if (config.getWatermarkPosition() != CUSTOM) {

            Pair<Integer, Integer> pair = PositionCalculator
                    .getCoordinates(config.getWatermarkPosition(),
                            baseBitmapWidth, baseBitmapHeight,
                            watermarkWidth, watermarkHeight);
            positionX = pair.first;
            positionY = pair.second;
        }

        // Add the margin to this position if it was passed to the config.
        positionX += config.getXMargin();
        positionY += config.getYMargin();

        float rotation = config.getRotation();
        // Add rotation if its present in the config.
        if (rotation != 0.0f) {
            canvas.rotate(rotation, positionX + bounds.exactCenterX(),
                    positionY - bounds.exactCenterY());
        }

        // Add the specified text color if its present in the config or it will use the default value.
        paint.setColor(config.getTextColor());

        int alpha = config.getAplha();

        if (alpha >= 0 && alpha <= 255) {
            paint.setAlpha(alpha);
        }

        if (config.getWatermarkPosition() != TILE) {

            int backgroundColor = config.getTextBackgroundColor();
            if (backgroundColor != 0) {
                Paint backgroundPaint = new Paint();
                backgroundPaint.setColor(backgroundColor);
                canvas.drawRect(positionX - BACKGROUND_MARGIN,
                        positionY - bounds.height() - paint.getFontMetrics().descent - BACKGROUND_MARGIN,
                        (positionX + watermarkMeasuredWidth + config.getTextShadowXOffset() + BACKGROUND_MARGIN),
                        positionY + config.getTextShadowYOffset() + paint.getFontMetrics().descent + BACKGROUND_MARGIN,
                        backgroundPaint);
            }

            StaticLayout sl = new StaticLayout(watermarkInfoString, paint, baseBitmapWidth - 8, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            canvas.translate(positionX, positionY);
            sl.draw(canvas);
//            canvas.save(Canvas.ALL_SAVE_FLAG);
            canvas.save();
            canvas.restore();
        } else {

            Bitmap textImage = Bitmap.createBitmap((int) watermarkMeasuredWidth,
                    watermarkHeight,
                    Bitmap.Config.ARGB_8888);
            Canvas textCanvas = new Canvas(textImage);
            textCanvas.drawText(config.getWatermarkString(), 0, watermarkHeight, paint);
            paint.setShader(new BitmapShader(textImage,
                    Shader.TileMode.REPEAT,
                    Shader.TileMode.REPEAT));
            Rect bitmapShaderRect = canvas.getClipBounds();
            canvas.drawRect(bitmapShaderRect, paint);
        }
    }

    /**
     * Method to add a bitmap  to a canvas based on the provided configuration
     *
     * @param watermarkBitmap
     * @param config
     * @param canvas
     * @param baseBitmapWidth
     * @param baseBitmapHeight
     */
    private void addBitmapToBitmap(@NonNull Bitmap watermarkBitmap,
                                   @NonNull WatermarkConfig config,
                                   @NonNull Canvas canvas,
                                   int baseBitmapWidth,
                                   int baseBitmapHeight) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setUnderlineText(false);

        int alpha = config.getAplha();

        if (alpha >= 0 && alpha <= 255) {
            paint.setAlpha(alpha);
        }

        int positionX = config.getPositionX();
        int positionY = config.getPositionY();
        WatermarkPosition watermarkPosition = config.getWatermarkPosition();
        if (watermarkPosition != CUSTOM) {
            // If the specified RubberStampPosition is not CUSTOM, use calculates its x & y
            // co-ordinates.
            Pair<Integer, Integer> pair =
                    PositionCalculator.getCoordinates(watermarkPosition,
                            baseBitmapWidth, baseBitmapHeight,
                            watermarkBitmap.getWidth(), watermarkBitmap.getHeight());
            positionX = pair.first;
            positionY = pair.second - watermarkBitmap.getHeight();
        }

        // Add the margin to this position if it was passed to the config.
        positionX += config.getXMargin();
        positionY += config.getYMargin();

        float rotation = config.getRotation();
        if (rotation != 0.0f) {
            // Add rotation if its present in the config.
            canvas.rotate(rotation, positionX + (watermarkBitmap.getWidth() / 2),
                    positionY + (watermarkBitmap.getHeight() / 2));
        }

        if (watermarkPosition != TILE) {
            canvas.drawBitmap(watermarkBitmap, positionX, positionY, paint);
        } else {

            paint.setShader(new BitmapShader(watermarkBitmap,
                    Shader.TileMode.REPEAT,
                    Shader.TileMode.REPEAT));
            Rect bitmapShaderRect = canvas.getClipBounds();
            canvas.drawRect(bitmapShaderRect, paint);
        }
    }

    private Bitmap scaleBitmap(Bitmap bm, int newWidth, int newHeight) {
        // 获得图片的宽高.
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例.
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数.
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片.
        Bitmap newBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return newBitmap;
    }
}
