/*
 * Copyright (c) 2017.
 * BOCO Inter-Telecom. All rights reserved.
 */

package com.yangkan.commonutillibrary.mobile.http;

/**
 * @author yangkan
 * @date 2017/12/15
 * @description
 */

public final class HttpConstant {

    /**
     * 超时时间
     */
    public static final int MAX_TIME_OUT = 60 * 1000;
    /**
     * 最大重试次数
     */
    public static final int MAX_RETRIES_NUM = 3;
    /**
     * 超时时间乘积因子
     */
    public static final float BACKOFF_MULT = 1f;

    /**
     * http访问约定的字段名
     */
    public static final String CODE = "code";
}
