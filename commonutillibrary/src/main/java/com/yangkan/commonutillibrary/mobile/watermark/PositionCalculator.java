package com.yangkan.commonutillibrary.mobile.watermark;

import android.util.Pair;

public class PositionCalculator {

    /**
     * 确定水印的位置
     *
     * @param location
     * @param bitmapWidth
     * @param bitmapHeight
     * @param watermarkWidth
     * @param watermarkHeight
     * @return
     */
    public static Pair<Integer, Integer> getCoordinates(WatermarkPosition location,
                                                        int bitmapWidth, int bitmapHeight,
                                                        int watermarkWidth, int watermarkHeight) {
        switch (location) {
            case TOP_LEFT:
                return new Pair<>(0, watermarkHeight);

            case TOP_CENTER:
                return new Pair<>((bitmapWidth / 2) - (watermarkWidth / 2),
                        watermarkHeight);
            case TOP_RIGHT:
                return new Pair<>(bitmapWidth - watermarkWidth, watermarkHeight);

            case CENTER_LEFT:
                return new Pair<>(0, (bitmapHeight / 2) + (watermarkHeight / 2));

            case CENTER:
                return new Pair<>((bitmapWidth / 2) - (watermarkWidth / 2),
                        (bitmapHeight / 2) + (watermarkHeight / 2));
            case CENTER_RIGHT:
                return new Pair<>(bitmapWidth - watermarkWidth,
                        (bitmapHeight / 2) + (watermarkHeight / 2));

            case BOTTOM_LEFT:
                return new Pair<>(0, bitmapHeight);

            case BOTTOM_CENTER:
                return new Pair<>((bitmapWidth / 2) - (watermarkWidth / 2),
                        bitmapHeight);
            case BOTTOM_RIGHT:
                return new Pair<>(bitmapWidth - watermarkWidth, bitmapHeight);

            default:
                return new Pair<>((bitmapWidth / 2) - (watermarkWidth / 2),
                        (bitmapHeight / 2) + (watermarkHeight / 2));
        }
    }
}
