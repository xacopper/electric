package com.yangkan.commonutillibrary.mobile.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.FileProvider;


import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.yangkan.commonutillibrary.R;

import java.io.File;

/**
 * @author yangkan
 * @date 2017/9/7
 * @description 跳转工具类
 */
public final class IntentUtils {
    private static final String TAG = "IntentUtils";

    private IntentUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static void chooseFile(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * @param activity    当前activity
     * @param imageUri    拍照后照片存储路径
     * @param requestCode 调用系统相机请求码
     */
    public static void takePicture(Activity activity, Uri imageUri, int requestCode) {
        //调用系统相机
        Intent intentCamera = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // 对目标应用临时授权该Uri所代表的文件
            intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intentCamera.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        //将拍照结果保存至photo_file的Uri中，不保留在相册中
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        activity.startActivityForResult(intentCamera, requestCode);
    }

    /**
     * @param fragment    Fragment
     * @param imageUri    拍照后照片存储路径
     * @param requestCode 调用系统相机请求码
     */
    public static void takePicture(Fragment fragment, Uri imageUri, int requestCode) {
        //调用系统相机
        Intent intentCamera = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // 对目标应用临时授权该Uri所代表的文件
            intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intentCamera.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        //将拍照结果保存至photo_file的Uri中，不保留在相册中
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        fragment.startActivityForResult(intentCamera, requestCode);
    }

    /**
     * @param activity    当前activity
     * @param filePath    拍照后照片存储路径
     * @param authority   标记FileProvider
     * @param requestCode 调用系统相机请求码
     */
    public static void takePicture(Activity activity, String filePath, String authority, int requestCode) {
        if (SDCardUtils.isSDCardEnable()) {
            Intent intent = new Intent();
            File file = new File(filePath);
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileUri = FileProvider.getUriForFile(activity.getApplicationContext(), authority, file);
            } else {
                fileUri = Uri.fromFile(file);
            }
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            activity.startActivityForResult(intent, requestCode);
        } else {
            ToastUtils.getInstance(activity).showMessage(activity.getString(R.string.on_sd_card_hint));
        }

    }

    /**
     * @param fragment    fragment
     * @param filePath    拍照后照片存储路径
     * @param authority   标记FileProvider
     * @param requestCode 调用系统相机请求码
     */
    public static void takePicture(Fragment fragment, String filePath, String authority, int requestCode) {
        if (null != fragment.getContext()) {
            Context context = fragment.getContext().getApplicationContext();
            if (SDCardUtils.isSDCardEnable()) {
                Intent intent = new Intent();
                File file = new File(filePath);
                File dir = file.getParentFile();
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                Uri fileUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    fileUri = FileProvider.getUriForFile(context, authority, file);
                } else {
                    fileUri = Uri.fromFile(file);
                }
                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                fragment.startActivityForResult(intent, requestCode);
            } else {
                ToastUtils.getInstance(context).showMessage(R.string.on_sd_card_hint);
            }
        }
    }


    /**
     * @param activity    当前activity
     * @param requestCode 打开相册的请求码
     */
    public static void selectPicture(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 更新媒体库
     *
     * @param context  上下文
     * @param filePath 文件路径名
     */
    public static void refreshMediaLibrary(Context context, String filePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(filePath));
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }
}
