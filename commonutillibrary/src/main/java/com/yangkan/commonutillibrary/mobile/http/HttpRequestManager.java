/*
 * Copyright (c) 2017.
 * BOCO Inter-Telecom. All rights reserved.
 */

package com.yangkan.commonutillibrary.mobile.http;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.util.List;
import java.util.Map;

/**
 * @author yangkan
 * @date 2017/12/15
 * @description 采用策略模式，进行Http请求
 */

public class HttpRequestManager {
    private static final String TAG = "HttpRequestManager";

    private HttpStrategy mHttpStrategy;

    public static HttpRequestManager getInstance() {
        return Holder.sInstance;
    }

    private HttpRequestManager() {
    }

    private static final class Holder {
        private static final HttpRequestManager sInstance = new HttpRequestManager();
    }

    /**
     * 初始化动作，需要在Application中调用
     * @param context
     */
    public void init(Context context) {

        mHttpStrategy = new OkHttpStrategy(new Handler(Looper.getMainLooper()));
    }

    /**
     * 设置网络请求的策略
     * @param httpStrategy
     */
    public void setHttpStrategy(HttpStrategy httpStrategy) {
        mHttpStrategy = httpStrategy;
    }

    /**
     * http get 请求， json 串是一个对象
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param httpListener
     * @param <T>
     */
    public <T> void httpGet(String url, Class<T> clazz, Object tag,
                            Map<String, String> headers, HttpListener<T> httpListener) {
        mHttpStrategy.httpGet(url, clazz, tag, headers, httpListener);
    }

    /**
     * http post 请求， json 串是一个对象
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param params
     * @param httpListener
     * @param <T>
     */
    public <T> void httpPost(String url, Class<T> clazz, Object tag, Map<String, String> headers,
                             Map<String, String> params, HttpListener<T> httpListener) {
        mHttpStrategy.httpPost(url, clazz, tag, headers, params, httpListener);
    }

    /**
     * http get 请求， json 串是一个数组
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param httpListener
     * @param <T>
     */
    public <T> void httpArrayGet(String url, final Class<T> clazz, Object tag, Map<String, String> headers,
                                 final HttpListener<List<T>> httpListener){
        mHttpStrategy.httpArrayGet(url, clazz, tag, headers, httpListener);
    }

    /**
     * http post 请求， json 串是一个数组
     * @param url
     * @param clazz
     * @param tag
     * @param headers
     * @param params
     * @param httpListener
     * @param <T>
     */
    public <T> void httpArrayPost(String url, Class<T> clazz, Object tag, Map<String, String> headers,
                                  Map<String, String> params, HttpListener<List<T>> httpListener){
        mHttpStrategy.httpArrayPost(url, clazz, tag, headers, params, httpListener);
    }

    public void cancelRequest(Object tag) {
        mHttpStrategy.cancelRequest(tag);
    }


}
