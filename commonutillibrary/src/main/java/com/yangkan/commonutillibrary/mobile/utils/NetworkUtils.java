package com.yangkan.commonutillibrary.mobile.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author yangkan
 * @date 2018/01/22
 * @description 网络工具类
 */
public class NetworkUtils {

    /**
     * 没有网络
     */
    public static final int NETWORK_TYPE_INVALID = -1;
    /**
     * 移动网络
     */
    public static final int NETWORK_TYPE_MOBILE = 0;
    /**
     * wifi网络
     */
    public static final int NETWORK_TYPE_WIFI = 1;

    /**
     * 判断网络是否连接
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null != manager) {
            NetworkInfo info = manager.getActiveNetworkInfo();
            if (null != info && info.isConnected()) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否是wifi连接
     */
    public static boolean isWifiConnected(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        return manager.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;

    }

    /**
     * get the network type (wifi, mobile)
     */
    public static int getNetWorkType(Context context) {

        int netWorkType;
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            int type = networkInfo.getType();
            switch (type) {
                case ConnectivityManager.TYPE_MOBILE:
                    netWorkType = NETWORK_TYPE_MOBILE;
                    break;
                case ConnectivityManager.TYPE_WIFI:
                    netWorkType = NETWORK_TYPE_WIFI;
                    break;
                default:
                    netWorkType = NETWORK_TYPE_INVALID;
                    break;
            }
        } else {
            netWorkType = NETWORK_TYPE_INVALID;
        }
        return netWorkType;
    }

}
