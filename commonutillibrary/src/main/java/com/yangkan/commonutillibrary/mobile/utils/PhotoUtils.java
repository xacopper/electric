package com.yangkan.commonutillibrary.mobile.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author yangkan
 * @date 2018/01/22
 * @description 相机工具类
 */
public class PhotoUtils {

    private static final String TAG = "PhotoUtils";

    /**
     * 读取uri所在的图片
     *
     * @param uri      图片对应的Uri
     * @param mContext 上下文对象
     * @return 获取图像的Bitmap
     */
    public static Bitmap getBitmapFromUri(Uri uri, Context mContext) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), uri);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param filePath
     * @param width
     * @param height
     * @return bitmap
     * 获取对应大小的图片
     */
    public static Bitmap getFitSampleBitmap(String filePath, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = getFitInSampleSize(width, height, options);
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        return BitmapFactory.decodeFile(filePath, options);
    }

    private static int getFitInSampleSize(int reqWidth, int reqHeight, BitmapFactory.Options options) {
        int inSampleSize = 1;
        if (options.outWidth > reqWidth || options.outHeight > reqHeight) {
            int widthRatio = Math.round((float) options.outWidth / (float) reqWidth);
            int heightRatio = Math.round((float) options.outHeight / (float) reqHeight);
            inSampleSize = Math.min(widthRatio, heightRatio);
        }
        return inSampleSize;
    }

    /**
     * 照片添加到本地图库
     *
     * @param context
     * @param bmp
     * @param filePath
     */
    public static void saveImageToGallery(Context context, Bitmap bmp, String filePath) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Log.e(TAG, "no sdcard");
            Toast.makeText(context, "no sdcard", Toast.LENGTH_LONG).show();
            return;
        }
        Log.d(TAG, "saveImageToGallery: filePath: " + filePath);
        int index = filePath.lastIndexOf('/');
        if (index == -1) {
            Log.e(TAG, "saveImageToGallery: " + "error filePath: " + filePath);
            return;
        }
        String dir = filePath.substring(0, index);
        String fileName = filePath.substring(index + 1);
        File appDir = new File(dir);
        if (!appDir.exists()) {
            appDir.mkdirs();
        }
        File file = new File(appDir, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 50, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //更新媒体库
        IntentUtils.refreshMediaLibrary(context, filePath);
    }


    /**
     * 设置水印图片在左上角
     *
     * @param context
     * @param src
     * @param watermark
     * @param paddingLeft
     * @param paddingTop
     * @return
     */
    public static Bitmap createWaterMaskLeftTop(Context context,
                                                Bitmap src,
                                                Bitmap watermark,
                                                int paddingLeft,
                                                int paddingTop) {
        return createWaterMaskBitmap(src, watermark,
                DisplayUtils.dip2px(context, paddingLeft),
                DisplayUtils.dip2px(context, paddingTop));
    }

    private static Bitmap createWaterMaskBitmap(Bitmap src,
                                                Bitmap watermark,
                                                int paddingLeft,
                                                int paddingTop) {
        if (src == null) {
            return null;
        }
        int width = src.getWidth();
        int height = src.getHeight();
        //创建一个bitmap
        Bitmap newb = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图
        //将该图片作为画布
        Canvas canvas = new Canvas(newb);
        //在画布 0，0坐标上开始绘制原始图片
        canvas.drawBitmap(src, 0, 0, null);
        //在画布上绘制水印图片
        canvas.drawBitmap(watermark, paddingLeft, paddingTop, null);
        // 保存
//        canvas.save(Canvas.ALL_SAVE_FLAG);
        canvas.save();
        // 存储
        canvas.restore();
        return newb;
    }

    /**
     * 设置水印图片在右下角
     *
     * @param context
     * @param src
     * @param watermark
     * @param paddingRight
     * @param paddingBottom
     * @return
     */
    public static Bitmap createWaterMaskRightBottom(Context context,
                                                    Bitmap src,
                                                    Bitmap watermark,
                                                    int paddingRight,
                                                    int paddingBottom) {
        return createWaterMaskBitmap(src, watermark,
                src.getWidth() - watermark.getWidth() - DisplayUtils.dip2px(context, paddingRight),
                src.getHeight() - watermark.getHeight() - DisplayUtils.dip2px(context, paddingBottom));
    }


    /**
     * 设置水印图片到右上角
     *
     * @param context
     * @param src
     * @param watermark
     * @param paddingRight
     * @param paddingTop
     * @return
     */
    public static Bitmap createWaterMaskRightTop(Context context,
                                                 Bitmap src,
                                                 Bitmap watermark,
                                                 int paddingRight,
                                                 int paddingTop) {
        return createWaterMaskBitmap(src, watermark,
                src.getWidth() - watermark.getWidth() - DisplayUtils.dip2px(context, paddingRight),
                DisplayUtils.dip2px(context, paddingTop));
    }

    /**
     * 设置水印图片到左下角
     *
     * @param context
     * @param src
     * @param watermark
     * @param paddingLeft
     * @param paddingBottom
     * @return
     */
    public static Bitmap createWaterMaskLeftBottom(Context context,
                                                   Bitmap src,
                                                   Bitmap watermark,
                                                   int paddingLeft,
                                                   int paddingBottom) {
        return createWaterMaskBitmap(src, watermark,
                DisplayUtils.dip2px(context, paddingLeft),
                src.getHeight() - watermark.getHeight() -
                        DisplayUtils.dip2px(context, paddingBottom));
    }

    /**
     * 设置水印图片到中间
     *
     * @param src
     * @param watermark
     * @return
     */
    public static Bitmap createWaterMaskCenter(Bitmap src, Bitmap watermark) {

        return createWaterMaskBitmap(src, watermark,
                (src.getWidth() - watermark.getWidth()) / 2,
                (src.getHeight() - watermark.getHeight()) / 2);
    }

    /**
     * 给图片添加文字到左上角
     *
     * @param context
     * @param bitmap
     * @param text
     * @return
     */
    public static Bitmap drawTextToLeftTop(Context context,
                                           Bitmap bitmap,
                                           String text,
                                           int size,
                                           int color,
                                           int paddingLeft,
                                           int paddingTop) {
        TextPaint textPaint = new TextPaint();
        textPaint.setColor(color);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(DisplayUtils.dip2px(context, size));

//        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        paint.setColor(color);
//        paint.setTextSize(dp2px(context, size));
        Rect bounds = new Rect();
        Log.d(TAG, "text: " + text);
        textPaint.getTextBounds(text, 0, text.length(), bounds);

        return drawTextToBitmap(context,
                bitmap,
                text,
                textPaint,
                bounds,
                DisplayUtils.dip2px(context, paddingLeft),
                DisplayUtils.dip2px(context, paddingTop) + bounds.height());
    }

    /**
     * 绘制文字到右下角
     *
     * @param context
     * @param bitmap
     * @param text
     * @param size
     * @param color
     * @param paddingRight
     * @param paddingBottom
     * @return
     */
    public static Bitmap drawTextToRightBottom(Context context,
                                               Bitmap bitmap,
                                               String text,
                                               int size,
                                               int color,
                                               int paddingRight,
                                               int paddingBottom) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(DisplayUtils.dip2px(context, size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        return drawTextToBitmap(context, bitmap, text, paint, bounds,
                bitmap.getWidth() - bounds.width() - DisplayUtils.dip2px(context, paddingRight),
                bitmap.getHeight() - DisplayUtils.dip2px(context, paddingBottom));
    }

    /**
     * 绘制文字到右上方
     *
     * @param context
     * @param bitmap
     * @param text
     * @param size
     * @param color
     * @param paddingRight
     * @param paddingTop
     * @return
     */
    public static Bitmap drawTextToRightTop(Context context,
                                            Bitmap bitmap,
                                            String text,
                                            int size,
                                            int color,
                                            int paddingRight,
                                            int paddingTop) {

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(DisplayUtils.dip2px(context, size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        return drawTextToBitmap(context, bitmap, text, paint, bounds,
                bitmap.getWidth() - bounds.width() - DisplayUtils.dip2px(context, paddingRight),
                DisplayUtils.dip2px(context, paddingTop) + bounds.height());
    }

    /**
     * 绘制文字到左下方
     *
     * @param context
     * @param bitmap
     * @param text
     * @param size
     * @param color
     * @param paddingLeft
     * @param paddingBottom
     * @return
     */
    public static Bitmap drawTextToLeftBottom(Context context,
                                              Bitmap bitmap,
                                              String text,
                                              int size,
                                              int color,
                                              int paddingLeft,
                                              int paddingBottom) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(DisplayUtils.dip2px(context, size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        return drawTextToBitmap(context, bitmap, text, paint, bounds,
                DisplayUtils.dip2px(context, paddingLeft),
                bitmap.getHeight() - DisplayUtils.dip2px(context, paddingBottom));
    }

    /**
     * 绘制文字到中间
     *
     * @param context
     * @param bitmap
     * @param text
     * @param size
     * @param color
     * @return
     */
    public static Bitmap drawTextToCenter(Context context,
                                          Bitmap bitmap,
                                          String text,
                                          int size,
                                          int color) {

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(DisplayUtils.dip2px(context, size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        return drawTextToBitmap(context, bitmap, text, paint, bounds,
                (bitmap.getWidth() - bounds.width()) / 2,
                (bitmap.getHeight() + bounds.height()) / 2);
    }

    /**
     * 图片上绘制文字
     *
     * @param context
     * @param bitmap
     * @param text
     * @param paint
     * @param bounds
     * @param paddingLeft
     * @param paddingTop
     * @return
     */
    private static Bitmap drawTextToBitmap(Context context,
                                           Bitmap bitmap,
                                           String text,
                                           Paint paint,
                                           Rect bounds,
                                           int paddingLeft,
                                           int paddingTop) {
        android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();

        paint.setDither(true); // 获取跟清晰的图像采样
        paint.setFilterBitmap(true);// 过滤一些
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);
        Canvas canvas = new Canvas(bitmap);

        StaticLayout sl = new StaticLayout(text, (TextPaint) paint,
                bitmap.getWidth() - 8, Layout.Alignment.ALIGN_NORMAL,
                1.0f, 0.0f, false);

        canvas.translate(paddingLeft, paddingTop);
        sl.draw(canvas);
//        canvas.save(Canvas.ALL_SAVE_FLAG);
        canvas.save();
        canvas.restore();

//        canvas.drawText(text, paddingLeft, paddingTop, paint);
        return bitmap;
    }

    /**
     * 缩放图片
     *
     * @param src
     * @param w
     * @param h
     * @return
     */
    public static Bitmap scaleWithWH(Bitmap src, double w, double h) {
        if (w == 0 || h == 0 || src == null) {
            return src;
        } else {
            // 记录src的宽高
            int width = src.getWidth();
            int height = src.getHeight();
            // 创建一个matrix容器
            Matrix matrix = new Matrix();
            // 计算缩放比例
            float scaleWidth = (float) (w / width);
            float scaleHeight = (float) (h / height);
            // 开始缩放
            matrix.postScale(scaleWidth, scaleHeight);
            // 创建缩放后的图片
            return Bitmap.createBitmap(src, 0, 0, width, height, matrix, true);
        }
    }
}
