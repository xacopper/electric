
package com.yangkan.commonutillibrary.mobile.watermark;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Shader;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import static com.yangkan.commonutillibrary.mobile.watermark.WatermarkPosition.CENTER;

public class WatermarkConfig {

    private Bitmap mBaseBitmap;
    @DrawableRes
    private int mBaseDrawable;
    private int mTextSize;
    @ColorInt
    private int mTextColor;
    @ColorInt
    private int mTextBackgroundColor;
    private String mTypeFacePath;
    private WatermarkPosition watermarkPosition;
    private String watermarkString;
    private Bitmap watermarkBitmap;
    private int mAplha;
    private Shader mTextShader;
    private float mRotation;
    private int mPositionX, mPositionY;
    private float mTextShadowBlurRadius, mTextShadowXOffset, mTextShadowYOffset;
    private int mTextShadowColor;
    private int mXMargin, mYMargin;

    private int mWatermarkWidth, mWatermarkHeight;

    private WatermarkConfig(WatermarkConfigBuilder builder) {
        this.mBaseBitmap = builder.mBaseBitmap;
        this.mBaseDrawable = builder.mBaseDrawable;
        this.mTextSize = builder.mTextSize;
        this.mTextColor = builder.mTextColor;
        this.mTextBackgroundColor = builder.mTextBackgroundColor;
        this.mTypeFacePath = builder.mTypeFacePath;
        this.watermarkPosition = builder.mWatermarkPosition;
        this.watermarkString = builder.mWatermarkString;
        this.watermarkBitmap = builder.mWatermarkBitmap;
        this.mAplha = builder.mAlpha;
        this.mTextShader = builder.mTextShader;
        this.mRotation = builder.mRotation;
        this.mPositionX = builder.mPositionX;
        this.mPositionY = builder.mPositionY;
        this.mTextShadowColor = builder.mTextShadowColor;
        this.mTextShadowXOffset = builder.mTextShadowXOffset;
        this.mTextShadowYOffset = builder.mTextShadowYOffset;
        this.mTextShadowBlurRadius = builder.mTextShadowBlurRadius;
        this.mXMargin = builder.mXMargin;
        this.mYMargin = builder.mYMargin;
        this.mWatermarkWidth = builder.mWatermarkWidth;
        this.mWatermarkHeight = builder.mWatermarkHeight;
    }

    public Bitmap getBaseBitmap() {
        return mBaseBitmap;
    }

    protected int getBaseDrawable() {
        return mBaseDrawable;
    }

    public int getTextSize() {
        return mTextSize;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public int getTextBackgroundColor() {
        return mTextBackgroundColor;
    }

    public String getTypeFacePath() {
        return mTypeFacePath;
    }

    public WatermarkPosition getWatermarkPosition() {
        return watermarkPosition;
    }

    public String getWatermarkString() {
        return watermarkString;
    }

    protected Bitmap getWatermarkBitmap() {
        return watermarkBitmap;
    }

    public int getAplha() {
        return mAplha;
    }

    public Shader getTextShader() {
        return mTextShader;
    }

    public float getRotation() {
        return mRotation;
    }

    public int getPositionX() {
        return mPositionX;
    }

    public int getPositionY() {
        return mPositionY;
    }

    public float getTextShadowBlurRadius() {
        return mTextShadowBlurRadius;
    }

    public float getTextShadowXOffset() {
        return mTextShadowXOffset;
    }

    public float getTextShadowYOffset() {
        return mTextShadowYOffset;
    }

    public int getTextShadowColor() {
        return mTextShadowColor;
    }

    public int getXMargin() {
        return mXMargin;
    }

    public int getYMargin() {
        return mYMargin;
    }

    public int getWatermarkWidth() {
        return mWatermarkWidth;
    }

    public int getWatermarkHeight() {
        return mWatermarkHeight;
    }

    public static class WatermarkConfigBuilder {

        private Bitmap mBaseBitmap;
        @DrawableRes
        private int mBaseDrawable;
        private int mTextSize = 40;
        @ColorInt
        private int mTextColor = Color.BLACK;
        @ColorInt
        private int mTextBackgroundColor;
        private String mTypeFacePath;
        private WatermarkPosition mWatermarkPosition = CENTER;
        private String mWatermarkString;
        private Bitmap mWatermarkBitmap;
        private int mAlpha = 255;
        private Shader mTextShader;
        private float mRotation;
        private int mPositionX, mPositionY;
        private float mTextShadowBlurRadius, mTextShadowXOffset, mTextShadowYOffset;
        @ColorInt
        private int mTextShadowColor = Color.WHITE;
        private int mXMargin, mYMargin;

        private int mWatermarkWidth, mWatermarkHeight;

        public WatermarkConfigBuilder base(final Bitmap bitmap) {
            this.mBaseBitmap = bitmap;
            return this;
        }

        public WatermarkConfigBuilder base(@DrawableRes int drawable) {
            this.mBaseDrawable = drawable;
            return this;
        }

        public WatermarkConfigBuilder textSize(final int size) {
            this.mTextSize = size;
            return this;
        }

        public WatermarkConfigBuilder textColor(final int color) {
            this.mTextColor = color;
            return this;
        }

        public WatermarkConfigBuilder textBackgroundColor(final int color) {
            this.mTextBackgroundColor = color;
            return this;
        }

        public WatermarkConfigBuilder textFont(final String typeFacePath) {
            this.mTypeFacePath = typeFacePath;
            return this;
        }

        public WatermarkConfigBuilder watermarkPosition(final WatermarkPosition position,
                                                        final int positionX,
                                                        final int positionY) {
            if (position != WatermarkPosition.CUSTOM) {
                throw new IllegalArgumentException("This constructor can only be used when the " +
                        "watermarkPosition is RubberStamp.CUSTOM");
            }
            this.mWatermarkPosition = position;
            this.mPositionX = positionX;
            this.mPositionY = positionY;
            return this;
        }

        public WatermarkConfigBuilder watermarkPosition(final WatermarkPosition position) {
            this.mWatermarkPosition = position;
            return this;
        }

        public WatermarkConfigBuilder watermark(final String watermarkString) {
            this.mWatermarkString = watermarkString;
            return this;
        }

        public WatermarkConfigBuilder watermark(final Bitmap watermarkBitmap) {
            this.mWatermarkBitmap = watermarkBitmap;
            return this;
        }


        public WatermarkConfigBuilder watermarkBitmapWidth(int watermarkWidth) {
            this.mWatermarkWidth = watermarkWidth;
            return this;
        }


        public WatermarkConfigBuilder watermarkBitmapHeith(int watermarkHeight) {
            this.mWatermarkHeight = watermarkHeight;
            return this;
        }


        public WatermarkConfigBuilder alpha(final int alpha) {
            this.mAlpha = alpha;
            return this;
        }

        public WatermarkConfigBuilder textShader(final Shader shader) {
            this.mTextShader = shader;
            return this;
        }

        public WatermarkConfigBuilder rotation(final float rotation) {
            this.mRotation = rotation;
            return this;
        }

        public WatermarkConfigBuilder textShadow(final float blurRadius, final float shadowXOffset,
                                                 final float shadowYOffset, @ColorInt final int shadowColor) {
            this.mTextShadowBlurRadius = blurRadius;
            this.mTextShadowXOffset = shadowXOffset;
            this.mTextShadowYOffset = shadowYOffset;
            this.mTextShadowColor = shadowColor;
            return this;
        }

        public WatermarkConfigBuilder margin(final int xMargin, final int yMargin) {
            this.mXMargin = xMargin;
            this.mYMargin = yMargin;
            return this;
        }

        public WatermarkConfig build() {
            return new WatermarkConfig(this);
        }
    }
}
