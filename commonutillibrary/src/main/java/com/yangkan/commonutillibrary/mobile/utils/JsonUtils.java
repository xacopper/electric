/*
 * Copyright (c) 2017.
 * BOCO Inter-Telecom. All rights reserved.
 */

package com.yangkan.commonutillibrary.mobile.utils;


import com.google.gson.Gson;
import com.yangkan.commonutillibrary.mobile.http.ParameterizedTypeImpl;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author yangkan
 * @date 2017/12/14
 * @description JSON工具类
 */

public final class JsonUtils {
    private static final String TAG = "JsonUtils";

    private JsonUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 将一个对象转换为JSON串
     *
     * @param object
     * @return
     */
    public static String toJson(Object object) {
        Gson gson = new Gson();
        String result = gson.toJson(object);
        LogUtils.d(TAG, "toJson: result: " + result);
        return result;
    }

    /**
     * 解析Json串
     *
     * @param jsonStr
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T parseJson(String jsonStr, Class<T> clazz) {
        LogUtils.d(TAG, "parseJson: jsonStr: " + jsonStr);
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, clazz);
    }

    /**
     * 解析json数组
     * @param jsonStr
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> parseArrayJson(String jsonStr, Class<T> clazz) {
        Gson gson = new Gson();
        Type listType = new ParameterizedTypeImpl(List.class, new Class[]{clazz});
        return gson.fromJson(jsonStr, listType);
    }
}


