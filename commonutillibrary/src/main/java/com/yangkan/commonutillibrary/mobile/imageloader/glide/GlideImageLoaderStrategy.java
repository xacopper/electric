package com.yangkan.commonutillibrary.mobile.imageloader.glide;

import android.content.Context;
import android.util.Log;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.yangkan.commonutillibrary.mobile.imageloader.BaseImageLoaderStrategy;
import com.yangkan.commonutillibrary.mobile.imageloader.ImageLoader;
import com.yangkan.commonutillibrary.mobile.imageloader.ImageLoaderUtil;
import com.yangkan.commonutillibrary.mobile.utils.NetworkUtils;


/**
 * @author yangkan
 * @date 2018/01/22
 * @description 图片加载框架 Glide策略
 */
public class GlideImageLoaderStrategy implements BaseImageLoaderStrategy {

    private static final String TAG = GlideImageLoaderStrategy.class.getSimpleName();

    @Override
    public void loadImage(Context context, ImageLoader imageLoader) {

        // TODO: 2017/5/11 get image display flag: wifi only?
        boolean flag = false;
        if (!flag) {
            loadNormal(context, imageLoader);
            return;
        }

        int strategy = imageLoader.getWifiStrategy();
        if (ImageLoaderUtil.LOAD_STRATEGY_ONLY_WIFI == strategy) {
            int networkType = NetworkUtils.getNetWorkType(context);
            //如果是在wifi下才加载图片，并且当前网络是wifi,直接加载
            if (networkType == NetworkUtils.NETWORK_TYPE_WIFI) {
                loadNormal(context, imageLoader);
            } else {
                //如果是在wifi下才加载图片，并且当前网络不是wifi，加载缓存
                loadCache(context, imageLoader);
            }
        } else {
            loadNormal(context, imageLoader);
        }

    }

    private void loadNormal(Context context, ImageLoader imageLoader) {

        Log.d(TAG, "loadNormal...");

        Glide.with(context)
                .load(imageLoader.getUrl())
                .placeholder(imageLoader.getPlaceHolder())
                .error(imageLoader.getError())
                .into(imageLoader.getImgView());

    }

    /**
     * load cache image with Glide
     */
    private void loadCache(Context context, ImageLoader imageLoader) {

        Glide.with(context)
                .load(imageLoader.getUrl())
                .placeholder(imageLoader.getPlaceHolder())
                .error(imageLoader.getError())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageLoader.getImgView());
    }

}
