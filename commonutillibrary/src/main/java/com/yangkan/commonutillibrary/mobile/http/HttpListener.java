package com.yangkan.commonutillibrary.mobile.http;

/**
 * @author yangkan
 * @date 2017/12/15
 * @description Http请求回调
 */

public interface HttpListener<T> {

    void onResponse(T response);

    void onFailure(String message);
}