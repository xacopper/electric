package com.yangkan.commonutillibrary.mobile.watermark;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;

import com.yangkan.commonutillibrary.mobile.utils.DisplayUtils;


/**
 * Created by yangkan on 2017/12/19.
 */

public class WatermarkHelper {

    /**
     * 水印文字到图片
     *
     * @param context
     * @param uri
     * @param str
     * @return
     */
    public static Bitmap watermarkText(Context context, Uri uri, String str) {

        Watermark mRubberStamp = new Watermark(context);

        WatermarkConfig config = new WatermarkConfig.WatermarkConfigBuilder()
                .base(getBitmapFromUri(uri, context))
                .alpha(255)
                .rotation(0)
                .watermarkPosition(WatermarkPosition.TOP_LEFT)
                .watermark(str)
                .textColor(Color.RED)
                .textBackgroundColor(Color.TRANSPARENT)
                //设置阴影
//               .textShadow(0.1f, 5, 5, Color.BLUE)
                /**
                 * 设置字体样式
                 * "fonts/bebasneue.otf"，"fonts/caviardreams.ttf"，
                 * "fonts/champagne.ttf"，"fonts/bebasneue.otf";
                 */
//               .textFont("fonts/bebasneue.otf")
                .textSize(DisplayUtils.dip2px(context, 30))
                .build();
        return mRubberStamp.addStamp(config);
    }

    /**
     * 水印文字到图片
     *
     * @param context
     * @param path
     * @param str
     * @return
     */
    public static Bitmap watermarkText(Context context, String path, String str) {

        Watermark mRubberStamp = new Watermark(context);

        WatermarkConfig config = new WatermarkConfig.WatermarkConfigBuilder()
                .base(getFitSampleBitmap(path, 1080, 1920))
                .alpha(255)
                .rotation(0)
                .watermarkPosition(WatermarkPosition.TOP_LEFT)
                .watermark(str)
                .textColor(Color.RED)
                .textBackgroundColor(Color.TRANSPARENT)
                .textSize(DisplayUtils.dip2px(context, 30))
                .build();
        return mRubberStamp.addStamp(config);
    }

    /**
     * 水印文字到图片
     *
     * @param context
     * @param bitmap
     * @param str
     * @return
     */
    public static Bitmap watermarkText(Context context, Bitmap bitmap, String str) {

        Watermark mRubberStamp = new Watermark(context);

        WatermarkConfig config = new WatermarkConfig.WatermarkConfigBuilder()
                .base(bitmap)
                .alpha(255)
                .rotation(0)
                .watermarkPosition(WatermarkPosition.TOP_LEFT)
                .watermark(str)
                .textColor(Color.RED)
                .textBackgroundColor(Color.TRANSPARENT)
                .textSize(DisplayUtils.dip2px(context, 30))
                .build();
        return mRubberStamp.addStamp(config);
    }

    /**
     * 添加水印图片（不需要指定水印图片的宽高，宽高为水印图片原图的宽高）
     * @param context
     * @param path
     * @param waterBitmap
     * @return
     */
    public static Bitmap watermarkBitmap(Context context, String path, Bitmap waterBitmap) {

        Watermark mRubberStamp = new Watermark(context);

        WatermarkConfig config = new WatermarkConfig.WatermarkConfigBuilder()
                .base(getFitSampleBitmap(path, 1080, 1920))
                .alpha(255)
                .rotation(0)
                .margin(40, 40)
                .watermarkPosition(WatermarkPosition.TOP_LEFT)
                .watermark(waterBitmap)
                .build();
        return mRubberStamp.addStamp(config);
    }

    /**
     * 添加水印图片,需要指定水印图片的宽高
     * @param context
     * @param path
     * @param waterBitmap
     * @param width
     * @param heith
     * @return
     */
    public static Bitmap watermarkBitmap(Context context, String path, Bitmap waterBitmap, int width, int heith) {

        Watermark mRubberStamp = new Watermark(context);

        WatermarkConfig config = new WatermarkConfig.WatermarkConfigBuilder()
                .base(getFitSampleBitmap(path, 1080, 1920))
                .alpha(255)
                .rotation(0)
                .watermarkPosition(WatermarkPosition.TOP_LEFT)
                .margin(40, 40)
                .watermark(waterBitmap)
                .watermarkBitmapWidth(DisplayUtils.dip2px(context, width))
                .watermarkBitmapHeith(DisplayUtils.dip2px(context, heith))
                .build();

        return mRubberStamp.addStamp(config);
    }

    /**
     * 读取uri所在的图片
     *
     * @param uri      图片对应的Uri
     * @param mContext 上下文对象
     * @return 获取图像的Bitmap
     */
    public static Bitmap getBitmapFromUri(Uri uri, Context mContext) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), uri);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getFitSampleBitmap(String file_path, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file_path, options);
        options.inSampleSize = getFitInSampleSize(width, height, options);
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        return BitmapFactory.decodeFile(file_path, options);
    }

    private static int getFitInSampleSize(int reqWidth, int reqHeight, BitmapFactory.Options options) {
        int inSampleSize = 1;
        if (options.outWidth > reqWidth || options.outHeight > reqHeight) {
            int widthRatio = Math.round((float) options.outWidth / (float) reqWidth);
            int heightRatio = Math.round((float) options.outHeight / (float) reqHeight);
            inSampleSize = Math.min(widthRatio, heightRatio);
        }
        return inSampleSize;
    }
}
