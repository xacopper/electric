package com.yangkan.commonutillibrary.mobile.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
//import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import androidx.core.content.FileProvider;

import java.io.File;

/**
 * @author yangkan
 * @date 2018/01/22
 * @description App工具类
 */
public class AppUtils {
    private static final String INSTALL_TYPE = "application/vnd.android.package-archive";

    public static PackageInfo getPackageInfo(Context context, String packageName) {
        if (null == context) {
            return null;
        }
        PackageInfo info = null;
        try {
            info = context.getPackageManager().getPackageInfo(
                    packageName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info;
    }

    public static String getAppVersionName(Context context, String packageName) {
        PackageInfo info = getPackageInfo(context, packageName);
        return info == null ? null : info.versionName;
    }

    public static int getAppVersionCode(Context context, String packageName) {
        PackageInfo info = getPackageInfo(context, packageName);
        return info == null ? 0 : info.versionCode;
    }

    public static boolean isAppUpdateable(Context context, String packageName,
                                          int newVersionCode) {
        if (newVersionCode == 0) {
            return false;
        }
        PackageInfo info = getPackageInfo(context, packageName);
        if (null == info) {
            return true;
        }
        int curVersionCode = info.versionCode;
        return newVersionCode > curVersionCode;

    }

    public static boolean isAppUpdateable(Context context, String packageName,
                                          String newVersionName) {
        if (TextUtils.isEmpty(newVersionName)) {
            return false;
        }
        PackageInfo info = getPackageInfo(context, packageName);
        if (null == info) {
            return true;
        }

        String curVersionName = info.versionName;
        if (TextUtils.isEmpty(curVersionName)) {
            return true;
        }

        String splitString = "\\.";
        String[] curVersions = curVersionName.split(splitString);
        String[] newVersions = newVersionName.split(splitString);
        boolean isUpdateable = false;
        for (int i = 0, curLength = curVersions.length, newLength = newVersions.length;
             i < curLength && i < newLength; i++) {
            if (TextUtils.isDigitsOnly(curVersions[i])
                    && TextUtils.isDigitsOnly(newVersions[i])) {
                int curTemp = Integer.parseInt(curVersions[i]);
                int newTemp = Integer.parseInt(newVersions[i]);
                if (newTemp != curTemp) {
                    if (newTemp > curTemp) {
                        isUpdateable = true;
                    }
                    break;
                }
            } else if (newVersions[i].compareToIgnoreCase(curVersions[i]) != 0) {
                if (newVersions[i].compareToIgnoreCase(curVersions[i]) > 0) {
                    isUpdateable = true;
                }
                break;
            }
        }
        return isUpdateable;
    }

    /**
     * 安装apk
     *
     * @param file      apk文件
     * @param context   上下文
     * @param authority FileProvider对应的authority，用于适配Android 7.0 以上版本
     */
    public static void installApk(File file, Context context, String authority) {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        Uri data;
//        // 判断版本大于等于7.0
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            data = FileProvider.getUriForFile(context, authority, file);
//            // 给目标应用一个临时授权
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        } else {
//            data = Uri.fromFile(file);
//        }
//        intent.setDataAndType(data, INSTALL_TYPE);
//        context.startActivity(intent);


        if (Build.VERSION.SDK_INT >= 24) {//判读版本是否在7.0以上
            Uri apkUri = FileProvider.getUriForFile(context, authority, file);//在AndroidManifest中的android:authorities值
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            install.setDataAndType(apkUri, "application/vnd.android.package-archive");
            context.startActivity(install);
        } else {
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(install);
        }

        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
