package com.yangkan.commonutillibrary.mobile.manage;

import android.app.Activity;

import java.util.Stack;

/**
 * @author yangkan
 * @date 2018/1/25
 * @description Activity栈管理类
 */

public class ActivityStackManager {

    private static Stack<Activity> mActivityStack;

    /**
     * 静态内部类构建单例
     */
    private static final class Holder {
        private static final ActivityStackManager INSTANCE = new ActivityStackManager();
    }

    /**
     * 获取唯一实例
     * @return
     */
    public static ActivityStackManager getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * 私有化构造函数
     */
    private ActivityStackManager() {
        mActivityStack = new Stack<>();
    }

    /**
     * 将 Activity 入栈
     * @param activity
     */
    public void pushActivity(Activity activity) {
        mActivityStack.push(activity);
    }

    /**
     * 将栈顶 Activity 出栈，并 finish
     */
    public void popActivity() {
        if (!mActivityStack.empty()) {
            mActivityStack.pop().finish();
        }
    }

    /**
     * finish 指定的 Activity
     * @param activity
     */
    public void finishActivity(Activity activity) {
        if (null != activity) {
            mActivityStack.remove(activity);
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    /**
     * 清空 Activity 栈，并 finish 所有 Activity
     */
    public void finishAllActivity() {
        while(!mActivityStack.empty()) {
            mActivityStack.pop().finish();
        }
    }

    /**
     * 获取栈顶 Activity
     * @return
     */
    public Activity getTopActivity() {
        return mActivityStack.peek();
    }

}
