package com.yangkan.commonutillibrary.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;
import com.yangkan.commonutillibrary.R;
import com.yangkan.commonutillibrary.mobile.imageloader.ImageLoader;
import com.yangkan.commonutillibrary.mobile.imageloader.ImageLoaderUtil;

/**
 * @author yangkan
 * @date 2018/01/22
 * @description 图片显示工具Activity
 */
public class PhotoViewActivity extends AppCompatActivity {
    private static final String TAG = "PhotoViewActivity";

    private static final String IMAGE_PATH = "image_path";
    private static final String IMAGE_URI = "image_uri";
    private PhotoView mPhotoView;

    public static void startPhotoActivity(Context context, String path) {
        Intent intent = new Intent(context, PhotoViewActivity.class);
        intent.putExtra(IMAGE_PATH, path);
        context.startActivity(intent);
    }

    public static void startPhotoActivity(Context context, Uri imageUri) {
        Intent intent = new Intent(context, PhotoViewActivity.class);
        intent.putExtra(IMAGE_URI, imageUri);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        initPhotoView();
    }


    private void initPhotoView() {
        mPhotoView = (PhotoView) findViewById(R.id.photo_view);
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        if (null != intent) {
            if (intent.hasExtra(IMAGE_PATH)) {
                String path = intent.getStringExtra(IMAGE_PATH);
                ImageLoader imageLoader = new ImageLoader.Builder()
                        .url(path.trim())
                        .placeHolder(R.drawable.details_img_loading)
                        .imgView(mPhotoView)
                        .build();
                ImageLoaderUtil.getInstance().loadImage(this, imageLoader);
            } else if (intent.hasExtra(IMAGE_URI)) {
                Uri imageUri = intent.getParcelableExtra(IMAGE_URI);
                if (null != imageUri) {
                    mPhotoView.setImageURI(imageUri);
                }
            }

        }
    }

    @Override
    protected void onDestroy() {
        mPhotoView.destroyDrawingCache();
        super.onDestroy();
    }
}
