
package com.yangkan.commonutillibrary.mobile.watermark;

public enum WatermarkPosition {
    TOP_LEFT, TOP_RIGHT, TOP_CENTER,
    CENTER_LEFT, CENTER_RIGHT, CENTER,
    BOTTOM_LEFT, BOTTOM_RIGHT,
    BOTTOM_CENTER, CUSTOM, TILE
}
