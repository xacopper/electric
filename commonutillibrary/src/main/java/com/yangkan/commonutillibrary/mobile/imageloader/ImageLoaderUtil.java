package com.yangkan.commonutillibrary.mobile.imageloader;

import android.content.Context;

import com.yangkan.commonutillibrary.mobile.imageloader.glide.GlideImageLoaderStrategy;


/**
 * @author yangkan
 * @date 2018/01/22
 * @description 图片加载框架
 */
public class ImageLoaderUtil {

    public static final int PIC_LARGE = 0;
    public static final int PIC_MEDIUM = 1;
    public static final int PIC_SMALL = 2;

    public static final int LOAD_STRATEGY_NORMAL = 0;
    public static final int LOAD_STRATEGY_ONLY_WIFI = 1;

    private static volatile ImageLoaderUtil instance;
    private BaseImageLoaderStrategy mStrategy;


    private ImageLoaderUtil() {
        mStrategy = new GlideImageLoaderStrategy();
    }

    public static ImageLoaderUtil getInstance() {
        if (null == instance) {
            synchronized (ImageLoaderUtil.class) {
                if (null == instance) {
                    instance = new ImageLoaderUtil();
                }
            }
        }
        return instance;
    }

    public void loadImage(Context context, ImageLoader imageLoader) {
        mStrategy.loadImage(context.getApplicationContext(), imageLoader);
    }

    public void setLoadImageStrategy(BaseImageLoaderStrategy strategy) {
        this.mStrategy = strategy;
    }
}
