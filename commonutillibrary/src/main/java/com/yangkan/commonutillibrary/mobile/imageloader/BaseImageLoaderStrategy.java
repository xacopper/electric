package com.yangkan.commonutillibrary.mobile.imageloader;

import android.content.Context;

/**
 * Created by yangkan on 2017/5/11.
 */

public interface BaseImageLoaderStrategy {
    void loadImage(Context context, ImageLoader imageLoader);

}
